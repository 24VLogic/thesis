\begin{thebibliography}{1}

\bibitem{sobottka}
Karin Sobottka;~Host Bunke.
\newblock Obstacle detection in range image sequences using radial slope.
\newblock In {\em IFAC Intelligent Autonomous Vehicles}, 1998.

\bibitem{luna}
Carlos A. Luna; Cristina Losada-Gutierrez; David Fuentes-Jimenez; Alvaro
  Fernandez-Rincon; Manuel Mazo;~Javier Macias-Guarasa.
\newblock Robust people detection using depth information from an overhead
  time-of-flight camera.
\newblock {\em Expert Systems With Applications}, 71:240--256, 2017.

\bibitem{zhang}
Min Zhang; Peizhi Liu; Xiaochuan Zhao; Xinxin Zhao;~Yuan Zhang.
\newblock An obstacle detection algorithm based on u-v disparity map analysis.
\newblock In {\em IEEE International Conference on Information Theory and
  Information Security}, 2010.

\bibitem{gao}
Yuan Gao; Xiao Ai; John Rarity;~Naim Dahnoun.
\newblock Obstacle detection with 3d camera using u-v-disparity.
\newblock In {\em International Workshop on Systems, Signal Processing, and
  their Applications}, 2011.

\bibitem{hane}
Christian H{\"a}ne; Lionel Heng; Gim Hee Lee; Friedrich Fraundorfer; Paul
  Furgale; Torsten Sattler;~Marc Pollefeys.
\newblock 3d visual perception for self-driving cars using a multi-camera
  system: Calibration, mapping, localization, and obstacle detection.
\newblock {\em Image and Vision Computing}, 68:14--27, 2017.

\bibitem{bendiksen}
A.~Bendiksen; G.~D. Hager.
\newblock Fast 3d boundary computation for occluding contour motion.
\newblock In {\em IEEE International Conference on Robotics {\&} Automation},
  1999.

\bibitem{bhatt}
Pankti P. Bhatt; Dr. Jeegar~A. Trivedi.
\newblock Obstacle detection for vehicles using sensors and artificial
  intelligence.
\newblock {\em International Journal of Advanced Research in Computer Science},
  8(9), 2017.

\bibitem{redelinghuys}
Ferdinand Redelinghuys; Vasileios Argyriou;~Maria Petrou.
\newblock Cast shadows estimation and synthesis using the walsh transform.
\newblock {\em Pattern Recognition Letters}, 48:93--99, 2014.

\end{thebibliography}
