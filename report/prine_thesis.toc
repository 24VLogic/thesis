\contentsline {paragraph}{}{1}{section*.2}
\contentsline {chapter}{Bibliography}{5}{chapter*.4}
\contentsline {chapter}{\numberline {1}Introduction}{8}{chapter.1}
\contentsline {chapter}{\numberline {2}Survey of the Literature}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Object Tracking}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Shape Reconstruction}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Obstacle Identification}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Spatial Clustering}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Rangefinders}{10}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Cameras}{10}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Stereo Vision}{11}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Time-of-Flight Cameras}{12}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Multiple Cameras}{12}{subsection.2.3.6}
\contentsline {subsection}{\numberline {2.3.7}U-V Disparity Mapping}{12}{subsection.2.3.7}
\contentsline {section}{\numberline {2.4}The U-V-Disparity Map}{14}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}U and V Disparity Map Sampling}{14}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Image Processing in The U and V Disparity Maps}{16}{subsection.2.4.2}
\contentsline {subsubsection}{Filter Response Difference Method}{18}{section*.10}
\contentsline {subsubsection}{Disparity Based Segmentation}{18}{section*.13}
\contentsline {chapter}{\numberline {3}Time-of-Flight Distance Imaging}{20}{chapter.3}
\contentsline {section}{\numberline {3.1}Physics of Time-of-Flight Cameras}{20}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Pulsed Modulation\cite {li}}{20}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}The Photonic Mixer Device and the ACF}{21}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Cartesian Spatial Coordinates}{24}{section.3.2}
\contentsline {subsubsection}{Ground Plane Subtraction}{24}{section*.20}
\contentsline {subsubsection}{Spatial Clustering of 3D Data}{25}{section*.23}
\contentsline {chapter}{\numberline {4}Results}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}Identifying Planes}{27}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}U and V Disparity Map Image Processing}{27}{subsection.4.1.1}
\contentsline {subsubsection}{Thresholding in the U- and V-disparity Map}{27}{section*.25}
\contentsline {subsubsection}{Gaussian Response Filtering}{28}{section*.27}
\contentsline {subsection}{\numberline {4.1.2}Plane Identification}{28}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}The $zy$ and $xz$ Histogram}{31}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Detection of Objects in the Occlusion Zone}{31}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Sources of Error}{33}{subsection.4.1.5}
\contentsline {subsubsection}{Sensor Limitations}{33}{section*.33}
\contentsline {subsubsection}{Plane Segmentation}{34}{section*.35}
\contentsline {subsection}{\numberline {4.1.6}Comparison of the UV-Disparity Map and the XYZ-Histogram Method}{38}{subsection.4.1.6}
\contentsline {chapter}{\numberline {5}Discussion}{44}{chapter.5}
\contentsline {section}{\numberline {5.1}Applications of Blind-spot Detection}{44}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Industrial Applications}{44}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Autonomous Navigation}{44}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Pedestrian Safety}{45}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Future Work}{45}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}The Curiosity Vector}{45}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Occlusion Density and Vector Selection}{46}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Networked Applications}{46}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Mobile Detectors}{46}{subsection.5.2.4}
\contentsline {chapter}{\numberline {6}Resources}{47}{chapter.6}
\contentsline {section}{\numberline {6.1}The dbbd Python Package}{47}{section.6.1}
\contentsline {chapter}{Bibliography}{47}{section.6.1}
