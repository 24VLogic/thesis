# Decision-Based Blind-Spot Detection with 3D Imaging
## by Joseph Bryant Prine
### Florida Polytechnic University

This repository holds the drafts for thee work to be submitted for the completion of the Master of Engineering degree at Florida Polytechnic University.

## Contents

### Report
This folder contains the drafts and latex files for the final report.

### Papers
This folder contains information regarding the literature referenced by this work.

### Python
This folder contains the applications and scripts used in the 3D imaging and processing algorithm.

### Images
This folder contains images that will be used in the final report and presentation.

### TBD: Presentation
This folder will contain the files for the defense presentation.
