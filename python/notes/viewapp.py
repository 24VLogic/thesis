import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D
import cv2
import sys
import time
from vtkmesh import gen_mesh

def hist_eq(image):
    hist,bins = np.histogram(image.flatten(),256,[0,256])
    cdf = hist.cumsum()
    cdf_m = np.ma.masked_equal(cdf,0)
    cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
    cdf = np.ma.filled(cdf_m,0).astype('uint8')
    eq_img = cdf[ndist]
    return eq_img

def thresh_array(array, limits):
    threshed_array = np.copy(array)
    for i in np.ndindex(array.shape):
        if array[i] < limits[0]:
            threshed_array[i] = limits[0]
        if array[i] > limits[1]:
            threshed_array[i] = limits[1]

    return threshed_array

def scan_array(array):
    for i in np.ndindex(array.shape):
        print array[i]

def scale_array(array,newmax):
    scaled_array = np.copy(array)
    scaled_array_max = float(max(np.max(scaled_array),1));
    scaled_array = np.array(newmax*scaled_array/scaled_array_max,dtype=np.uint8)
    return scaled_array

def reverse_array(array):
    rev_array = np.copy(array)
    array_max = float(max(np.max(array),1))
    rev_array = array_max - rev_array
    return rev_array

def main():
    #amp = sys.argv[1]
    #dist = sys.argv[2]
    amp_img = np.load(amp)
    dist_img = np.load(dist)
    dist_img = dist_img[:,25:155]
    #print dist_img.shape
    z_img = np.load('xyz.npy')[2]
    z_img = z_img/float(max(np.max(z_img),1))
    gen_mesh(z_img)

    threshed_dist_img = thresh_array(dist_img,[0,2800])
    rev_thresh_img = reverse_array(threshed_dist_img)
    blur_rev_thresh_img = cv2.GaussianBlur(rev_thresh_img,(5,5),0)
    blur_rev_thresh_img = cv2.GaussianBlur(blur_rev_thresh_img,(5,5),0)
    #gen_mesh((blur_rev_thresh_img)/15)

    fig = plt.figure()
    plt.title("O3D303 Images")
    plt.axis('off')
    ax1 = fig.add_subplot(1,2,1)
    ax2 = fig.add_subplot(1,2,2)
    ax1.axis('off')
    ax2.axis('off')
    ax1.title.set_text("Amplitude Image")
    ax2.title.set_text("Distance Image",)
    ax1.imshow(amp_img)
    ax2.imshow(dist_img,cmap='winter')
    #fig.savefig("o3dfig1.png", bbox_inches="tight")

    plt.show()

if __name__ == '__main__':
    main()
