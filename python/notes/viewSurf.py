import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig1 = plt.figure()

ax2 = fig1.add_subplot(121)
ax2.set_yticklabels('')
ax2.set_xticklabels('')
ax = fig1.add_subplot(122,projection='3d',aspect = 1)
ax.set_yticklabels('')
ax.set_xticklabels('')
ax.set_zticklabels('')

data = np.load('data/newData.npy')
amp = np.copy(data[0])
ax2.imshow(amp[:,45:140])
dist = np.copy(data[1])
xx = np.copy(data[2])
yy = np.copy(data[3])
zz = np.copy(data[4])

x = xx.flatten()
y = yy.flatten()
z = zz.flatten()

xf = []
yf = []
zf = []

for i in range(len(z)):
    if z[i] < 8000:
        xf.append(x[i])
        yf.append(y[i])
        zf.append(z[i])
print xx,yy,zz
xWindow = xx[:,45:140]
yWindow = yy[:,45:140]
zWindow = zz[:,45:140]
xx = xWindow
yy = yWindow
zz = zWindow
X = np.array([xx.flatten(),yy.flatten(),zz.flatten()]).T

# DBSCAN clustering algorithm
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.preprocessing import StandardScaler
X = StandardScaler().fit_transform(X)

db = DBSCAN(eps=0.125/1.5, min_samples=12).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_)
clusters = []
unique_labels = set(labels)
colors = [plt.cm.Vega20(each)
          for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    if k == 1:
        pass
    else:
        class_member_mask = (labels == k)

        xy = X[class_member_mask & core_samples_mask]
        ax.plot( xy[:, 0],xy[:, 1],  xy[:,2], 'o', markerfacecolor=tuple(col))

        clusters.append(xy)
        xy = X[class_member_mask & ~core_samples_mask]
        ax.plot( xy[:, 0],xy[:, 1],  xy[:,2], 'o', markerfacecolor=tuple(col),markeredgecolor='k', markersize=1)
        #print clusters

#ax.plot_wireframe(xWindow,yWindow,zWindow)
#ax.plot_trisurf(xf,yf,zf)
ax.view_init(-75, -90)
plt.show()
