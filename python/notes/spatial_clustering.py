import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler

def dbscan_mesh(mesh, eps=.125, min_samples=3):
    pointCloud = np.array([mesh[0].flatten(),mesh[1].flatten(),mesh[2].flatten()]).T
    scaler = StandardScaler
    pointCloud = scaler().fit_transform(pointCloud)
    # DBSCAN Algorithm
    db = DBSCAN(eps=eps, min_samples=10).fit(pointCloud)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    clusters = []
    unique_labels = set(labels)
    for k in unique_labels:
        class_member_mask = (labels == k)
        clusters.append(pointCloud[class_member_mask & core_samples_mask])
    return clusters
if __name__ == "__main__":
    data = np.load('data/newData.npy')
    mesh = data[2:5]
    clusters = dbscan_mesh(mesh)
    print len(clusters)
