import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import cv2
import numpy as np

x = np.zeros((172,136))
y = np.zeros((136,172))
z = np.zeros((172,136))


z[20:70,20:70] = z[20:70,20:70] + np.random.rand(50,50)
z[30:60,30:60]= z[30:60,30:60] + 10
z[30:60,30:60]= z[30:60,30:60] + np.random.rand(30,30)
for i, each in enumerate(y):
    y[i]= np.linspace(-y.shape[1]/2,y.shape[1]/2,y.shape[1])

for i,each in enumerate(x):

    x[i]= np.linspace(-x.shape[1]/2,x.shape[1]/2,x.shape[1])
b1 = 5
b2 = 7
b3 = 5
print x.shape
y = y.T
print y.shape
zWindow = z[20:70,20:70]
blur = cv2.blur(zWindow,(b1,b1))
blur2 = cv2.blur(zWindow,(b2,b2))
#kernel = np.array([[-1,-1,-1], [-1,13,-1], [-1,-1,-1]])
#im = cv2.filter2D(blur2, -1, kernel)
blur3 = cv2.blur(zWindow,(b3,b3))
fig = plt.figure(figsize=(10,10))

n = 99
blur3b = np.copy(blur3)
for i in range(n):
    blur3b = cv2.blur(blur3b,(b3,b3))

zSubtract = np.copy(zWindow)
zSubtract[zSubtract < np.max(blur3b)] = 0

ax = fig.add_subplot(221,projection='3d',aspect=0.6)
ax.plot_surface(x[20:70,20:70],y[20:70,20:70],zWindow,rstride=1,cstride=1,cmap='winter')
ax.title.set_text('Noisy Image')

ax = fig.add_subplot(222,projection='3d',aspect=0.6)
ax.plot_surface(x[20:70,20:70],y[20:70,20:70],blur,rstride=1,cstride=1,cmap='winter')
ax.title.set_text('{0} x {0} blur'.format(b1))
'''
ax = fig.add_subplot(323,projection='3d',aspect=0.6)
ax.plot_surface(x[20:70,20:70],y[20:70,20:70],blur2,rstride=1,cstride=1,cmap='winter')
ax.title.set_text('{0} x {0} blur'.format(b2))

ax = fig.add_subplot(324,projection='3d',aspect=0.6)
ax.plot_surface(x[20:70,20:70],y[20:70,20:70],blur3,rstride=1,cstride=1,cmap='winter')
ax.title.set_text('{0} x {0} blur'.format(b3))
'''


ax = fig.add_subplot(223,projection='3d',aspect=0.6)
ax.plot_surface(x[20:70,20:70],y[20:70,20:70],blur3b,rstride=1,cstride=1,cmap='winter')
ax.title.set_text('{0} x {0} blur, {1} times'.format(b3,n+1))


ax = fig.add_subplot(224,projection='3d',aspect=0.6)
ax.plot_surface(x[20:70,20:70],y[20:70,20:70],zSubtract,rstride=1,cstride=1,cmap='winter')
ax.title.set_text('Threshhold, max of of {0} x {0} blur, {1} times'.format(b3,n+1))

fig.savefig("background_subtraction.png")
plt.show()
