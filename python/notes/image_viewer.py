from __future__ import (absolute_import, division, print_function, unicode_literals)
from builtins import *
import o3d3xx
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


imageWidth = 176
imageHeight = 132

class GrabO3D300():
    def __init__(self,data):
        self.data = data
        self.Amplitude = np.zeros((imageHeight,imageWidth))
        self.Distance = np.zeros((imageHeight,imageWidth))
        self.x = np.zeros((imageHeight,imageWidth))
        self.y = np.zeros((imageHeight,imageWidth))
        self.z = np.zeros((imageHeight,imageWidth))

    def readNextFrame(self):
        result = self.data.readNextFrame()
        self.Amplitude = np.frombuffer(result['amplitude'],dtype='uint16')
        self.Amplitude = self.Amplitude.reshape(imageHeight,imageWidth)
        self.Distance = np.frombuffer(result['distance'],dtype='uint16')
        self.Distance = self.Distance.reshape(imageHeight,imageWidth)
        self.x = np.frombuffer(result['x'],dtype='int16')
        self.x = self.x.reshape(imageHeight,imageWidth)
        self.y = np.frombuffer(result['y'],dtype='int16')
        self.y = self.y.reshape(imageHeight,imageWidth)
        self.z = np.frombuffer(result['z'],dtype='int16')
        self.z = self.z.reshape(imageHeight,imageWidth)
        self.illuTemp = 20.0

def updatefig(*args):
    g = args[1]
    g.readNextFrame();
    imX = args[2]
    xmax = float(max(np.max(g.x),1));
    imX.set_array(g.x/ xmax)
    #axAmp = imAmp.get_axes()
    #axAmp.set_title('Amplitude (Illu temp: %0.2f)'%(g.illuTemp))

    imY = args[3]
    ymax = float(max(np.max(g.y),1));
    imY.set_array(g.y/ ymax)
    #axDist = imDist.get_axes()
    #axDist.set_title('Distance')

    imZ = args[4]
    zmax = float(max(np.max(g.z),1));
    imZ.set_array(g.z/ zmax)
    #axDist = imDist.get_axes()
    #axDist.set_title('Distance')
    return imX,imY,imZ

def main():
    address = sys.argv[1]
    camData = o3d3xx.ImageClient(address, 50010)

    fig = plt.figure()
    grabber = GrabO3D300(camData)
    ax1 = fig.add_subplot(1, 3, 1)
    ax2 = fig.add_subplot(1, 3, 2)
    ax3 = fig.add_subplot(1, 3, 3)
    imX = ax1.imshow(np.random.rand(imageHeight,imageWidth))
    imY = ax2.imshow(np.random.rand(imageHeight,imageWidth))
    imZ = ax3.imshow(np.random.rand(imageHeight,imageWidth))
    ani = animation.FuncAnimation(fig, updatefig, interval=50, blit=True, fargs = [grabber,imX,imY,imZ])
    plt.show()

if __name__ == '__main__':
    main()
