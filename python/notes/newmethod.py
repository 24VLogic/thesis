import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111,projection="3d")

data = np.load("data/newxyz.npy")
data = np.array(data,dtype="int16")
print "shape of array: ", data.shape
centerCol = data[:,:,176/2]
print "center column: ", centerCol
for i in range(data.shape[2]):
    col = data[:,:,i]
    xx = col[0]
    yy = col[1]
    zz = col[2]
    ax.plot(xx,yy,zz,'kx')
#print type(data[0,0,0])
plt.show()
'''
fig = plt.figure()
ax = fig.add_subplot(111,projection="3d")
#ax.plot(centerCol[0],centerCol[1],centerCol[2])


plt.show()
'''
