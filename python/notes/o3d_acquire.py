from __future__ import (absolute_import, division, print_function, unicode_literals)
from builtins import *
import o3d3xx
import sys
import time
import numpy as np


imageWidth = 176
imageHeight = 132

class GrabO3D300():
    def __init__(self,data):
        self.data = data
        self.Amplitude = np.zeros((imageHeight,imageWidth))
        self.Distance = np.zeros((imageHeight,imageWidth))
        self.x = np.zeros((imageHeight,imageWidth))
        self.y = np.zeros((imageHeight,imageWidth))
        self.z = np.zeros((imageHeight,imageWidth))

    def readNextFrame(self):
        result = self.data.readNextFrame()
        self.Amplitude = np.frombuffer(result['amplitude'],dtype='uint16')
        self.Amplitude = self.Amplitude.reshape(imageHeight,imageWidth)
        self.Distance = np.frombuffer(result['distance'],dtype='uint16')
        self.Distance = self.Distance.reshape(imageHeight,imageWidth)
        self.x = np.frombuffer(result['x'],dtype='int16')
        self.x = self.x.reshape(imageHeight,imageWidth)
        self.y = np.frombuffer(result['y'],dtype='int16')
        self.y = self.y.reshape(imageHeight,imageWidth)
        self.z = np.frombuffer(result['z'],dtype='int16')
        self.z = self.z.reshape(imageHeight,imageWidth)
        self.illuTemp = 20.0

def main():
    address = sys.argv[1]
    camData = o3d3xx.ImageClient(address, 50010)
    grabber = GrabO3D300(camData)
    print(grabber)
    grabber.readNextFrame()
    data = np.array([grabber.Amplitude,grabber.Distance,grabber.x,grabber.y, grabber.z])
    print(data)
    np.save('newData1.npy',data)
if __name__ == "__main__":
    main()
