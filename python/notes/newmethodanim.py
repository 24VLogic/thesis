import numpy as np
#import scipy as sp
from scipy.signal import medfilt as mf
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation

def next_scan(*args):
    axis = args[1]
    data = args[2]
    col = data[:,:,args[0]]
    x = col[0].T.tolist()
    y = col[1].T.tolist()
    z = col[2].T.tolist()
    for i, eachElement in enumerate(z):
        if eachElement == 0:
            del(z[i]); del(x[i]); del(y[i])
            print "catch"
    #print min(z)
    line = axis.plot(x,y,z)
    if args[0] == data.shape[2]-1:
        axis.clear()
        axis.set_xlim(-1500,4000)
        axis.set_ylim(-2000,2000)
        axis.set_zlim(0,6000)
        axis.view_init(-64,-34)
    return line

def main():

    data = np.array(np.load("data/newxyz.npy"),dtype="int16")
    fig = plt.figure()
    ax = fig.add_subplot(111,projection="3d")
    ax.set_xlim(-1500,4000)
    ax.set_ylim(-2000,2000)
    ax.set_zlim(0,6000)
    ax.view_init(-64,-34)
    ani = animation.FuncAnimation(fig, next_scan, interval=50, frames=data.shape[2], blit=False, fargs = [ax,data])
    #ani.save("linescan.gif",dpi=80,writer="imagemagick")
    plt.show()

if __name__  == "__main__":
    main()
