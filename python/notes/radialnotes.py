import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Arc
from mpl_toolkits.mplot3d import Axes3D

def get_cartesians(point):
    xVals = [point[0],point[0]],[0,point[1]]
    yVals = [0,point[0]],[point[1],point[1]]
    xLine = Line2D(xVals[0],xVals[1],linestyle='dashed')
    yLine = Line2D(yVals[0],yVals[1],linestyle='dashed')
    return [xLine,yLine]

def get_radials(point, offset = 2, color = 'black', origin = [0,0], len_x_axis = 3, len_y_axis = 3):
    rvals = [[0,point[0]],[0,point[1]]]
    theta = np.degrees(np.arctan(point[1]/point[0]))
    rLine = Line2D(rvals[0],rvals[1], linestyle='dashed')
    thetaArc = Arc(origin, len_x_axis*offset, len_y_axis*offset, 0, theta1=0, theta2=theta, color=color, label = str(theta)+u"\u00b0",linestyle='dashed')
    return [rLine, thetaArc]

def cart_to_rad(point):
    r = np.sqrt(point[0]**2+point[1]**2)
    th = np.arctan(point[1]/point[0])
    return [r,th]

def rad_to_cart(point):
    x = np.cos(point[0])*point[1]
    y = np.sin(point[0])*point[1]
    return [x,y]
    
def main():
    #Set up the figure
    fig1 = plt.figure()
    #Set up the first subplot
    ax1 = fig1.add_subplot(231)
    ax1.title.set_text("Cartesian Coordinates")
    ax1.set_xlim(-10,10)
    ax1.set_ylim(-10,10)
    ax1.axhline(color='black',linestyle='solid',lw=0.5)
    ax1.axvline(color='black',linestyle='solid',lw=0.5)
    ax1.grid()
    ax1.figaspect=('equal')
    #Set up the second subplot
    ax2 = fig1.add_subplot(232)
    ax2.title.set_text("Radial Coordinates")
    ax2.set_xlim(-10,10)
    ax2.set_ylim(-10,10)
    ax2.axhline(color='black',linestyle='solid',lw=0.5)
    ax2.axvline(color='black',linestyle='solid',lw=0.5)
    ax2.grid()

    #Set up the third subplot
    ax3 = fig1.add_subplot(233, projection = 'polar')
    ax3.title.set_text("Radial Coordinates")
    ax3.set_ylim(-10,10)
    #Create point of interest
    point = [3,4]
    #Plot cartesian coordinates
    ax1.plot(point[0],point[1],'ob')
    cart_lines = get_cartesians(point)
    for line in cart_lines:
        ax1.add_line(line)
    ax1.plot(point[0],point[1],'ob')
    #plot radial coordinates
    radials = get_radials(point)
    ax2.add_patch(radials[1])
    ax2.add_line(radials[0])
    ax2.plot(point[0],point[1],'ob')

    rad_coords = cart_to_rad(point)

    ax3.scatter(rad_coords[1],rad_coords[0])

    plt.show()

if __name__ =='__main__':
    main()
