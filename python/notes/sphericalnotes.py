import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def get_sphericals(point):
    x = point[0]
    y = point[1]

def sph_to_cart(point):
    r,t,p = point[0],point[1],point[2]
    x = r*np.sin(np.radians(t))*np.cos(np.radians(p))
    y = r*np.sin(np.radians(t))*np.sin(np.radians(p))
    z = r*np.cos(np.radians(t))
    return [x,y,z]

def spherical_transform(array,wangle,hangle):
    points = []
    xs = []
    ys = []
    zs = []
    width = array.shape[1]
    height = array.shape[0]
    thetas = np.linspace(-wangle/2,wangle/2,num=width, endpoint=True)
    phis = np.linspace(-hangle/2,hangle/2,num=height, endpoint=True)

    for i in np.ndindex(array.shape):
        spoint = [array[i],phis[i[0]],thetas[i[1]]]
        cpoint = sph_to_cart(spoint)
        xs.append(cpoint[0])
        ys.append(cpoint[1])
        zs.append(cpoint[2])
    #np.save('data/cpoints.npy', points)
    return xs,ys,zs

def main():

    figure1 = plt.figure()

    axis1 = figure1.add_subplot(111,projection='3d')
    #axis1.set_xlim(-10,10)
    #axis1.set_ylim(0,15)
    #spoints_array = np.load('data/boxdist.npy')
    #cx,cy,cz = spherical_transform(spoints_array,60,45)
    #x = np.linspace(-10,10, num=10,endpoint=True)
    #y = np.random.randint(3,size=10)+10
    xyz = np.load('xyz.npy')
    axis1.scatter(xyz[0],xyz[1],xyz[2])
    '''
    for i,point in enumerate(y):
        axis1.plot([0,x[i]],[0,point],[0,0])

    thetas = np.linspace(120,60,num=10,endpoint=True)

    randrs = np.random.randint(3, size=10)+10
    txs = np.cos(np.radians(thetas))*randrs
    tys = np.sin(np.radians(thetas))*randrs
    print randrs

    for i, tx in enumerate(txs):
        axis1.plot([0,txs[i]],[0,tys[i]],[0,0],linestyle='dotted', color = 'black')
        axis1.plot([txs[i],txs[i]],[tys[i],0],[-10,-10], color = 'red')
    axis1.plot(txs,tys,[0]*10)
    axis1.plot(txs,tys,[-10]*10)
    axis1.plot([0],[0],[0])
    '''

    plt.show()


if __name__ == '__main__':
    main()
