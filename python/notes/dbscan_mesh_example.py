import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def getSurface(mesh):
    x = mesh[0].flatten()
    y = mesh[1].flatten()
    z = mesh[2].flatten()
    flatMesh = np.array([x,y,z]).T
    return flatMesh

x = np.arange(-1,1.125,0.125)
y = np.arange(-1,1.125,0.125)
x,y = np.meshgrid(x,y)
z = np.sqrt(1 - (x**2 + y**2))
for row in z:
    for element in row:
        element +=100
print z
xyz =  [x.flatten(),y.flatten(),z.flatten()]

xF = []
yF = []
zF = []
for i,zVal in enumerate(xyz[2]):
    if  zVal >= 0:
        xF.append(x.flatten()[i])
        yF.append(y.flatten()[i])
        zF.append(z.flatten()[i])


def main():
    fig1 = plt.figure()
    rawSurfPlot = fig1.add_subplot(111,projection='3d')
    #rawSurfPlot.plot(spherePoints1[0],spherePoints1[1],spherePoints1[2],'o')
    rawSurfPlot.plot_trisurf(xyz[0],xyz[1],xyz[2])
    #rawSurfPlot.plot(xF,yF,zF,'o')
    plt.show()
if __name__ == "__main__":
    main()
