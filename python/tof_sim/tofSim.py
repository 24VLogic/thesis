import numpy as np
import matplotlib.pyplot as plt
from scipy import signal



def emitted_signal(t,window=[np.pi/2,3*np.pi/2]):
    if window[0] <= t <= window[1]:
        return 1
    else:
        return 0

def reflected_signal(t,window=[np.pi/2+np.pi/4,3*np.pi/2+np.pi/4]):
    if window[0] <= t <= window[1]:
        return 1
    else:
        return 0

def main():
    ts = np.linspace(0,2*np.pi,1000)
    ts = np.hstack([ts,ts+2*np.pi])
    ys = []
    ys2 = []
    for t in ts:
        ys.append(emitted_signal(t))
        ys2.append(reflected_signal(t))
    fig = plt.figure()
    print fig
    ax1 = fig.add_subplot(411)
    ax1.plot(ts,ys)

    ax2 = fig.add_subplot(412)
    ax2.plot(ts,ys2)

    plt.show()

if __name__ == "__main__":
    main()
