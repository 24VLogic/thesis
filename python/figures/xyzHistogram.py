import sys
import dbbsd
import matplotlib.pyplot as plt
np = dbbsd.np
data = np.load('data/testData1.npy')

distanceImage = data[0]
amplitudeImage = data[1]
xImage = data[2]
yImage = data[3]
zImage = data[4]
groundMask = dbbsd.get_groundMask(yImage)['mask']
vDispG = dbbsd.get_vDisparity(distanceImage,30,groundMask)
yzHistG = dbbsd.get_yzHistogram(yImage,zImage,zBins = 30,mask = groundMask)
vDisp = dbbsd.get_vDisparity(distanceImage,30)
yzHist = dbbsd.get_yzHistogram(yImage,zImage,zBins = 30)
maskedImage = (1-groundMask)*distanceImage
fig = plt.figure()
ax1 = fig.add_subplot(131,aspect='auto')
ax2 = fig.add_subplot(132,aspect='auto')
ax3 = fig.add_subplot(133,aspect='auto')
ax1.imshow(vDispG['image'])
ax2.imshow(yzHistG)
ax3.imshow(maskedImage)
ax1.axis('off')
ax1.title.set_text('V-Disparity Map')
ax2.axis('off')
ax2.title.set_text(r'$zy$-Histogram')
ax3.axis('off')
ax3.title.set_text('Distance Image')
#plt.imshow(maskedImage)
plt.savefig('yzHistMasked.png',facecolor = 'xkcd:grey')
