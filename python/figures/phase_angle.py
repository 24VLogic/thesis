import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Rectangle

fig = plt.figure(facecolor = 'xkcd:grey')
axes = fig.subplots(nrows=4,sharex = True)
ax1 = axes[0]
ax2 = axes[1]
ax3 = axes[2]
ax4 = axes[3]
ax1.title.set_text('Emitted Signal')
ax2.title.set_text('Reflected Signal')
ax3.title.set_text(r'$C_1$ Window')
ax4.title.set_text(r'$C_2$ Window')
for ax in axes:
    ax.set_xlim(0,7)
    ax.set_ylim(0,1.75)
    ax.set_facecolor('xkcd:white')
    ax.set_xticklabels([])
    ax.set_yticklabels([])
'''zeros = np.zeros(10)
ones = np.ones(10)

signal = np.hstack((zeros,ones))
for i in range(2):
    signal = np.hstack((signal,signal))
signal = np.hstack((signal,zeros))
ax1.plot(signal)'''
pulseWidth = 1
lineXs = []
lineYs = []
for i in range(7):

    lineXs.append([i,i+pulseWidth])
    if i%2:
        lineYs.append([1,1])
    else:
        lineYs.append([0,0])
    if i > 0:
        lineXs.append([i,i])
        lineYs.append([0,1])
lines = []
linesA = []
linesB = []
for i, pts in enumerate(lineXs):
    lines.append(plt.Line2D(lineXs[i],lineYs[i]))
    linesA.append(plt.Line2D(lineXs[i],lineYs[i],linestyle='dashed'))
    linesB.append(plt.Line2D(lineXs[i],lineYs[i],linestyle='dashed'))
for line in lines:
    ax1.add_line(line)
for line in linesA:
    ax3.add_line(line)
for line in linesB:
    ax4.add_line(line)
lineXs2 = []
offset = 0.75
for xPts in lineXs:
    lineXs2.append([xPts[0]+offset,xPts[1]+offset])
lines2 = []
lines2A = []
lines2B = []
for i, pts in enumerate(lineXs2):
    if i == 0:
        lines2.append(plt.Line2D([0,offset],[0,0],color = 'xkcd:violet'))
        lines2A.append(plt.Line2D([0,offset],[0,0],linestyle='dashed',color = 'xkcd:violet'))
        lines2B.append(plt.Line2D([0,offset],[0,0],linestyle='dashed',color = 'xkcd:violet'))
    lines2.append(plt.Line2D(lineXs2[i],lineYs[i],color = 'xkcd:violet'))
    lines2A.append(plt.Line2D(lineXs2[i],lineYs[i],linestyle='dashed',color = 'xkcd:violet'))
    lines2B.append(plt.Line2D(lineXs2[i],lineYs[i],linestyle='dashed',color = 'xkcd:violet'))
for line in lines2:
    ax2.add_line(line)
for line in lines2A:
    ax3.add_line(line)
for line in lines2B:
    ax4.add_line(line)
c1Width = pulseWidth-offset
c2Width = pulseWidth - c1Width
c1rects = []
c2rects = []
for i in range(7):
    if i%2:
        xc1 = i+offset
        c1rects.append(Rectangle((xc1,0),height = 1, width = c1Width,alpha =0.7))
        ax3.text(xc1+c1Width/2,0.5,r'$C_1$',horizontalalignment = 'center',verticalalignment='center')
        xc2 = i+1
        c2rects.append(Rectangle((xc2,0),height = 1, width = c2Width,alpha =0.7))
        ax4.text(xc2+c2Width/2,0.5,r'$C_2$',horizontalalignment = 'center',verticalalignment='center')
for rect in c1rects:
    ax3.add_patch(rect)
for rect in c2rects:
    ax4.add_patch(rect)
plt.tight_layout()
plt.savefig('tof_windows',facecolor='xkcd:grey')
#plt.show()
