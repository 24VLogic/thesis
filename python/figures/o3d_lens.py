import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
fig = plt.figure()
ax = fig.add_subplot(111,facecolor = 'beige')

pmd = {'x':[0]*4,'y':np.arange(4)+2}
lensCoords = {'x':[1.5]*6,'y':np.arange(6)+1}
lensLine = Line2D(lensCoords['x'],lensCoords['y'],linestyle = 'dashed')
ax.add_line(lensLine)
target1Coords = {'x':[7]*4,'y':np.arange(4)}
target1Line = Line2D(target1Coords['x'],target1Coords['y'],linewidth=2,color = 'xkcd:blue')
target2Coords = {'x':[9]*7,'y':np.arange(7)}
target2Line = Line2D(target2Coords['x'],target2Coords['y'],linewidth=2,color = 'xkcd:green')
lineStarts = {'x':[7,7,9,9],'y':[1,2.5,4.5,6]}
lineEnds = {'x':[1.5]*4,'y':np.arange(4)+2}
for i, xCoord in enumerate(lineStarts['x']):
    line = Line2D([xCoord,lineEnds['x'][i]],[lineStarts['y'][i],lineEnds['y'][i]],linestyle = 'dotted',color = 'r')
    ax.add_line(line)
    line = Line2D([lineEnds['x'][i],pmd['x'][i]],[lineEnds['y'][i],pmd['y'][i]],linestyle = 'dotted', color = 'r')
    ax.add_line(line)
ax.add_line(target1Line)
ax.add_line(target2Line)
ax.text(pmd['x'][-1],pmd['y'][-1]+0.25,'pmd array',horizontalalignment = 'center',verticalalignment = 'bottom')
ax.text(lensCoords['x'][0],lensCoords['y'][0],'lens',horizontalalignment = 'center',verticalalignment = 'top')
ax.text(target1Coords['x'][-1],target1Coords['y'][-1]+0.25,'obstacle', horizontalalignment = 'center',verticalalignment = 'bottom')
ax.text(target2Coords['x'][-1],target2Coords['y'][-1]+0.25,'obstacle', horizontalalignment = 'center',verticalalignment = 'bottom')
#ax.axis('off')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_xlim(-1,10)
ax.set_ylim(0,8)
ax.plot(pmd['x'],pmd['y'],'o')
plt.savefig('pmd_array',facecolor = 'grey')
#plt.show()
