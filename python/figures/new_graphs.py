from pygraph import dgraph

def binary_tree(i, limit=7):
	"""for a complete tree, try limits: 1, 3, 7, 15, 31, 63, 127, 255 .. etc"""

	node = i
	left = 2 * i
	right = 2 * i + 1

	g.add_relation('{} l {}'.format(node, left))
	g.add_relation('{} r {}'.format(node, right))

	if i < limit:
		i = i + 1
		binary_tree(i)

g = dgraph.PyGraph()
binary_tree(1)
g.draw_graph("binary-tree", orientation='TB')
