import bsd
import numpy as np
import matplotlib.pyplot as plt

data = np.load('data/testData1.npy')

bsdata = bsd.bsdata(data)
fig = plt.figure()
ax1 = fig.add_subplot(121)
ax1.imshow(bsdata.vd_map_ng)
ax2 = fig.add_subplot(122)
ax1.axis('off')
ax1.title.set_text('V-disparity Map')
ax2.axis('off')
ax2.title.set_text('Gaussian Filter Response')
ax2.imshow(bsdata.vd_ng_fr)
plt.savefig('tdr_result',facecolor='xkcd:grey')

fig2 = plt.figure()
ax1 = fig2.add_subplot(121)
ax2 = fig2.add_subplot(122)
ax1.imshow(bsdata.vd_ng_fr)
ax2.imshow(bsdata.vd_ng_fr_thresh)
ax1.axis('off')
ax1.title.set_text('Gaussian Filter Response')
ax2.axis('off')
ax2.title.set_text('Thresholding Result')
#plt.show()
plt.savefig('tdr_thresh_result',facecolor='xkcd:grey')
