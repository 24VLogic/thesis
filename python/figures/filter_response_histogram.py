from bsd import bsdata, get_v_disparity_map, get_threshold,get_threshold_tozero
import numpy as np
import matplotlib.pyplot as plt
data = np.load('data/testData1.npy')

bsd1 = bsdata(data)
'''nonzero_vmap_unfiltered = bsd1.vd_map_ng.flatten()[bsd1.vd_map_ng.flatten()>1]
nonzero_vmap_filtered = bsd1.vd_ng_fr.flatten()[bsd1.vd_ng_fr.flatten()>1]
vmap_unfiltered =  bsd1.vd_map_ng.flatten()
vmap_filtered = bsd1.vd_ng_fr.flatten()
vmap_unfiltered_norm =  bsd1.vd_map_ng.flatten()/np.max(bsd1.vd_map_ng.flatten())
vmap_filtered_norm = bsd1.vd_ng_fr.flatten()/np.max(bsd1.vd_ng_fr.flatten())
nonzero_vmap_unfiltered_norm = nonzero_vmap_unfiltered/np.max(nonzero_vmap_unfiltered)
nonzero_vmap_filtered_norm = nonzero_vmap_filtered/np.max(nonzero_vmap_filtered)

unfiltered_histogram = np.histogram(nonzero_vmap_unfiltered_norm,bins=100)
filtered_histogram = np.histogram(nonzero_vmap_filtered_norm,bins=100)
vmap_ng_norm = bsd1.vd_map_ng/np.max(bsd1.vd_map_ng)
vmap_ng_fr_norm = bsd1.vd_ng_fr/np.max(bsd1.vd_ng_fr)'''
def get_histogram_analysis(input_image):
    input_image_flattened = input_image.flatten()/np.max(input_image)
    input_image_flattened_nonzero = input_image_flattened[input_image.flatten().nonzero()]/np.max(input_image)
    histogram, bins = np.histogram(input_image_flattened,100)
    nonzero_histogram, nonzero_bins = np.histogram(input_image_flattened_nonzero,100)
    return({'counts':histogram,'bins':bins,'nonzero_counts':nonzero_histogram,'nonzero_bins':nonzero_bins})

fig = plt.figure('fig',facecolor = 'xkcd:grey')
ax1 = fig.add_subplot(131,facecolor = 'xkcd:beige')
ax2 = fig.add_subplot(132)
ax3 = fig.add_subplot(133)
fig.suptitle(r'$v$-Disparity Map: Filter Response')
#ax1.imshow()
#ax2.imshow()
unfiltered = get_histogram_analysis(bsd1.vd_map_ng)
filtered = get_histogram_analysis(bsd1.vd_ng_fr)
threshold = 0.45
vd_fr_norm = bsd1.vd_ng_fr/np.max(bsd1.vd_ng_fr)
vd_fr_norm_thresh = get_threshold_tozero(vd_fr_norm,threshold)
fr_thresh_analysis = get_histogram_analysis(vd_fr_norm_thresh)

vd_map_ng_norm = bsd1.vd_map_ng/np.max(bsd1.vd_map_ng)
vd_map_ng_norm_thresh = get_threshold_tozero(vd_map_ng_norm,threshold)
ax1.set_xticklabels([])

ax1.plot(filtered['nonzero_counts'])
ax1.plot([threshold*100,threshold*100],[0,np.max(filtered['nonzero_counts'])],color = 'r')
ax2.imshow(vd_fr_norm)
ax3.imshow(vd_fr_norm_thresh)
#ax2.plot(filtered['nonzero_counts'])
fig = plt.figure('fig2',facecolor = 'xkcd:grey')
fig.suptitle(r'$v$-Disparity Map')

ax1 = fig.add_subplot(131,facecolor = 'xkcd:beige')
ax1.set_xticklabels([])

ax2 = fig.add_subplot(132)
ax3 = fig.add_subplot(133)
ax1.plot(unfiltered['nonzero_counts'])
ax1.plot([threshold*100,threshold*100],[0,np.max(unfiltered['nonzero_counts'])],color = 'r')
ax2.imshow(vd_map_ng_norm)
ax3.imshow(vd_map_ng_norm_thresh)
plt.show()
