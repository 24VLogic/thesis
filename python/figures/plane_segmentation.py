import numpy as np
from bsd import get_u_disparity_map, get_horizontal_lines, get_threshold, get_v_disparity_map, get_vertical_lines

distance_image = np.full((100,100),100.0)
test_image = np.load('data/testData1.npy')[0]

'''for m in range(50):
    distance_image[-m] = m'''

for m in range(25,76):
    for n in range(25,76):
        distance_image[m,n]=n/1.5
#distance_image2 = np.copy(distance_image1)
'''for m in range(15,45):
    for n in range(15,45):
        distance_image[m,n] = 30 - (30-m)**2 - (30-n)**2'''
def show_plane_segmentation_figures(distance_image):
    import matplotlib.pyplot as plt
    bins = [10,5,3]
    vd_map1 = get_v_disparity_map(distance_image,bins[0])
    vd_map2 = get_v_disparity_map(distance_image,bins[1])
    vd_map3 = get_v_disparity_map(distance_image,bins[2])
    vd_maps = [vd_map1,vd_map2,vd_map3]

    v_lines = []
    for i, vd_map in enumerate(vd_maps):
        thresh = get_threshold(vd_map,1)
        #axes[i].imshow(thresh)
        vertical_lines = get_vertical_lines(thresh, votes=5,r_resolution = 0.1, verbose = False)
        v_lines.append(vertical_lines)
        '''for line in vertical_lines:
            axes[i].plot(line['d'],line['y'], color = 'r')'''

    ud_map1 = get_u_disparity_map(distance_image, bins[0])
    ud_map2 = get_u_disparity_map(distance_image, bins[1])
    ud_map3 = get_u_disparity_map(distance_image,bins[2])

    ud_maps = [ud_map1,ud_map2,ud_map3]

    h_lines = []
    for i, ud_map in enumerate(ud_maps):
        thresh = get_threshold(ud_map,1)
        #axes[i].imshow(thresh)
        horizontal_lines = get_horizontal_lines(thresh, votes = 5,r_resolution = 0.1)
        h_lines.append(horizontal_lines)
        '''for line in horizontal_lines:
            axes[i].plot(line['x'],line['d'], color = 'r')'''
    #plt.savefig('plane_segmentation_ud',facecolor='xkcd:grey')
    pair_maps = []
    for i in range(len(h_lines)):
        pairs = []
        for h_line in h_lines[i]:
            for v_line in v_lines[i]:
                if np.average(h_line['d']) == np.average(v_line['d']):
                    pairs.append({'x':[h_line['x'][0],h_line['x'][-1]],'y':[v_line['y'][0],v_line['y'][-1]]})
        pair_maps.append(pairs)

    plot_pair_maps = []
    for i,pairs in enumerate(pair_maps):
        plot_pairs = []
        for pair in pairs:
            x1 = pair['x'][0]
            x2 = pair['x'][-1]
            y1 = pair['y'][0]
            y2 = pair['y'][-1]
            plot_pairs.append([[x1,x2,x2,x1,x1],[y1,y1,y2,y2,y1]])
        plot_pair_maps.append(plot_pairs)
    fig1, axes1 = plt.subplots(2,2,figsize = (6,6))
    fig2, axes2 = plt.subplots(2,2,figsize = (6,6))
    fig3, axes3 = plt.subplots(2,2,figsize = (6,6))
    ax = [axes1.flatten(),axes2.flatten(),axes3.flatten()]
    fig = [fig1,fig2,fig3]
    for i, f in enumerate(fig):
        f.suptitle('Number of Bins: ' + str(bins[i]))
        f.set_facecolor('xkcd:grey')
    for i, ax in enumerate(ax):
        for a in ax:
            a.set_xticklabels([])
            a.set_yticklabels([])
        ax[1].imshow(distance_image,cmap = 'viridis_r')
        ax[0].imshow(get_threshold(vd_maps[i],1))
        ax[0].set_aspect(0.25)
        ax[3].imshow(get_threshold(ud_maps[i],1))
        ax[3].set_aspect(1/0.25)
        ax[2].imshow(distance_image,cmap = 'gray_r')
        for plot_pair in plot_pair_maps[i]:
            ax[2].plot(plot_pair[0],plot_pair[1], color = 'r',alpha = 0.8)
        for line in h_lines[i]:
            ax[3].plot(line['x'],line['d'], color = 'r', alpha = 0.8)
        for line in v_lines[i]:
            ax[0].plot(line['d'],line['y'], color = 'r', alpha = 0.8)
    #ax.title.set_text('Simulated Distance Image')
    #plt.savefig('plane_segmentation_distance',facecolor = 'xkcd:grey')
    #plt.imshow(ud_map)
    #plt.show()
    for i, f in enumerate(fig):
        f.suptitle('Number of Bins: ' + str(bins[i]))

        f.savefig('plane_seg_'+str(i),facecolor = 'xkcd:grey')
#show_plane_segmentation_figures(distance_image)
#show_plane_segmentation_figures(test_image)

def plane_segmentation_bsd_uv(bsd,ud_bins):
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    for i, ud_bin_n in enumerate(ud_bins):
        if bsd.ud_bins != ud_bin_n:
                bsd.ud_bins = ud_bin_n
                bsd.vd_bins = ud_bin_n
        #bsd.distance_image = bsd.z_image
        bsd.update_frame()
        figure, axes = plt.subplots(2,2)
        figure.set_size_inches(6,6)
        axes = axes.flatten()
        for ax in axes:
            ax.set_xticklabels([])
            ax.set_yticklabels([])
        axes[0].imshow(bsd.vd_ng_fr)
        axes[1].imshow(bsd.d_ng)
        axes[3].imshow(bsd.ud_ng_fr)
        '''for line in bsd.ud_ng_lines:
            axes[3].plot(line['x'],line['d'], color = 'r')
        for line in bsd.vd_ng_lines:
            axes[0].plot(line['d'],line['y'], color = 'r')'''
        colors = cm.jet(np.linspace(0, 1, len(bsd.uv_planes)))
        for n, plane in enumerate(bsd.uv_planes):
            x1 = plane['uu'][0][0]
            x2 = plane['uu'][0][-1]
            y1 = plane['vv'].T[0][0]
            y2 = plane['vv'].T[0][-1]
            d = np.average(plane['dd'])
            axes[2].plot([x1,x2,x2,x1,x1],[y1,y1,y2,y2,y1],color = colors[n],alpha = 0.8)
            axes[0].plot([d,d],[y1,y2], color = colors[n],alpha = 0.8)
            axes[3].plot([x1,x2],[d,d], color = colors[n],alpha = 0.8)
            #axes[2].plot([plane['uu'][0][0],plane['uu'][0][-1]],[plane['vv'].T[0][0],plane['vv'].T[0][-1]])
        axes[2].imshow(bsd.distance_image,cmap = 'gray_r',alpha = 0.8)
        figure.suptitle('Number of Bins: ' + str(ud_bin_n))
        #figure.savefig('test_data_plane_seg' + str(i),facecolor = 'xkcd:grey')
    plt.show()
def plane_segmentation_bsd_uv_results(bsd,ud_bins):
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    for i, ud_bin_n in enumerate(ud_bins):
        if bsd.ud_bins != ud_bin_n:
                bsd.ud_bins = ud_bin_n
                bsd.vd_bins = ud_bin_n
        #bsd.distance_image = bsd.z_image
        bsd.update_frame()
        #figure, axes = plt.subplots(2,2)
        #figure.set_size_inches(6,6)
        #axes = axes.flatten()
        '''for ax in axes:
            ax.set_xticklabels([])
            ax.set_yticklabels([])
        axes[0].imshow(bsd.vd_ng_fr)
        axes[1].imshow(bsd.d_ng)
        axes[3].imshow(bsd.ud_ng_fr)'''
        '''for line in bsd.ud_ng_lines:
            axes[3].plot(line['x'],line['d'], color = 'r')
        for line in bsd.vd_ng_lines:
            axes[0].plot(line['d'],line['y'], color = 'r')'''
        figure = plt.figure()
        ax = figure.add_subplot(111)
        ax.imshow(bsd.distance_image,cmap = 'gray_r',alpha = 0.8)

        colors = cm.jet(np.linspace(0, 1, len(bsd.uv_planes)))
        for n, plane in enumerate(bsd.uv_planes):
            x1 = plane['uu'][0][0]
            x2 = plane['uu'][0][-1]
            y1 = plane['vv'].T[0][0]
            y2 = plane['vv'].T[0][-1]
            d = np.average(plane['dd'])
            ax.plot([x1,x2,x2,x1,x1],[y1,y1,y2,y2,y1],color = colors[n],alpha = 0.8)

            #axes[0].plot([d,d],[y1,y2], color = colors[n],alpha = 0.8)
            #axes[3].plot([x1,x2],[d,d], color = colors[n],alpha = 0.8)
            #ax.plot([plane['uu'][0][0],plane['uu'][0][-1]],[plane['vv'].T[0][0],plane['vv'].T[0][-1]])
        #figure.suptitle('Number of Bins: ' + str(ud_bin_n))
        figure.savefig('test_data_plane_seg_results' + str(i),facecolor = 'xkcd:grey')
    plt.show()

def plane_segmentation_bsd_xy(bsd,z_bins):
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    for i, z_bin_n in enumerate(z_bins):
        bsd.z_bins = z_bin_n
        #bsd.distance_image = bsd.z_image
        bsd.update_frame()
        figure, axes = plt.subplots(2,2)
        figure.set_size_inches(6,6)
        axes = axes.flatten()
        for ax in axes:
            pass
            #ax.set_xticklabels([])
            #ax.set_yticklabels([])
        axes[0].imshow(bsd.yz_ng_fr_thresh)
        axes[1].imshow(bsd.d_ng)
        axes[3].imshow(bsd.xz_ng_fr_thresh)
        for line in bsd.yz_ng_lines:
            axes[0].plot(line['d'],line['y'], color = 'r',alpha = 0.5)
        for line in bsd.xz_ng_lines:
            axes[3].plot(line['x'],line['d'], color = 'r',alpha = 0.5)
        #colors = cm.jet(np.linspace(0, 1, len(bsd.uv_planes)))
        '''for n, plane in enumerate(bsd.uv_planes):
            x1 = plane['uu'][0][0]
            x2 = plane['uu'][0][-1]
            y1 = plane['vv'].T[0][0]
            y2 = plane['vv'].T[0][-1]
            d = np.average(plane['dd'])
            axes[2].plot([x1,x2,x2,x1,x1],[y1,y1,y2,y2,y1],color = colors[n],alpha = 0.8)
            axes[0].plot([d,d],[y1,y2], color = colors[n],alpha = 0.8)
            axes[3].plot([x1,x2],[d,d], color = colors[n],alpha = 0.8)
            #axes[2].plot([plane['uu'][0][0],plane['uu'][0][-1]],[plane['vv'].T[0][0],plane['vv'].T[0][-1]])'''
        colors =cm.jet(np.linspace(0,1,len(bsd.xy_plane_masks)))
        total_mask = np.zeros(bsd.distance_image.shape)
        for plane_mask_number, plane_mask in enumerate(bsd.xy_plane_masks):
            #axes[2].imshow(plane_mask*np.full(plane_mask.shape,plane_mask_number),alpha = 0.8)

            total_mask += plane_mask*plane_mask_number
        for m, row in enumerate(total_mask):
            for n, pixel in enumerate(row):
                if pixel == 0:
                    total_mask[m,n] = np.nan
        axes[2].imshow(bsd.distance_image,cmap = 'gray_r')
        axes[2].imshow(total_mask,alpha = 0.8)
        figure.suptitle('Number of Bins: ' + str(z_bin_n))
        figure.savefig('test_data_plane_seg_xyz' + str(i),facecolor = 'xkcd:grey')
    plt.show()
import bsd
data = np.load('data/testData1.npy')
bsd1 = bsd.bsdata(data)
#plane_segmentation_bsd_xy(bsd1,[40,20,10])
plane_segmentation_bsd_uv_results(bsd1,[40,20,10])
