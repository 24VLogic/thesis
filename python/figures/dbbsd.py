import sys
import o3d3xx
import matplotlib.pyplot as plt
import numpy as np
import time
import cv2
from scipy.ndimage.filters import gaussian_filter as gf

def get_vDisparity(distanceImage, bins = 30, mask = np.array([None])):
    distanceImageScaled = bins*distanceImage/np.max(distanceImage)
    distanceImageScaled = distanceImageScaled.astype(int)
    dMap = np.zeros((distanceImageScaled.shape[0],bins+1))
    for m,row in enumerate(distanceImageScaled):
        for n, pixel in enumerate(row):
            if mask.any() != None:
                dMap[m,pixel] +=1 & np.invert(mask[m,n])
            else:
                dMap[m,pixel] +=1
    return({'image':dMap/np.max(dMap),'bins':bins})
def get_uDisparity(distanceImage, bins = 30, mask = np.array([None])):
    distanceImageScaled = bins*distanceImage/np.max(distanceImage)
    distanceImageScaled = distanceImageScaled.astype(int)
    dMap = np.zeros((bins+1,distanceImageScaled.shape[1]))
    for n,column in enumerate(distanceImageScaled.T):
        for m, pixel in enumerate(column):
            if mask.any() != None:
                dMap[pixel,n] +=1 & np.invert(mask[m,n])
            else:
                dMap[pixel,n] +=1
    return({'image':dMap/np.max(dMap),'bins':bins})
def get_groundMask(yImage, bins = 15):
    yImageScaled = yImage
    counts,bounds = np.histogram(yImageScaled.flatten(),bins)
    nGroundPoints = np.max(counts)
    #yBounds = (bounds[np.argmax(counts)],bounds[np.argmax(counts)+1])
    yBounds = (bounds[-2],bounds[-1])
    #yBounds = (bounds[np.argmax(counts)],bounds[-1])
    groundMask= np.zeros((yImage.shape),dtype=bool)
    for m,row in enumerate(yImageScaled):
        for n,pixel in enumerate(row):
            if yBounds[0] <= pixel <= yBounds[1]:
                groundMask[m,n] = True
    return({'mask':groundMask,'bins':bins})

def gaussian_filter(inImage, sigma = 1, order = [0,0]):
    imageOut = {}
    imageOut['image'] = 1-gf(inImage,sigma = sigma,order = order)
    imageOut['sigma'] = sigma
    imageOut['order'] = order
    return(imageOut)

def filter_response(image,sigma = 0.5):
    imxx = np.copy(image)
    imxy = np.copy(image)
    imyy = np.copy(image)
    for i in range(3):
        imxx = gaussian_filter(imxx,sigma,[0,2])['image']
        imxy = gaussian_filter(imxy,sigma,[1,1])['image']
        imyy = gaussian_filter(imyy,sigma,[2,0])['image']
    imDiff = np.zeros(image.shape)
    for y, row in enumerate(image):
        for x, value in enumerate(row):
            inspect = [imxx[y,x],imxy[y,x],imyy[y,x]]
            highestFilterResponse = max(inspect)
            lowestFilterResponse = min(inspect)
            imDiff[y,x] = highestFilterResponse - lowestFilterResponse
    return(imDiff/np.max(imDiff))
def thresh(inImage,threshold,kw = cv2.THRESH_BINARY):
    ret,imageOut = cv2.threshold(inImage,threshold,1,kw)
    return(imageOut.astype('uint8'))
'''def get_vertical_lines(inImage, votes = 5, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    vLines = []
    if len(lines>0):
        for line in lines:
            ys = []

            r, theta = line[0]
            if theta == 0:
                #print(inImage.T[int(r)])
                ys = [i for i, p in enumerate(inImage.T[int(r)]) if p >0]
            rs = [r]*len(ys)
            if ys:
                vLines.append({'d':rs,'y':ys})
    return(vLines)'''
'''def get_horizontal_lines(inImage, votes = 5, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    hLines = []
    if len(lines)>0:
        for line in lines:
            xs = []
            r, theta = line[0]
            #print(r,int(np.degrees(theta)))
            if int(np.degrees(theta)) == 90:
                row = inImage[int(r)]
                xArrs = []
                x = []
                for i, pixel in enumerate(row):
                    if pixel > 0:
                        x.append(i)
                    else:
                        if x:
                            xArrs.append(x)
                            x = []
                print(xArrs)
                x_arrays=[x[x!=0] for x in np.split(row, np.where(row==0)[0]) if len(x[x!=0])]
                test = [x[x!=0] for x in np.split(row, np.where(row==0)[0]) if len(x[x!=0])]
                xs = [i for i, p in enumerate(inImage[int(r)]) if p >0]

                #print(x_arrays)
                for x_array in x_arrays:
                    pass
                    #print(r)
                    #print(x_array)
            rs = [r]*len(xs)
            if xs:
                pass
                #hLines.append({'d':rs,'x':xs})
            for arr in xArrs:
                hLines.append({'d':[r]*len(arr),'x':arr})
        #print(hLines)
    return(hLines)'''
def get_vertical_lines(inImage, votes = 5, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    vLines = []
    if len(lines>0):
        for line in lines:
            r, theta = line[0]
            if theta == 0:
                row = inImage.T[int(r)]
                yArrs = []
                y = []
                for i, pixel in enumerate(row):
                    if pixel > 0:
                        y.append(i)
                    else:
                        if y:
                            yArrs.append(y)
                            y=[]
                for arr in yArrs:
                    vLines.append({'d':[r]*len(arr),'y':arr})
        return(vLines)
def get_horizontal_lines(inImage, votes = 5, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    hLines = []
    if len(lines)>0:
        for line in lines:
            r, theta = line[0]
            if int(np.degrees(theta)) == 90:
                row = inImage[int(r)]
                xArrs = []
                x = []
                for i, pixel in enumerate(row):
                    if pixel > 0:
                        x.append(i)
                    else:
                        if x:
                            xArrs.append(x)
                            x = []
                for arr in xArrs:
                    hLines.append({'d':[r]*len(arr),'x':arr})
    return(hLines)
def check_lines(uLines,vLines):
    pairIndex = 0
    matchedLines = {}
    for uLine in uLines:
        #print('check')
        #pass
        for vLine in vLines:
            #print(uLine['d'][0],vLine['d'][0])
            if uLine['d'][0] == vLine['d'][0]:
                matchedLines[pairIndex] = {'uLine' : uLine,'vLine' : vLine}
                pairIndex += 1
    return(matchedLines)
def get_planes(pairs):
    planes = {}
    for key, pair in pairs.items():
        print('key',key)
        xVector = pair['uLine']['x']
        yVector = pair['vLine']['y']
        averageDistance = pair['uLine']['d']
        xx,yy = np.meshgrid(xVector,yVector)
        planes[key]={
            'xVector':xVector,
            'yVector':yVector,
            'distance':averageDistance,
            'xMesh':xx,
            'yMesh':yy,
            'zMesh':np.full(xx.shape,averageDistance[0])
            }
    return(planes)

def get_xzHistogram(xImage,zImage,xBins = None,zBins = None,mask = np.array([None])):
    if not xBins:
        xBins = xImage.shape[1]
    if not zBins:
        zBins = zImage.shape[0]
    xSpan = np.max(xImage)-np.min(xImage)
    xOffset = np.min(xImage)
    xN = np.array(xImage,dtype=float)
    xN -= xOffset
    xScale = np.copy(xN)*xBins/xSpan

    zSpan = np.max(zImage)-np.min(zImage)
    zOffset = np.min(zImage)
    zN = np.array(zImage,dtype=float)
    zN -= zOffset

    zScale = np.copy(zN)*zBins/zSpan
    for m, row in enumerate(xScale):
        for n, val in enumerate(row):
            if val == np.max(xScale):
                print(val,zScale[m,n])
    xz = np.zeros((zBins+1,xBins))
    for i in range(len(xScale.flatten())):
        xVal = int(xScale.flatten()[i])-1
        zVal = int(zScale.flatten()[i])
        if mask.any():
            if not mask.flatten()[i]:
                xz[zVal,xVal] +=1
        else:
            xz[zVal,xVal] +=1
    return(xz)
def get_yzHistogram(yImage,zImage,yBins = None,zBins = None,mask = np.array([None])):
    if not yBins:
        yBins = yImage.shape[0]
    if not zBins:
        zBins = zImage.shape[1]
    zSpan = np.max(zImage)-np.min(zImage)
    zOffset = np.min(zImage)
    zN = np.array(zImage,dtype=float)
    zN -= zOffset

    zScale = np.copy(zN)*zBins/zSpan

    ySpan = np.max(yImage)-np.min(yImage)
    yOffset = np.min(yImage)
    yN = np.array(yImage,dtype=float)
    yN -= yOffset

    yScale = np.copy(yN)*yBins/ySpan
    yz = np.zeros((yBins,zBins+1))
    for i in range(len(yScale.flatten())):
        yVal = int(yScale.flatten()[i])-1
        zVal = int(zScale.flatten()[i])
        if mask.any():
            if not mask.flatten()[i]:
                yz[yVal,zVal] +=1
        else:
            yz[yVal,zVal] +=1
    return(yz)

def main():
    data = np.load('data/testData1.npy')
    distanceImage = data[0][:,30:]
    amplitudeImage = data[1][:,30:]
    xImage = data[2][:,30:]
    yImage = data[3][:,30:]
    zImage = data[4][:,30:]
    bins = 25
    groundMask = get_groundMask(yImage)

    uDisp = get_uDisparity(distanceImage,bins = bins,mask = groundMask['mask'])
    vDisp = get_vDisparity(distanceImage, bins = bins,mask = groundMask['mask'])
    uResponse = filter_response(uDisp['image'], sigma = 0.75)
    vResponse = filter_response(vDisp['image'],sigma = 0.75)
    vThresh = thresh(vResponse,0.5)
    uThresh = thresh(uResponse,0.4)
    uLines = get_horizontal_lines(uThresh)
    vLines = get_vertical_lines(vThresh)
    pairs = check_lines(uLines,vLines)
    planes = get_planes(pairs)
    #print(uLines)
    #print(vLines)
    #fig = plt.figure()
    fig,axes = plt.subplots(2,2)
    axes = axes.flatten()
    axes[0].imshow(np.hstack((vDisp['image'],vResponse,vThresh)))
    axes[3].imshow(np.vstack((uDisp['image'],uResponse,uThresh)))
    axes[1].imshow(distanceImage)
    axes[2].imshow(groundMask['mask'])
    for line in uLines:
        axes[3].plot(line['x'],line['d'])
    for line in vLines:
        axes[0].plot(line['d'],line['y'])
    planes = get_planes(pairs)
    #print(len(planes))
    for key,plane in planes.items():
        axes[2].plot(plane['xMesh'].flatten(),plane['yMesh'].flatten(),'o','x',0.1)
    plt.show()


if __name__ == '__main__':
    main()
