import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure(facecolor = 'xkcd:grey')
ax = fig.add_subplot(111,aspect = 'equal')
data = np.load('data/testData1.npy')

dist = data[0][20:120,20:120]
x = data[2][20:120,20:120]
y = data[3][20:120,20:120]
z = data[4][20:120,20:120]
ax.set_xticks([])
ax.set_yticks([])
ax.imshow(dist,cmap = 'viridis_r')
#plt.show()
plt.savefig('spatialdist',facecolor = 'xkcd:grey')
fig = plt.figure(facecolor = 'xkcd:grey')

ax3d = fig.add_subplot(111,projection = '3d',aspect = 0.75,facecolor =  'xkcd:grey')
ax3d.plot_surface(x,z,y,shade=False,facecolors=plt.cm.viridis_r((z-z.min())/(z.max()-z.min())))
ax3d.invert_zaxis()
ax3d.set_xticklabels([])
ax3d.set_yticklabels([])
ax3d.set_zticklabels([])
plt.savefig('spatialxyz',facecolor = 'xkcd:grey')

#print('hi')
