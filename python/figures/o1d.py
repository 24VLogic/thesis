import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
fig = plt.figure(facecolor = 'xkcd:grey')
ax = fig.add_subplot(111,facecolor = 'xkcd:white')
ax.set_xlim(0,4*np.pi)
ax.set_ylim(0,4.5)
ax.set_yticks([0,3])
ax.set_xticks(np.arange(0,4*np.pi,np.pi/2))
aValues = np.arange(0,4*np.pi,np.pi/2)
ax.title.set_text("PMD Frequency Modulated Signal Analysis")
labels = [
    '',
    r'$\frac{\pi}{2}$',
    r'$\pi$',
    r'$\frac{3\pi}{2}$',
    r'$2\pi$',
    r'$\frac{\pi}{2}$',
    r'$\pi$',
    r'$\frac{3\pi}{2}$',
    r'$2\pi$'
    ]
ax.set_xticklabels(labels)
ax.set_yticklabels(['','B'])
xs = np.linspace(0,4*np.pi)
ys = np.cos(xs-np.pi/5)+3

ax.plot(xs,ys)

for i,aValue in enumerate(aValues):
    line = Line2D([aValue]*2,[0,np.cos(aValue-np.pi/5)+3],color = 'xkcd:purple')
    ax.add_line(line)
    if i > 0:
        ax.plot([aValue],[np.cos(aValue-np.pi/5)+3],'o', color = 'k',markersize = 2.5)
    if 5 >i > 0:
        aLabel = r'$A_' + str(i)+r'$'
        ax.text(aValue+0.125,np.cos(aValue-np.pi/5)+3,aLabel,color = "k")
    if i >= 5:
        aLabel = r'$A_' + str(i-4)+r'$'
        ax.text(aValue+0.125,np.cos(aValue-np.pi/5)+3,aLabel,color = "k")
line = Line2D([2*np.pi+np.pi/5]*2,[0,np.cos(2*np.pi)+3], linestyle = 'dotted', color = 'xkcd:forest green')
ax.add_line(line)
ax.arrow(2*np.pi,1.5,dx = np.pi/5,dy = 0,shape = 'full',head_width = .1, fc = 'k',length_includes_head=True)
ax.arrow(2*np.pi+np.pi/5,1.5,dx = -np.pi/5,dy = 0,shape = 'full',head_width = .1, fc = 'k',length_includes_head=True)
ax.text(2*np.pi+np.pi/10,1.5,r'$\phi$',horizontalalignment = 'center',verticalalignment = 'bottom')

plt.hlines(y = 3,xmin=0,xmax=4*np.pi,linestyle = 'dashed')


#plt.show()
plt.savefig('frequency_mod_analysis',facecolor = "xkcd:grey")
