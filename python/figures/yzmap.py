import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter as gf
from matplotlib.patches import Rectangle
import cv2
data = np.load('data/testData1.npy')
distanceImage = data[0]
amplitudeImage = data[1]
xImage = data[2]
yImage = data[3]
zImage = data[4]


def get_vzMap(zImage, bins = 30):
    zImageScaled = bins*zImage/np.max(zImage)
    zImageScaled = zImageScaled.astype(int)
    zMap = np.zeros((zImageScaled.shape[1],bins+1))
    for m,row in enumerate(zImageScaled):
        for n, pixel in enumerate(row):
            zMap[m,pixel] +=1
    return(zMap)
def get_vDisparity(distanceImage, bins = 30, mask = np.array([None])):
    distanceImageScaled = bins*distaceImage/np.max(distanceImage)
    distanceImageScaled = distanceImageScaled.astype(int)
    dMap = np.zeros((distanceImageScaled.shape[0],bins+1))
    for m,row in enumerate(distanceImageScaled):
        for n, pixel in enumerate(row):
            if mask.any() != None:
                dMap[m,pixel] +=1 & np.invert(mask[m,n])
            else:
                dMap[m,pixel] +=1
    return(dMap)
def get_uDisparity(distanceImage, bins = 30, mask = np.array([None])):
    distanceImageScaled = bins*distanceImage/np.max(distanceImage)
    distanceImageScaled = distanceImageScaled.astype(int)
    dMap = np.zeros((bins+1,distanceImageScaled.shape[1]))
    for n,column in enumerate(distanceImageScaled.T):
        for m, pixel in enumerate(column):
            if mask != None:
                dMap[pixel,n] +=1 & np.invert(mask[m,n])
            else:
                dMap[pixel,n] +=1
    return(dMap)
def get_uzMap(zImage, bins = 30):
    zImageScaled = bins*zImage/np.max(zImage)
    zImageScaled = zImageScaled.astype(int)
    zMap = np.zeros((bins+1,zImageScaled.shape[1]))
    for n,column in enumerate(zImageScaled.T):
        for m, pixel in enumerate(column):
            zMap[pixel,n] +=1
    return(zMap)
def get_groundMask(yImage, bins = 10):
    yImageScaled = bins*yImage/np.max(yImage)
    yImageScaled = yImageScaled.astype(int)
    counts,bounds = np.histogram(yImageScaled.flatten(),bins)
    nGroundPoints = np.max(counts)
    yBounds = (bounds[np.argmax(counts)],bounds[np.argmax(counts)+1])
    groundMask= np.zeros((yImage.shape),dtype=bool)
    for m,row in enumerate(yImageScaled):
        for n,pixel in enumerate(row):
            if yBounds[0] <= pixel <= yBounds[1]:
                groundMask[m,n] = True
    return(groundMask)
def gaussian_filter(inImage, sigma = 1, order = [0,0]):
    imageOut = {}
    imageOut['image'] = 1-gf(inImage,sigma = sigma,order = order)
    imageOut['sigma'] = sigma
    imageOut['order'] = order
    return(imageOut)

def filter_response(image,sigma = 0.75):
    imxx = np.copy(image)
    imxy = np.copy(image)
    imyy = np.copy(image)
    for i in range(3):
        imxx = gaussian_filter(imxx,sigma,[0,2])['image']
        imxy = gaussian_filter(imxy,sigma,[1,1])['image']
        imyy = gaussian_filter(imyy,sigma,[2,0])['image']
    imDiff = np.zeros(image.shape)
    for y, row in enumerate(image):
        for x, value in enumerate(row):
            inspect = [imxx[y,x],imxy[y,x],imyy[y,x]]
            highestFilterResponse = max(inspect)
            lowestFilterResponse = min(inspect)
            imDiff[y,x] = highestFilterResponse - lowestFilterResponse
    return(imDiff/np.max(imDiff))
def thresh(inImage,threshold,kw = cv2.THRESH_BINARY):
    ret,imageOut = cv2.threshold(inImage,threshold,1,kw)
    return(imageOut)
def get_vertical_lines(inImage, votes = 10, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    vLines = []
    if len(lines>0):
        for line in lines:
            ys = []

            r, theta = line[0]
            if theta == 0:
                #print(inImage.T[int(r)])
                ys = [i for i, p in enumerate(inImage.T[int(r)]) if p >0]
            rs = [r]*len(ys)
            if ys:
                vLines.append({'d':rs,'y':ys})
    return(vLines)
def get_horizontal_lines(inImage, votes = 10, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    hLines = []
    if len(lines)>0:
        for line in lines:
            xs = []
            r, theta = line[0]
            #print(r,int(np.degrees(theta)))
            if int(np.degrees(theta)) == 90:
                #print('catch')
                xs = [i for i, p in enumerate(inImage[int(r)]) if p >0]
            rs = [r]*len(xs)
            if xs:
                hLines.append({'d':rs,'x':xs})
        #print(hLines)
    return(hLines)
def check_lines(uLines,vLines):
    pairIndex = 0
    matchedLines = {}
    for uLine in uLines:
        #print('check')
        #pass
        for vLine in vLines:
            #print(uLine['d'][0],vLine['d'][0])
            if uLine['d'][0] == vLine['d'][0]:
                #print('catch')
                matchedLines[pairIndex] = {'uLine' : uLine,'vLine' : vLine}
                pairIndex += 1
    return(matchedLines)
def get_planes(pairs):
    planes = {}
    for key, pair in pairs.items():
        #print(key)
        xVector = pair['uLine']['x']
        yVector = pair['vLine']['y']
        averageDistance = pair['uLine']['d']
        xx,yy = np.meshgrid(xVector,yVector)
        planes[key]={
            'xVector':xVector,
            'yVector':yVector,
            'distance':averageDistance,
            'xMesh':xx,
            'yMesh':yy,
            'zMesh':np.full(xx.shape,averageDistance[0])
            }
        return(planes)
uzMap = get_uzMap(zImage)
vzMap = get_vzMap(zImage)
uMap = get_uDisparity(distanceImage)
groundMask = get_groundMask(yImage,bins = 100)
vMap = get_vDisparity(distanceImage, 30, groundMask)
vLines = get_vertical_lines(thresh(filter_response(vMap),0.3).astype('uint8'))
'''plt.imshow(thresh(filter_response(vMap),0.3))
for line in lines:
    plt.plot(line['d'],line['y'],linewidth = 4, alpha = 0.7)'''
hLines = get_horizontal_lines(thresh(filter_response(uMap),0.3).astype('uint8'),votes=10)
fig = plt.figure()
ax1 = plt.subplot(111)
ax1.imshow(amplitudeImage)

'''for key,pair in check_lines(hLines,vLines).items():
    #print( pair)
    xVector = pair['uLine']['x']
    yVector = pair['vLine']['y']
    rectWidth = abs(xVector[0] - xVector[-1])
    rectHeight = abs(yVector[0] - yVector[-1])
    rect = Rectangle((np.min(xVector),np.min(yVector)),width = rectWidth, height = rectHeight, alpha = 0.7)
    xx,yy = np.meshgrid([xVector[0],xVector[-1]],[yVector[0],yVector[-1]])
    plt.plot(xx.flatten(),yy.flatten(),'o')
    ax1.add_patch(rect)'''
get_planes(check_lines(hLines,vLines))
plt.show()
