import numpy as np
import matplotlib.pyplot as plt

td1 = np.load('data/testData1.npy')
td2 = np.load('data/testData2.npy')

fig = plt.figure()
ax = plt.subplot(111)
ax.set_xticklabels([])
ax.set_xticks([])
ax.set_yticklabels([])
ax.set_yticks([])
ax.imshow(td1[1])
plt.savefig('amplitude1',facecolor = 'xkcd:grey')
#ax.clear()
ax.imshow(td1[0])
plt.savefig('distance1',facecolor = 'xkcd:grey')
ax.imshow(td2[1])
plt.savefig('amplitude2',facecolor = 'xkcd:grey')
ax.imshow(td2[0])
plt.savefig('distance2',facecolor = 'xkcd:grey')
