import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

data = np.load('data/testData1.npy')
dist = data[0]
xx = data[2]
yy = data[3]
zz = data[4]

yScale = 30
yScale = 30
#yyScaled = yScale*yy/np.max(yy)
#yyScaled = yyScaled.astype(int)
yyScaled = yy
counts,bins = np.histogram(zz.flatten(),bins=10)
zBounds = bins[np.argmax(counts)],bins[np.argmax(counts)+1]
counts,bins = np.histogram(yyScaled.flatten(),bins=10)
maxY = np.max(counts)
yBounds = (bins[np.argmax(counts)],bins[np.argmax(counts)+1])
groundDetect = np.copy(yyScaled)
for m,row in enumerate(groundDetect):
    for n,pixel in enumerate(row):
        if yBounds[0] <= pixel <= yBounds[1]:
            groundDetect[m,n] = 1
        else:
            groundDetect[m,n] = 0
wallDetect = np.copy(zz)
for m, row in enumerate(zz):
    for n, pixel in enumerate(row):
        if zBounds[0] <= pixel <= zBounds[1]:
            wallDetect[m,n] = 1
        else:
            wallDetect[m,n] = 0
xs = xx.flatten()
ys = yy.flatten()
zs = zz.flatten()
points = []
groundPoints = []
wallPoints = []
objPoints = []
for i in range(len(xs)):
    if yBounds[0] < ys[i] < yBounds[1]:
        groundBool = True

    else:
        groundBool = False
    if zBounds[0] < zs[i] < zBounds[1]:
        wallBool = True
    else:
        wallBool = False

    if groundBool == True:
        if wallBool == False:
            groundPoints.append([xs[i],ys[i],zs[i]])
    if wallBool == True:
        if groundBool == False:
            wallPoints.append([xs[i],ys[i],zs[i]])
    if wallBool == False:
        if groundBool == False:
            objPoints.append([xs[i],ys[i],zs[i]])

yyFiltered = np.zeros(yy.shape)
for m, row in enumerate(yy):
    for n, pixel in enumerate(row):
        if yBounds[0] <= pixel <= yBounds[1]:
            yyFiltered[m,n] = True
def gen_v_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8')
    vDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] += 1
        vDisparity[i] = hist
    return({'image':vDisparity,'bins':bins})
def gen_v_disparity_filtered(distanceImage,bins,yMask):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8')
    vDisparity = np.zeros((scaledImage.shape[0],bins))
    for m,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for n,pixel in enumerate(row):
            if yMask[m,n] == False:
                hist[pixel] += 1
        vDisparity[m] = hist
    return({'image':vDisparity,'bins':bins})
imV = gen_v_disparity_filtered(dist,bins=30,yMask = yyFiltered)
groundArray = np.zeros((len(groundPoints),3))
for i,point in enumerate(groundPoints):
    groundArray[i][0] = point[0]
    groundArray[i][1] = point[1]
    groundArray[i][2] = point[2]
wallArray = np.zeros((len(wallPoints),3))
for i,point in enumerate(wallPoints):
    wallArray[i][0] = point[0]
    wallArray[i][1] = point[1]
    wallArray[i][2] = point[2]
objArray = np.zeros((len(objPoints),3))
for i,point in enumerate(objPoints):
    objArray[i][0] = point[0]
    objArray[i][1] = point[1]
    objArray[i][2] = point[2]
fig = plt.figure()
ax1 = fig.add_subplot(132)
#ax1.imshow(dist)
ax1.set_xticklabels([])
ax1.set_yticklabels([])
#ax1.set_xticks([])
#ax1.set_yticks([])
'''plt.savefig('ground_detect_dist',facecolor = "xkcd:grey")
ax1.imshow(xx)
plt.savefig('ground_detect_xx',facecolor = "xkcd:grey")
ax1.imshow(yy)
plt.savefig('ground_detect_yy',facecolor = "xkcd:grey")
ax1.imshow(zz)
plt.savefig('ground_detect_zz',facecolor = "xkcd:grey")
ax1.imshow(groundDetect)
plt.savefig('ground_detect_gm',facecolor = "xkcd:grey")'''
yyScaled = 100*yy/np.max(yy)
yyScaled = yyScaled.astype(int)
counts,bins = np.histogram(yyScaled.flatten(),bins=10)
maxY = np.max(counts)
yBounds = (bins[np.argmax(counts)],bins[np.argmax(counts)+1])
fig, (ax2, ax1, ax3) = plt.subplots(1, 3, figsize=(14, 6))
ax1.set_facecolor('xkcd:white')
#ax1.imshow(yyScaled)
ax1.set_yticks(bins)
ax1.set_yticklabels([])
ax1.set_xticks([])
for i, count in enumerate(counts):
    if i != np.argmax(counts):
        ax1.plot([0,count],[bins[i],bins[i]], linewidth = 4, color = 'xkcd:violet')
    else:
        ax1.plot([0,count],[bins[i],bins[i]], linewidth = 4, color = 'xkcd:yellow')
ax1.invert_yaxis()
#ax2=fig.add_subplot(131)
ax2.set_xticks([])
ax2.set_yticks([])
ax2.imshow(yyScaled)
#ax3 = fig.add_subplot(133)
ax3.set_xticks([])
ax3.set_yticks([])
ax3.imshow(groundDetect)
ax1.title.set_text(r'$y$-axis Histogram')
ax2.title.set_text(r'$y$-axis Image')
ax3.title.set_text(r'Ground Plane Mask')
plt.savefig('ground_detect_hist',facecolor = 'xkcd:grey')
fig = plt.figure()
ax = fig.add_subplot(111,projection = '3d')
ax.plot_surface(xx,zz,yy, facecolors=plt.cm.viridis((yy-yy.min())/(yy.max()-yy.min())))
ax.invert_zaxis()
ax.set_xticks([])
ax.set_yticks([])
ax.set_zticks([])
ax.set_facecolor='xkcd:grey'
plt.savefig('ground_detection_surface',facecolor='xkcd:grey')
