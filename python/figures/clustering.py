import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import DBSCAN

data = np.load('data/testData1.npy')

distanceImage = data[0]
amplitudeImage = data[1]
xImage = data[2]
yImage = data[3]
zImage = data[4]

pointsArray = np.vstack((xImage.flatten(),yImage.flatten(),zImage.flatten())).T

counts,bins = np.histogram(yImage.flatten(),bins=10)
maxY = np.max(counts)
yBounds = (bins[np.argmax(counts)],bins[np.argmax(counts)+1])
groundDetect = np.copy(yImage).astype(float)
for m,row in enumerate(groundDetect):
    for n,pixel in enumerate(row):
        if yBounds[0] <= pixel <= yBounds[1]:
            groundDetect[m,n] = np.nan
        else:
            groundDetect[m,n] = 1.0

#Find Indices of Non-Ground Points in Array
indices_ng = []
for i,point in enumerate(pointsArray):
    if yBounds[0] <= point[1] <= yBounds[1]:
        pass
    else:
        indices_ng.append(i)
pointsArray_ng = np.zeros((len(indices_ng),3))
for i, index in enumerate(indices_ng):
    pointsArray_ng[i] = pointsArray[index]
epsilon = 100
db = DBSCAN(eps=epsilon, min_samples=10).fit(pointsArray)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_
unique_labels = set(labels)
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)


db = DBSCAN(eps=epsilon, min_samples=10).fit(pointsArray_ng)
core_samples_mask_ng = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask_ng[db.core_sample_indices_] = True
labels_ng = db.labels_
unique_labels_ng = set(labels_ng)
n_clusters_ng_ = len(set(labels)) - (1 if -1 in labels else 0)

nanArray = np.full(labels.shape,np.nan)
for i,label in enumerate(labels_ng):
    nanArray[indices_ng[i]] = label
labels_ng = nanArray.reshape(zImage.shape)
fig,(ax1,ax2) = plt.subplots(1,2,subplot_kw=dict(projection='3d'))
clustersImage = np.zeros((distanceImage.shape))
clustersImage_ng = np.zeros((distanceImage.shape))
colors = [plt.cm.jet(each)
          for each in np.linspace(0, 1, len(unique_labels))]
'''for k, col in zip(unique_labels, colors):
    if k != -1:
        class_member_mask = (labels == k)
        xy = pointsArray[class_member_mask & core_samples_mask]
        ax1.plot(xy[:,0],xy[:,1],xy[:,2],color = col)'''
labels = labels.reshape(distanceImage.shape)
for k, col in zip(unique_labels, colors):
    if k != -1:
        zzCopy = np.full(zImage.shape,np.nan)
        zzCopy_ng = np.full(zImage.shape,np.nan)
        for m, row in enumerate(labels):
            for n, pixel in enumerate(row):
                if pixel == k:
                    zzCopy[m,n] = zImage[m,n]
                    zzCopy_ng[m,n] = zImage[m,n]
        ax1.plot_wireframe(xImage, yImage, zzCopy, color = col)
        ax2.plot_wireframe(xImage, yImage, zzCopy_ng, color = col)

ax1.invert_yaxis()

'''for k, col in zip(unique_labels_ng, colors):
    if k != -1:
        class_member_mask = (labels_ng == k)
        xy = pointsArray_ng[class_member_mask & core_samples_mask_ng]
        xyLen = len(xy)
        xyShape = xy.shape
        xy = np.unique(xy,axis = 0)
        print(xyLen - len(xy))
        print(xyShape, xy.shape)
        if len(xy) > 2:
            pass
            ax2.plot_trisurf(xy[:,0],xy[:,1],xy[:,2],color = col)'''
ax2.invert_zaxis()
def rip_points(distanceImage):
    distPoints = np.zeros((len(distanceImage.flatten()),3))
    index = 0
    for y, row in enumerate(distanceImage):
        for x, distance in enumerate(row):
            if yBounds[0] <= y <= yBounds[1]:
                pass
            else:
                distPoints[index][0] = x
                distPoints[index][1] = y
                distPoints[index][2] = distance
                index +=1
    return (distPoints)
def mesh_to_points(xx,yy,zz):
    #input xx, yy, zz meshes and return array of shape (n, 3) of form [x, y, z]
    points = np.vstack((xx.flatten(),yy.flatten(),zz.flatten())).T
    return(points)
def dbscan_xyz(points, epsilon, min_samples):
    #Compute DBSCAN and return libray of unique lables, labels, and mask
    db = DBSCAN(eps=epsilon, min_samples = min_samples).fit(distPoints)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_
    unique_labels = set(labels)
    return({'uniqueLabels':unique_labels, 'labels':labels, 'mask':core_samples_mask})

ax2.invert_yaxis()
ax1.set_facecolor('k')
ax2.set_facecolor('k')
fig.set_facecolor('xkcd:grey')

plt.show()
