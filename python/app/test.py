import sys
import o3d3xx
import matplotlib.pyplot as plt
import numpy as np
import time
import cv2
from scipy.ndimage.filters import gaussian_filter as gf

def get_xzHistogram(xImage,zImage,xBins = xImage.shape[1],zBins = zImage.shape[0]):
    xSpan = np.max(xImage)-np.min(xImage)
    xOffset = np.min(xImage)
    xN = np.array(xImage,dtype=float)
    xN -= xOffset

    xScale = np.copy(xN)*xBins/xSpan
    
    zSpan = np.max(zImage)-np.min(zImage)
    zOffset = np.min(zImage)
    zN = np.array(zImage,dtype=float)
    zN -= zOffset

    zScale = np.copy(zN)*zBins/zSpan

    xz = np.zeros(xImage.shape)
    for i in range(len(xScale.flatten())):
        xVal = int(xScale.flatten()[i])-1
        zVal = int(zScale.flatten()[i])-1
        xz[zVal,xVal] +=1
    return(xz)
def get_yzHistogram(yImage,zImage):
    zSpan = np.max(zImage)-np.min(zImage)
    zOffset = np.min(zImage)
    zN = np.array(zImage,dtype=float)
    zN -= zOffset

    zScale = np.copy(zN)*zImage.shape[1]/zSpan

    ySpan = np.max(yImage)-np.min(yImage)
    yOffset = np.min(yImage)
    yN = np.array(yImage,dtype=float)
    yN -= yOffset

    yScale = np.copy(yN)*xImage.shape[0]/ySpan
    xy = np.zeros(yImage.shape)
    for i in range(len(yScale.flatten())):
        yVal = int(yScale.flatten()[i])-1
        zVal = int(zScale.flatten()[i])-1
        xy[yVal,zVal] +=1
    return(xy)
data = np.load('data/testData1.npy')
distanceImage = data[0]
amplitudeImage = data[1]
xImage = data[2]
yImage = data[3]
zImage = data[4]
'''distanceImage = data[0][:,30:]
amplitudeImage = data[1][:,30:]
xImage = data[2][:,30:]
yImage = data[3][:,30:]
zImage = data[4][:,30:]'''
xz = get_xzHistogram(xImage,zImage)
yz = get_yzHistogram(yImage,zImage)
plt.imshow(yz)
plt.show()
