import numpy as np


def RX(matrix, angle):
    #Rotate matrix about x axis
    rx = np.array([1, 0, 0, 0, np.cos(np.radians(angle)), -np.sin(np.radians(angle)), 0, np.sin(np.radians(angle)), np.cos(np.radians(angle))])
    rx = rx.reshape((3,3))

    numberOfPoints = matrix.shape[0]
    newMatrix = np.copy(matrix)
    for i,point in enumerate(newMatrix):
        point = np.matmul(rx,point.T)
        newMatrix[i] = point
    return newMatrix

def RY(matrix, angle):
    #Rotate matrix about y axis
    original_shape = matrix.shape
    matrix = np.reshape(matrix, (-1,3))
    ry = np.array([np.cos(np.radians(angle)), 0, np.sin(np.radians(angle)), 0, 1, 0, -np.sin(np.radians(angle)), 0, np.cos(np.radians(angle))])
    ry = ry.reshape((3,3))

    numberOfPoints = matrix.shape[0]
    newMatrix = np.copy(matrix)
    for i,point in enumerate(newMatrix):
        point = np.matmul(ry,point.T)
        newMatrix[i] = point
    return newMatrix.reshape(original_shape)

def RZ(matrix, angle):
    #Rotate matrix about z axis
    rz = np.array([np.cos(np.radians(angle)), -np.sin(np.radians(angle)), 0, np.sin(np.radians(angle)), np.cos(np.radians(angle)), 0, 0, 0, 1])
    rz = rz.reshape((3,3))

    numberOfPoints = matrix.shape[0]
    newMatrix = np.copy(matrix)
    for i,point in enumerate(newMatrix):
        point = np.matmul(rz,point.T)
        newMatrix[i] = point
    return newMatrix



if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.animation as animation

    myMatrix = np.arange(12).reshape((3,4)).T.astype('float')

    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    ax.set_position([0,0,1,1])
    ax.set_xlim(-12,12)
    ax.set_ylim(-12,12)
    ax.set_zlim(-12,12)

    frames1=[]
    frames2=[]
    frames3=[]

    def update(frame, frames, ax):
        ax.plot(myMatrix[:,0],myMatrix[:,1],myMatrix[:,2],'o',color = 'b')
        ax.plot(frames[frame][:,0],frames[frame][:,1],frames[frame][:,2],'o')

        if not (frame+1)%12:
            ax.cla()
            ax.set_xlim(-12,12)
            ax.set_ylim(-12,12)
            ax.set_zlim(-12,12)

        if frame == len(frames)-1:
            ax.cla()
            ax.set_xlim(-12,12)
            ax.set_ylim(-12,12)
            ax.set_zlim(-12,12)
        return ax

    for i in range(12):
        newMatrix1 = RX(myMatrix,360/12*(i+1))
        #ax.plot(newMatrix[:,0],newMatrix[:,1],newMatrix[:,2],color='r')
        newMatrix2 = RY(myMatrix, 360/12*(i+1))
        #ax.plot(newMatrix[:,0],newMatrix[:,1],newMatrix[:,2],color = 'b')
        newMatrix3 = RZ(myMatrix, 360/12*(i+1))
        #ax.plot(newMatrix[:,0],newMatrix[:,1],newMatrix[:,2], color = 'g')
        frames1.append(newMatrix1)
        frames2.append(newMatrix2)
        frames3.append(newMatrix3)

    frames = frames1 + frames2 + frames3
    anim = animation.FuncAnimation(fig, update, len(frames), fargs=(frames,ax),
                                   interval=100, blit=False,repeat = True)
    #anim.save("rotation.gif",dpi=180,writer="imagemagick")
    plt.show()
