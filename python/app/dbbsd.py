import sys
import o3d3xx
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import time
import cv2
from scipy.ndimage.filters import gaussian_filter as gf
from rotatexyz import RZ
from sklearn.cluster import DBSCAN

def select_blindspot(dataA,dataB):
    xImageA = dataA[2]
    yImageA = dataA[3]
    zImageA = dataA[4]
    groundMask = get_groundMask(yImageA)['mask']
    xImageB, yImageB, zImageB = reorient_mesh(dataB,xOffset = (51*25.4) +20,zOffset = (65*25.4) +240, yOffset = -240,angle = 90)
    xzHist,xscale,xoffset = get_xzHistogram(xImageA,zImageA,zBins = 10,mask = groundMask)
    xthresh = thresh(xzHist/np.max(xzHist),0.4).astype('uint8')
    xlines = get_horizontal_lines(xthresh)
    yzHist,yscale,yoffset = get_yzHistogram(yImageA,zImageA,zBins = 10,mask = groundMask)
    ythresh = thresh(yzHist/np.max(yzHist),0.2).astype('uint8')
    ylines = get_vertical_lines(ythresh)
    pairs = check_lines(xlines,ylines)
    planes = get_planes(pairs)
    selectionX = np.full(xImageB.shape,np.nan)
    selectionY = np.full(yImageB.shape,np.nan)
    selectionZ = np.full(zImageB.shape,np.nan)
    for key, plane in planes.items():
        xBounds = [plane['xVector'][0]*xscale+xoffset,plane['xVector'][-1]*xscale+xoffset]
        yBounds = [plane['yVector'][0]*yscale+yoffset,plane['yVector'][-1]*yscale+yoffset]
        print(xBounds)
        for m, row in enumerate(selectionX):
            for n, value in enumerate(row):
                if np.max(xBounds)> xImageB[m,n]>np.min(xBounds):
                    if np.max(yBounds)> yImageB[m,n]>np.min(yBounds):
                        selectionX[m,n] = xImageB[m,n]
                        selectionY[m,n] = yImageB[m,n]
                        selectionZ[m,n] = zImageB[m,n]
    return(selectionX,selectionY,selectionZ)

def reorient_mesh(data, xOffset,yOffset,zOffset, angle):
    yy = data[3]
    xx = data[2]
    zz = data[4]
    x = xx.flatten()
    y = yy.flatten()
    z = zz.flatten()
    points = np.vstack((x,z,y)).T
    newPoints = RZ(points, angle)
    nxx = newPoints.T[0].reshape(xx.shape)
    nyy = newPoints.T[2].reshape(yy.shape)
    nzz = newPoints.T[1].reshape(zz.shape)
    nxx = nxx + xOffset
    nyy = nyy + yOffset
    nzz = nzz + zOffset
    return(np.array([nxx,nyy,nzz]))

def get_vDisparity(distanceImage, bins = 30, mask = np.array([None])):
    distanceImageScaled = bins*distanceImage/np.max(distanceImage)
    distanceImageScaled = distanceImageScaled.astype(int)
    dMap = np.zeros((distanceImageScaled.shape[0],bins+1))
    for m,row in enumerate(distanceImageScaled):
        for n, pixel in enumerate(row):
            if mask.any() != None:
                dMap[m,pixel] +=1 & np.invert(mask[m,n])
            else:
                dMap[m,pixel] +=1
    return({'image':dMap/np.max(dMap),'bins':bins})
def get_uDisparity(distanceImage, bins = 30, mask = np.array([None])):
    distanceImageScaled = bins*distanceImage/np.max(distanceImage)
    distanceImageScaled = distanceImageScaled.astype(int)
    dMap = np.zeros((bins+1,distanceImageScaled.shape[1]))
    for n,column in enumerate(distanceImageScaled.T):
        for m, pixel in enumerate(column):
            if mask.any() != None:
                dMap[pixel,n] +=1 & np.invert(mask[m,n])
            else:
                dMap[pixel,n] +=1
    return({'image':dMap/np.max(dMap),'bins':bins})
def get_groundMask(yImage, bins = 15):
    yImageScaled = yImage
    counts,bounds = np.histogram(yImageScaled.flatten(),bins)
    nGroundPoints = np.max(counts)
    #yBounds = (bounds[np.argmax(counts)],bounds[np.argmax(counts)+1])
    yBounds = (bounds[-2],bounds[-1])
    #yBounds = (bounds[np.argmax(counts)],bounds[-1])
    groundMask= np.zeros((yImage.shape),dtype=bool)
    for m,row in enumerate(yImageScaled):
        for n,pixel in enumerate(row):
            if yBounds[0] <= pixel <= yBounds[1]:
                groundMask[m,n] = True
    return({'mask':groundMask,'bins':bins,'bounds' : yBounds})

def gaussian_filter(inImage, sigma = 1, order = [0,0]):
    imageOut = {}
    imageOut['image'] = 1-gf(inImage,sigma = sigma,order = order)
    imageOut['sigma'] = sigma
    imageOut['order'] = order
    return(imageOut)

def filter_response(image,sigma = 0.5):
    imxx = np.copy(image)
    imxy = np.copy(image)
    imyy = np.copy(image)
    for i in range(3):
        imxx = gaussian_filter(imxx,sigma,[0,2])['image']
        imxy = gaussian_filter(imxy,sigma,[1,1])['image']
        imyy = gaussian_filter(imyy,sigma,[2,0])['image']
    imDiff = np.zeros(image.shape)
    for y, row in enumerate(image):
        for x, value in enumerate(row):
            inspect = [imxx[y,x],imxy[y,x],imyy[y,x]]
            highestFilterResponse = max(inspect)
            lowestFilterResponse = min(inspect)
            imDiff[y,x] = highestFilterResponse - lowestFilterResponse
    return(imDiff/np.max(imDiff))
def thresh(inImage,threshold,kw = cv2.THRESH_BINARY):
    ret,imageOut = cv2.threshold(inImage,threshold,1,kw)
    return(imageOut.astype('uint8'))
'''def get_vertical_lines(inImage, votes = 5, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    vLines = []
    if len(lines>0):
        for line in lines:
            ys = []

            r, theta = line[0]
            if theta == 0:
                #print(inImage.T[int(r)])
                ys = [i for i, p in enumerate(inImage.T[int(r)]) if p >0]
            rs = [r]*len(ys)
            if ys:
                vLines.append({'d':rs,'y':ys})
    return(vLines)'''
'''def get_horizontal_lines(inImage, votes = 5, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    hLines = []
    if len(lines)>0:
        for line in lines:
            xs = []
            r, theta = line[0]
            #print(r,int(np.degrees(theta)))
            if int(np.degrees(theta)) == 90:
                row = inImage[int(r)]
                xArrs = []
                x = []
                for i, pixel in enumerate(row):
                    if pixel > 0:
                        x.append(i)
                    else:
                        if x:
                            xArrs.append(x)
                            x = []
                print(xArrs)
                x_arrays=[x[x!=0] for x in np.split(row, np.where(row==0)[0]) if len(x[x!=0])]
                test = [x[x!=0] for x in np.split(row, np.where(row==0)[0]) if len(x[x!=0])]
                xs = [i for i, p in enumerate(inImage[int(r)]) if p >0]

                #print(x_arrays)
                for x_array in x_arrays:
                    pass
                    #print(r)
                    #print(x_array)
            rs = [r]*len(xs)
            if xs:
                pass
                #hLines.append({'d':rs,'x':xs})
            for arr in xArrs:
                hLines.append({'d':[r]*len(arr),'x':arr})
        #print(hLines)
    return(hLines)'''
def get_vertical_lines(inImage, votes = 5, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    vLines = []
    if len(lines>0):
        for line in lines:
            r, theta = line[0]
            if theta == 0:
                row = inImage.T[int(r)]
                yArrs = []
                y = []
                for i, pixel in enumerate(row):
                    if pixel > 0:
                        y.append(i)
                    else:
                        if y:
                            yArrs.append(y)
                            y=[]
                for arr in yArrs:
                    vLines.append({'d':[r]*len(arr),'y':arr})
        return(vLines)
def get_horizontal_lines(inImage, votes = 5, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    hLines = []
    if len(lines)>0:
        for line in lines:
            r, theta = line[0]
            if int(np.degrees(theta)) == 90:
                row = inImage[int(r)]
                xArrs = []
                x = []
                for i, pixel in enumerate(row):
                    if pixel > 0:
                        x.append(i)
                    else:
                        if x:
                            xArrs.append(x)
                            x = []
                for arr in xArrs:
                    hLines.append({'d':[r]*len(arr),'x':arr})
    return(hLines)
def check_lines(uLines,vLines):
    pairIndex = 0
    matchedLines = {}
    for uLine in uLines:
        #print('check')
        #pass
        for vLine in vLines:
            #print(uLine['d'][0],vLine['d'][0])
            if uLine['d'][0] == vLine['d'][0]:
                if len(uLine['d']) > 10:
                    if len(vLine['d']) > 10:
                        matchedLines[pairIndex] = {'uLine' : uLine,'vLine' : vLine}
                        pairIndex += 1
    return(matchedLines)
def get_planes(pairs):
    planes = {}
    for key, pair in pairs.items():
        print('key',key)
        xVector = pair['uLine']['x']
        yVector = pair['vLine']['y']
        averageDistance = pair['uLine']['d']
        xx,yy = np.meshgrid(xVector,yVector)
        planes[key]={
            'xVector':xVector,
            'yVector':yVector,
            'distance':averageDistance,
            'xMesh':xx,
            'yMesh':yy,
            'zMesh':np.full(xx.shape,averageDistance[0])
            }
    return(planes)

def get_xzHistogram(xImage,zImage,xBins = None,zBins = None,mask = np.array([None])):
    if not xBins:
        xBins = xImage.shape[1]
    if not zBins:
        zBins = zImage.shape[0]
    xSpan = np.max(xImage)-np.min(xImage)
    xOffset = np.min(xImage)
    xN = np.array(xImage,dtype=float)
    xN -= xOffset
    xScale = np.copy(xN)*xBins/xSpan

    zSpan = np.max(zImage)-np.min(zImage)
    zOffset = np.min(zImage)
    zN = np.array(zImage,dtype=float)
    zN -= zOffset

    zScale = np.copy(zN)*zBins/zSpan
    for m, row in enumerate(xScale):
        for n, val in enumerate(row):
            if val == np.max(xScale):
                print(val,zScale[m,n])
    xz = np.zeros((zBins+1,xBins))
    for i in range(len(xScale.flatten())):
        xVal = int(xScale.flatten()[i])-1
        zVal = int(zScale.flatten()[i])
        if mask.any():
            if not mask.flatten()[i]:
                xz[zVal,xVal] +=1
        else:
            xz[zVal,xVal] +=1
    return(xz,xSpan/xBins,xOffset)
def get_yzHistogram(yImage,zImage,yBins = None,zBins = None,mask = np.array([None])):
    if not yBins:
        yBins = yImage.shape[0]
    if not zBins:
        zBins = zImage.shape[1]
    zSpan = np.max(zImage)-np.min(zImage)
    zOffset = np.min(zImage)
    zN = np.array(zImage,dtype=float)
    zN -= zOffset

    zScale = np.copy(zN)*zBins/zSpan

    ySpan = np.max(yImage)-np.min(yImage)
    yOffset = np.min(yImage)
    yN = np.array(yImage,dtype=float)
    yN -= yOffset

    yScale = np.copy(yN)*yBins/ySpan
    yz = np.zeros((yBins,zBins+1))
    for i in range(len(yScale.flatten())):
        yVal = int(yScale.flatten()[i])-1
        zVal = int(zScale.flatten()[i])
        if mask.any():
            if not mask.flatten()[i]:
                yz[yVal,zVal] +=1
        else:
            yz[yVal,zVal] +=1
    return(yz,ySpan/yBins,yOffset)
def dbcluster(xx,yy,zz,epsilon = 100, min_samples = 10, mask = False):
    pointsArray = np.vstack((xx.flatten(),yy.flatten(),zz.flatten())).T
    if mask == True:
        groundMask= get_groundMask(yy)
        yBounds = groundMask['bounds']
        indicesNonGround = []
        for i,point in enumerate(pointsArray):
            if np.nan in point:
                if yBounds[0] <= point[1] <= yBounds[1]:
                    pass
                else:
                    indicesNonGround.append(i)
        pointsArrayNonGround = np.zeros((len(indicesNonGround),3))
    else:
        indicesNonGround = []
        for i,point in enumerate(pointsArray):
            if not np.isnan(min(point)):
                indicesNonGround.append(i)
        pointsArrayNonGround = np.zeros((len(indicesNonGround),3))
    for i, index in enumerate(indicesNonGround):
        pointsArrayNonGround[i] = pointsArray[index]

    dbNonGround = DBSCAN(eps=100, min_samples=10).fit(pointsArrayNonGround)
    core_samples_mask = np.zeros_like(dbNonGround.labels_, dtype=bool)
    core_samples_mask[dbNonGround.core_sample_indices_] = True
    labelsNonGround = dbNonGround.labels_
    unique_labelsNonGround = set(labelsNonGround)
    nanLabels = np.full(xx.flatten().shape,np.nan)
    for i, index in enumerate(indicesNonGround):
        nanLabels[index] = labelsNonGround[i]
    meshes = {}
    nanLabels = nanLabels.reshape(xx.shape)
    for label in unique_labelsNonGround:
        xMesh = np.full(xx.shape,np.nan)
        yMesh = np.copy(xMesh)
        zMesh = np.copy(xMesh)
        for m, row in enumerate(xMesh):
            for n, value in enumerate(row):
                if nanLabels[m,n] == label:
                    xMesh[m,n] = xx[m,n]
                    yMesh[m,n] = yy[m,n]
                    zMesh[m,n] = zz[m,n]
        meshes[label] = {'xx':xMesh,'yy':yMesh,'zz':zMesh}
    return(meshes,nanLabels)
def main():
    data = np.load('data/testData1.npy')
    distanceImage = data[0][:,30:]
    amplitudeImage = data[1][:,30:]
    xImage = data[2][:,30:]
    yImage = data[3][:,30:]
    zImage = data[4][:,30:]
    bins = 25
    groundMask = get_groundMask(yImage)

    uDisp = get_uDisparity(distanceImage,bins = bins,mask = groundMask['mask'])
    vDisp = get_vDisparity(distanceImage, bins = bins,mask = groundMask['mask'])
    uResponse = filter_response(uDisp['image'], sigma = 0.75)
    vResponse = filter_response(vDisp['image'],sigma = 0.75)
    vThresh = thresh(vResponse,0.5)
    uThresh = thresh(uResponse,0.4)
    uLines = get_horizontal_lines(uThresh)
    vLines = get_vertical_lines(vThresh)
    pairs = check_lines(uLines,vLines)
    planes = get_planes(pairs)
    #print(uLines)
    #print(vLines)
    #fig = plt.figure()
    fig,axes = plt.subplots(2,2)
    axes = axes.flatten()
    axes[0].imshow(np.hstack((vDisp['image'],vResponse,vThresh)))
    axes[3].imshow(np.vstack((uDisp['image'],uResponse,uThresh)))
    axes[1].imshow(distanceImage)
    axes[2].imshow(groundMask['mask'])
    for line in uLines:
        axes[3].plot(line['x'],line['d'])
    for line in vLines:
        axes[0].plot(line['d'],line['y'])
    planes = get_planes(pairs)
    #print(len(planes))
    for key,plane in planes.items():
        axes[2].plot(plane['xMesh'].flatten(),plane['yMesh'].flatten(),'o','x',0.1)
    plt.show()

def test():
    dataA = np.load('testData3a.npy')
    dataB = np.load('testData3b.npy')
    xb,yb,zb = select_blindspot(dataA,dataB)
    x = dataA[2].astype(float)
    y = dataA[3].astype(float)
    z = dataA[4].astype(float)
    groundMask = get_groundMask(y)
    print(groundMask['mask'])
    for m, row in enumerate(groundMask['mask']):
        for n, value in enumerate(row):
            if value == True:
                x[m,n] = np.nan
                y[m,n] = np.nan
                z[m,n] = np.nan
    labelsB,nlb = dbcluster(xb,yb,zb)
    labelsA,nla = dbcluster(x,y,z)
    fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,3))
    '''ax = fig.add_subplot(222,projection = '3d',aspect ='equal')
    ax.invert_zaxis()
    for label,meshes in labelsB.items():
        ax.plot_surface(meshes['xx'],meshes['zz'],meshes['yy'],cstride = 10,rstride = 10, color = 'xkcd:violet')
    for label, meshes in labelsA.items():
        ax.plot_surface(meshes['xx'],meshes['zz'],meshes['yy'],cstride = 10, rstride = 10, color = 'g')
    #plt.imshow(nla)
    ax1 = fig.add_subplot(221,projection='3d',aspect = 'equal')
    ax1.invert_zaxis()
    ax1.plot_wireframe(dataB[2],dataB[4],dataB[3], color = 'r',cstride=10,rstride=10,alpha = 0.5)
    ax1.plot_wireframe(dataA[2],dataA[4],dataA[3], color = 'b',cstride=10,rstride=10,alpha = 0.5)'''
    ax3 = fig.add_subplot(121)
    ax3.imshow(dataA[0],cmap = 'Greys')
    ax3.imshow(nla,alpha = 0.2,cmap = 'jet')
    ax4 = fig.add_subplot(122)
    ax4.imshow(dataB[0],cmap = 'Greys')
    ax4.imshow(nlb,cmap = 'jet')
    ax3.axis('off')
    ax4.axis('off')
    plt.savefig('blindspotMitigation2d', facecolor = 'xkcd:grey')
    plt.show()
if __name__ == '__main__':
    #main()
    test()
