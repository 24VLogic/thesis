import dbbsd
np = dbbsd.np

import timeit

setupRoutine = '''
import dbbsd
np = dbbsd.np

data = np.load('data/testData1.npy')
distanceImage = data[0][:,30:-30]
amplitudeImage = data[1][:,30:-30]
xImage = data[2][:,30:-30]
yImage = data[3][:,30:-30]
zImage = data[4][:,30:-30]
'''
groundMaskRoutine = '''
groundMask = dbbsd.get_groundMask(yImage)
'''
setupxzHist = '''
import dbbsd
np = dbbsd.np

data = np.load('data/testData1.npy')
distanceImage = data[0][:,30:-30]
amplitudeImage = data[1][:,30:-30]
xImage = data[2][:,30:-30]
yImage = data[3][:,30:-30]
zImage = data[4][:,30:-30]
groundMask = dbbsd.get_groundMask(yImage)['mask']
'''

xzHistogramRoutine = '''
xzHistogram = dbbsd.get_xzHistogram(xImage,zImage,zBins = 30,mask = groundMask)
'''
timeData = {}

timeData['groundMask'] = timeit.timeit(setup = setupRoutine, stmt = groundMaskRoutine, number = 1)
timeData['xzHistogram'] = timeit.timeit(setup = setupxzHist, stmt = xzHistogramRoutine,number = 1)
for key, item in timeData.items():
    print(key,item)
