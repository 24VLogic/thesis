import dbbsd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import timeit

data = np.load('data/testData1.npy')
distanceImage = data[0][:,30:-30]
amplitudeImage = data[1][:,30:-30]
xImage = data[2][:,30:-30]
yImage = data[3][:,30:-30]
zImage = data[4][:,30:-30]
groundMask = dbbsd.get_groundMask(yImage,bins = 10)['mask']
xzHistogram = dbbsd.get_xzHistogram(xImage,zImage,zBins = 30,mask = groundMask)
uDisp = dbbsd.get_uDisparity(distanceImage,bins = 30)
ufr = dbbsd.filter_response(xzHistogram,sigma = 0.75)
uthresh = dbbsd.thresh(ufr, threshold = 0.4).astype('uint8')
uLines = dbbsd.get_horizontal_lines(uthresh)
yzHistogram = dbbsd.get_yzHistogram(yImage,zImage,zBins = 30, mask = groundMask)
vfr = dbbsd.filter_response(yzHistogram,sigma = 0.75)
vthresh = dbbsd.thresh(vfr,threshold = 0.2).astype('uint8')
vLines = dbbsd.get_vertical_lines(vthresh)
pairs = dbbsd.check_lines(uLines,vLines)
planes = dbbsd.get_planes(pairs)
print(len(pairs))
for line in vLines:
    pass
    #plt.plot(line['d'],line['y'])
for line in uLines:
    pass
    #plt.plot(line['x'],line['d'])
#plt.imshow(np.vstack((xzHistogram/np.max(xzHistogram),uDisp['image'])))
#plt.imshow(zImage)
fig = plt.figure()
ax = fig.add_subplot(111,projection='3d')
ax.invert_zaxis()
def scaleImage(image, bins):
    offset = np.min(image)
    image -= offset
    span = np.max(image)-np.min(image)
    image = np.array(image,dtype=float)
    scale = image*bins/span
    return(scale)
ax.plot_wireframe(scaleImage(xImage,xImage.shape[1]),scaleImage(zImage,30),scaleImage(yImage,xImage.shape[0]),cstride = 10,rstride = 10)
for key,plane in planes.items():
    print(plane)
    ax.plot_surface(plane['xMesh'],plane['zMesh'],plane['yMesh'],cstride=10,rstride=10)
    #plt.plot(plane['xMesh'].flatten(),plane['yMesh'].flatten(),'o')
#plt.imshow(xImage.astype(float)/np.max(xImage))
plt.show()
