import dbbsd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from rotatexyz import RZ
np = dbbsd.np

def reorient_mesh(data, xOffset,yOffset,zOffset, angle):
    yy = data[3]
    xx = data[2]
    zz = data[4]
    x = xx.flatten()
    y = yy.flatten()
    z = zz.flatten()
    points = np.vstack((x,z,y)).T
    newPoints = RZ(points, angle)
    nxx = newPoints.T[0].reshape(xx.shape)
    nyy = newPoints.T[2].reshape(yy.shape)
    nzz = newPoints.T[1].reshape(zz.shape)
    nxx = nxx + xOffset
    nyy = nyy + yOffset
    nzz = nzz + zOffset
    return(np.array([nxx,nyy,nzz]))

dataA = np.load('testData3a.npy')
dataB = np.load('testData3b.npy')
yy = dataB[3]
xx = dataB[2]
zz = dataB[4]
y = yy.flatten()
x = xx.flatten()
z = zz.flatten()
points = np.vstack((x,z,y)).T
print(points.shape)
newPoints = RZ(points,90)

'''nxx = newPoints.T[0].reshape(xx.shape) + (51*25.4) +10
nyy = newPoints.T[2].reshape(yy.shape) -240
nzz = newPoints.T[1].reshape(zz.shape) + (65*25.4) +240'''

newMesh = reorient_mesh(dataB,xOffset = (51*25.4) +10,zOffset = (65*25.4) +240, yOffset = -240,angle = 90)
nxx = newMesh[0]
nyy = newMesh[1]
nzz = newMesh[2]
groundMask = dbbsd.get_groundMask(dataA[3])
zBins = 30
xzHist = dbbsd.get_xzHistogram(dataA[2],dataA[4],zBins = 10,mask = groundMask['mask'])
xzfr = dbbsd.filter_response(xzHist)
xzthresh = dbbsd.thresh(xzfr,0.5).astype('uint8')
xlines = dbbsd.get_horizontal_lines(xzthresh)

yzHist = dbbsd.get_yzHistogram(dataA[3],dataA[4],zBins = 10, mask = groundMask['mask'])
yzfr = dbbsd.filter_response(yzHist)
yzthresh = dbbsd.thresh(yzfr,0.5).astype('uint8')
ylines = dbbsd.get_vertical_lines(yzthresh)
pairs = dbbsd.check_lines(xlines,ylines)
planes = dbbsd.get_planes(pairs)
uHist = dbbsd.get_uDisparity(dataA[0],bins = 20, mask = groundMask['mask'])
ufr = dbbsd.filter_response(uHist['image'])
uthresh = dbbsd.thresh(uHist['image'],0.30).astype('uint8')
ulines = dbbsd.get_horizontal_lines(uthresh)
vHist = dbbsd.get_vDisparity(dataA[0],bins = 20, mask = groundMask['mask'])
vthresh = dbbsd.thresh(vHist['image'],0.3).astype('uint8')
vlines = dbbsd.get_vertical_lines(vthresh)
pairs = dbbsd.check_lines(ulines,vlines)
planes = dbbsd.get_planes(pairs)


    #plt.plot([plane['xVector'][0],plane['xVector'][-1]],[plane['yVector'][0],plane['yVector'][0]])
    #plt.plot([plane['xVector'][0],plane['xVector'][-1]],[plane['yVector'][-1],plane['yVector'][-1]])
fig = plt.figure()
ax1 = fig.add_subplot(221)
ax1.imshow(vthresh)
ax4 = fig.add_subplot(224)
ax4.imshow(uthresh)
for key,pair in pairs.items():
    ax1.plot(pair['vLine']['d'],pair['vLine']['y'], color = 'r')
    ax4.plot(pair['uLine']['x'],pair['uLine']['d'],color = 'r')
    #print(pair)
ax = fig.add_subplot(222,projection = '3d')
ax3 = fig.add_subplot(223,projection = '3d')
for key, plane in planes.items():
    nanxMesh = np.full(nxx.shape,np.nan)
    nanyMesh = np.full(nyy.shape,np.nan)
    xBounds = [plane['xVector'][0],plane['xVector'][-1]]
    yBounds = [plane['yVector'][0],plane['yVector'][-1]]
    for m,row in enumerate(nxx):
        for n, value in enumerate(row):
            print(value)

'''
for key,plane in planes.items():
    x = [plane['xVector'][0],plane['xVector'][-1]]
    y = [plane['yVector'][0],plane['yVector'][-1]]
    xMeshA = dataA[2][y[0]:y[1],x[0]:x[1]]
    yMeshA = dataA[3][y[0]:y[1],x[0]:x[1]]
    zMeshA = dataA[4][y[0]:y[1],x[0]:x[1]]
    ax.plot_wireframe(xMeshA,zMeshA,yMeshA)'''
'''fig = plt.figure()
ax = fig.add_subplot(111,projection = '3d')
ax.plot_wireframe(dataA[2][:,30:],dataA[4][:,30:],dataA[3][:,30:],cstride=10,rstride=10, alpha = 0.5)
#ax.plot_wireframe(dataB[2],dataB[4],dataB[3],cstride=10,rstride=10)
ax.plot_wireframe(nxx,nzz,nyy,cstride=5,rstride=5, color = 'r',alpha = 0.5)
ax.invert_zaxis()'''
#plt.imshow(nxx)
#plt.imshow(vthresh)
plt.show()
