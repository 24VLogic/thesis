import dbbsd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from rotatexyz import RZ
np = dbbsd.np

def reorient_mesh(data, xOffset,yOffset,zOffset, angle):
    yy = data[3]
    xx = data[2]
    zz = data[4]
    x = xx.flatten()
    y = yy.flatten()
    z = zz.flatten()
    points = np.vstack((x,z,y)).T
    newPoints = RZ(points, angle)
    nxx = newPoints.T[0].reshape(xx.shape)
    nyy = newPoints.T[2].reshape(yy.shape)
    nzz = newPoints.T[1].reshape(zz.shape)
    nxx = nxx + xOffset
    nyy = nyy + yOffset
    nzz = nzz + zOffset
    return(nxx,nyy,nzz)

dataA = np.load('testData3a.npy')
dataB = np.load('testData3b.npy')
yy = dataB[3]
xx = dataB[2]
zz = dataB[4]
y = yy.flatten()
x = xx.flatten()
z = zz.flatten()

newMesh = reorient_mesh(dataB,xOffset = (51*25.4) +10,zOffset = (65*25.4) +240, yOffset = -240,angle = 90)
nxx = newMesh[0]
nyy = newMesh[1]
nzz = newMesh[2]

groundMask = dbbsd.get_groundMask(dataA[3])
zBins = 30
xzHist,xscale,xoffset = dbbsd.get_xzHistogram(dataA[2],dataA[4],zBins = 10,mask = groundMask['mask'])
print(xscale,xoffset)
xthresh = dbbsd.thresh(xzHist/np.max(xzHist),0.4).astype('uint8')
xlines = dbbsd.get_horizontal_lines(xthresh)

yzHist,yscale,yoffset = dbbsd.get_yzHistogram(dataA[3],dataA[4],zBins = 10, mask = groundMask['mask'])
yzthresh = dbbsd.thresh(yzHist/np.max(yzHist),0.2).astype('uint8')
ylines = dbbsd.get_vertical_lines(yzthresh)

pairs = dbbsd.check_lines(xlines,ylines)
selectionX = np.full(nxx.shape,np.nan)
selectionY = np.full(nyy.shape,np.nan)

for key,pair in pairs.items():
    xVector = [pair['uLine']['x'][0]*xscale+xoffset,pair['uLine']['x'][-1]*xscale+xoffset]
    yVector = [pair['vLine']['y'][0]*yscale+yoffset,pair['vLine']['y'][-1]*yscale+yoffset]
    print(xVector)
    print(yVector)
    for m, row in enumerate(nxx):
        for n, value in enumerate(row):
            if np.max(xVector)> nxx[m,n]>np.min(xVector):
                if np.max(yVector)> nyy[m,n]>np.min(yVector):
                    selectionX[m,n] = nxx[m,n]
                    selectionY[m,n] = nyy[m,n]
#plt.imshow(selectionY)
def select_blindspot(dataA,dataB):
    xImageA = dataA[2]
    yImageA = dataA[3]
    zImageA = dataA[4]
    xImageB, yImageB, zImageB = reorient_mesh(dataB,xOffset = (51*25.4) +10,zOffset = (65*25.4) +240, yOffset = -240,angle = 90)
    xzHistogram,xscale,xoffset = get_xzHistogram(xImagaA,zImageA)
    xthresh = thresh(xzHist/np.max(xzHist),0.4).astype('uint8')
    xlines = get_horizontal_lines(xthresh)
    yzHistogram,yscale,yoffset = get_yzHistogram(yImagaA,zImageA)
    ythresh = thresh(yzHist/np.max(yzHist),0.4).astype('uint8')
    ylines = get_horizontal_lines(ythresh)
    pairs = check_lines(xlines,ylines)
    planes = get_planes(pairs)
    selectionX = np.full(xImageB.shape,np.nan)
    selectionY = np.full(yImageB.shape,np.nan)
    selectionZ = np.full(zImageB.shape,np.nan)
    for key, plane in planes.items():
        xBounds = [plane['xVector'][0]*xscale+xoffset,plane['xVector'][-1]*xscale+xoffset]
        yBounds = [plane['yVector'][0]*yscale+yoffset,plane['yVector'][-1]*yscale+yoffset]
        for m, row in enumerate(nxx):
            for n, value in enumerate(row):
                if np.max(Bounds)> nxx[m,n]>np.min(xBounds):
                    if np.max(yBounds)> nyy[m,n]>np.min(yBounds):
                        selectionX[m,n] = xImageB[m,n]
                        selectionY[m,n] = yImageB[m,n]
                        selectionZ[m,n] = zImageB[m,n]
    return(selectionX,selectionY,selectionZ)
fig = plt.figure()
ax = fig.add_subplot(111,projection = '3d')
ax.invert_zaxis()
ax.plot_wireframe(selectionX,nzz,selectionY, color = 'r')
ax.plot_wireframe(dataA[2],dataA[4],dataA[3],cstride = 10, rstride = 10,alpha = 0.5)
#plt.imshow(selectionX)
plt.show()
