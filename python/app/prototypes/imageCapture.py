import o3d3xx
import sys
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

if len(sys.argv) > 1:
    address = sys.argv[1]
else:
    address = '192.168.0.69'

imageWidth = 176
imageHeight = 132

lastTimeStamp = time.time()
startTimeStamp = lastTimeStamp
frameCounter = 0
def save_array(array):
    fileName = input('filename: ')
    np.save(fileName, array)

def save_array_test(array):
    fileName = input('filename: ')
    #np.save('/data/' + fileName + '.npy', array)
    print('/data/' + fileName + '.npy')

def updatefigTest(*args):
    i = args[0]
    axImg = args[1]
    if args[0] > 0:

        axImg.set_array(np.random.rand(imageHeight,imageWidth))
        response = input("s to save, e to exit\n")

        if response.upper() in ['EXIT','E']:
            sys.exit("done")

        if response.upper() in ['S']:
            save_array_test(axImg)

    return axImg

def updatefig(*args):
    i = args[0]
    cam = args[1]
    cam.readNextFrame()
    distImg = args[2]
    dist_max = float(max(np.max(cam.Distance),1))
    if args[0] > 0:

        distImg.set_array(cam.Distance/ dist_max)
        response = input("s to save, e to exit\n")

        if response.upper() in ['EXIT','E']:
            sys.exit("done")

        if response.upper() in ['S']:
            save_array(np.array([cam.Distance,cam.Amplitude,cam.x,cam.y,cam.z]))

    return distImg

def test():
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    axImg = ax1.imshow(np.random.rand(imageHeight,imageWidth))
    ani = animation.FuncAnimation(fig, updatefigTest, interval=50, blit=False, fargs = [axImg,'test'])
    plt.show()

def capture():
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    axImg = ax1.imshow(np.random.rand(imageHeight,imageWidth))
    ani = animation.FuncAnimation(fig, updatefig, interval=50, blit=False, fargs = [o3d,axImg])
    plt.show()

class GrabO3D300():
    def __init__(self,data):
        self.data = data
        self.Amplitude = np.zeros((imageHeight,imageWidth))
        self.Distance = np.zeros((imageHeight,imageWidth))
        self.x = np.zeros((imageHeight,imageWidth))
        self.y = np.zeros((imageHeight,imageWidth))
        self.z = np.zeros((imageHeight,imageWidth))
        self.xyz = np.array([self.x,self.y,self.z])

    def readNextFrame(self):
        result = self.data.readNextFrame()
        self.Amplitude = np.frombuffer(result['amplitude'],dtype='uint16')
        self.Amplitude = self.Amplitude.reshape(imageHeight,imageWidth)
        self.Distance = np.frombuffer(result['distance'],dtype='uint16')
        self.Distance = self.Distance.reshape(imageHeight,imageWidth)
        self.x = np.frombuffer(result['x'],dtype='int16')
        self.x = self.x.reshape(imageHeight,imageWidth)
        self.y = np.frombuffer(result['y'],dtype='int16')
        self.y = self.y.reshape(imageHeight,imageWidth)
        self.z = np.frombuffer(result['z'],dtype='int16')
        self.z = self.z.reshape(imageHeight,imageWidth)
        self.xyz = np.array([self.x,self.y,self.z])
        self.illuTemp = 20.0

if address != 'test':
    camData = o3d3xx.ImageClient(address,50010)
    o3d = GrabO3D300(camData)
    capture()
else:
    test()
