import numpy as np
import matplotlib.pyplot as plt
import cv2

# load image data
img = np.load('boxdist.npy')
img = img[:,30:140].astype('float')
img *= (img.shape[1]-1)/float(img.max())

img2 = np.load('boxamp.npy')
img2 = img2[:,30:140].astype('float')
img2 *= (img2.shape[1]-1)/float(img2.max())


imgInt = np.array(img, dtype = np.uint8)
ret,imgB= cv2.threshold(imgInt,85,255,cv2.THRESH_TRUNC)
img = imgB

for i in range(0):
    img = cv2.GaussianBlur(img,(5,5),0)


vDistm = img.shape[1]
# compute v disparity
vDisparity = np.zeros((img.shape[0],vDistm))
imgScaled = img*((vDistm-1)/255.0)
for i,row in enumerate(imgScaled):
    hist = np.zeros(vDistm)
    #print hist.shape
    for pixel in row:
        #print int(pixel)
        hist[int(pixel)] += 1
    vDisparity[i] = hist
#print vDisparity.shape
#print img.shape
uDistm = img.T.shape[1]
# compute u disparity
uDisparity = np.zeros((img.T.shape[0],uDistm))
imgScaled = img*((uDistm-1)/255.0)
for i,row in enumerate(imgScaled.T):
    hist = np.zeros(uDistm)
    #print hist.shape
    for pixel in row:
        #print int(pixel)
        hist[int(pixel)] += 1
    uDisparity[i] = hist
uDisparity = uDisparity.T

# hough line transform on v disparity map
vDisparity = np.array(vDisparity,dtype=np.uint8)
uDisparity = np.array(uDisparity,dtype=np.uint8)

for i in range(0):
    vDisparity = cv2.GaussianBlur(vDisparity,(3,3),0)
'''
vDisparity = cv2.Canny(vDisparity,4,10,apertureSize = 3)
ret,vDisparity= cv2.threshold(vDisparity,10,255,cv2.THRESH_TRUNC)

minLineLength = 100
maxLineGap = 1

lines = cv2.HoughLinesP(vDisparity,1,np.pi/180,100,minLineLength,maxLineGap)
line_img = np.zeros(vDisparity.shape)+255
for x1,y1,x2,y2 in lines[0]:
    cv2.line(line_img,(x1,y1),(x2,y2),(0,255,0),2)
'''
ret,vDisparity= cv2.threshold(vDisparity,6,255,cv2.THRESH_BINARY)
ret,uDisparity= cv2.threshold(uDisparity,6,255,cv2.THRESH_BINARY)

for i in vDisparity:
    


fig = plt.figure()
plt.suptitle("U-V Disparity Map of O3D Image")
ax1 = plt.subplot2grid((3,3),(0,0),rowspan=2)
ax1.imshow(vDisparity, cmap = 'plasma')
plt.title('V-Disparity Map')
plt.axis('off')
ax2 = plt.subplot2grid((3,3),(0,1),rowspan=2,colspan=2)
ax2.imshow(img,cmap = 'plasma_r')
plt.title('O3D Depth Image')
plt.axis('off')
ax4 = plt.subplot2grid((3,3),(2,1),colspan=2)
ax4.imshow(uDisparity,cmap = 'plasma')
plt.title('U-Disparity Map')
plt.axis('off')
ax3 = plt.subplot2grid((3,3),(2,0))
ax3.imshow(img2,cmap = 'gray')
plt.title('O3D Amplitude Image')
plt.axis('off')

plt.show()
