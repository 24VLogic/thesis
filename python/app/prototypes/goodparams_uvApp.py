import numpy as np
import matplotlib.pyplot as plt
from skimage.morphology import skeletonize
import cv2
import sys

class imageArrays():
    def __init__(self, arr):
        self.d = arr[0].astype('uint16')
        self.d_safe = arr[0].astype('uint16')
        self.a = arr[1].astype('uint16')
        self.x = arr[2].astype('uint16')
        self.y = arr[3].astype('uint16')
        self.z = arr[4].astype('uint16')
        self.v = None
        self.u = None

    def blur_d(self,kernelSize = 3):
        kernel = np.ones((kernelSize,kernelSize))/kernelSize
        self.d = cv2.filter2D(blur, -1, kernel)

    def bf_d(self):
        self.d = cv2.bilateralFilter(distanceImageFloat.astype('float32'),9,75,75)

    def reset_d(self):
        self.d = np.copy(self.d_safe)

    def gen_v_disparity(self,bins=30):
        self.v = gen_v_disparity(self.d,bins)

    def gen_u_disparity(self,bins=30):
        self.u = gen_u_disparity(self.d,bins)

    def thresh_v(self, threshold,key = 'image'):
        if self.v:
            ret, thresh = cv2.threshold(self.v[key],threshold,1,cv2.THRESH_BINARY)
            self.v['thresh'] = thresh

    def thresh_u(self, threshold,key = 'image'):
        if self.u:
            ret, thresh = cv2.threshold(self.u[key],threshold,1,cv2.THRESH_BINARY)
            self.u['thresh'] = thresh

    def blur_v(self, size=3 ,n=1, key = 'image'):
        #kernel = np.ones((size,size))
        kernel = np.zeros((size, size))
        kernel[int((size-1)/2), :] = np.ones(size)
        kernel = kernel.T / size
        blur = np.copy(self.v[key])
        for i in range(n):
            blur = cv2.filter2D(blur, -1, kernel)
        self.v['blur'] = blur

    def blur_u(self, size=3 ,n=1, key = 'image'):
        kernel = np.zeros((size, size))
        kernel[int((size-1)/2), :] = np.ones(size)
        kernel = kernel / size
        blur = np.copy(self.u[key])
        for i in range(n):
            blur = cv2.filter2D(blur, -1, kernel)
        self.u['blur'] = blur

    def lines_v(self, rRes =1, thRes = np.pi/180,votes=10, minLineLength = 10, maxLineGap =1):
        if 'thresh' in self.v:
            self.v['plinesPoints'] = cv2.HoughLinesP(self.v['thresh'].astype('uint8'),rRes,thRes,votes,minLineLength,maxLineGap)
            self.v['linesParams'] = cv2.HoughLines(self.v['thresh'].astype('uint8'),rRes,thRes,votes)
    def lines_u(self, rRes =1, thRes = np.pi/180,votes=10, minLineLength = 10, maxLineGap =1):
        if 'thresh' in self.v:
            self.u['plinesPoints'] = cv2.HoughLinesP(self.u['thresh'].astype('uint8'),rRes,thRes,votes,minLineLength,maxLineGap)
            self.u['linesParams'] = cv2.HoughLines(self.u['thresh'].astype('uint8'),rRes,thRes,votes)

    def get_plines_v(self, n=10):
        self.v['plines'] = {}
        for i,line in enumerate(self.v['plinesPoints']):
            d1,y1,d2,y2 = line[0]
            self.v['plines'][i]={'y' : np.linspace(y1,y2,num = n).astype('uint16'), 'd' : np.linspace(d1,d2,num = n).astype('uint16')}

def gen_v_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8')
    vDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] += 1
        vDisparity[i] = hist
    return({'image':vDisparity,'bins':bins})

def gen_u_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8').T
    uDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] += 1
        uDisparity[i] = hist
    return({'image':uDisparity.T,'bins':bins})

def test():
    fileName = sys.argv[1]
    arrays = np.load(fileName)[:,40:,20:110]
    image = imageArrays(arrays)

    image.gen_v_disparity(30)
    image.thresh_v(8,key='image')
    image.blur_v(n=1,key='thresh')
    image.thresh_v(0.5,key='blur')
    skel = skeletonize(image.v['thresh'])
    image.lines_v(votes=20,minLineLength = 20, maxLineGap = 1,thRes = np.pi/720)

    image.get_plines_v()

    #plt.imshow(image.v['thresh'])
    plt.imshow(image.d)
    for line in image.v['plines'].items():
        lineNumber, lineDict = line
        plt.plot(lineDict['d'],lineDict['y'])
    plt.show()

if __name__ == '__main__':
    test()
    '''fileName = sys.argv[1]
    arrays = np.load(fileName)[:,:,0:110]
    plt.imshow(arrays[0])
    plt.show()'''
