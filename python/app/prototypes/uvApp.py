import itertools
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy.ndimage.filters import gaussian_filter as gf

from mpl_toolkits.mplot3d import Axes3D
from skimage.morphology import skeletonize
import cv2
import sys

class imageArrays():
    def __init__(self, arr):

        self.d = arr[0].astype('uint16')
        self.d_safe = arr[0].astype('uint16')
        self.d_max = self.d_safe.max()
        self.a = arr[1].astype('uint16')
        self.x = arr[2].astype('uint16')
        self.y = arr[3].astype('uint16')
        self.z = arr[4].astype('uint16')
        self.v = None
        self.u = None
        self.image = {}
        self.image['original'] = self.d

    def sample_image(self, inImage, n = 255):
        self.image['sampled'] = {'image':inImage/inImage.max(),'bins':255}
        self.image['sampled']['image'] = self.image['sampled']['image']*bins

    '''def sample_d(self,n = 255):

        self.d = self.d/self.d.max()
        self.d = np.around(self.d*n)'''

    def average_blur(self, inImage, kSize = 3):
        kernel = np.ones((kSize,kSize))/kSize
        self.d = cv2.filter2D(self.d, -1, kernel)
    #def bilateral_blur(self, inImage, )
    '''
    def blur_d(self,kernelSize = 3):
        kernel = np.ones((kernelSize,kernelSize))/kernelSize
        self.d = cv2.filter2D(self.d, -1, kernel)'''
    def gaussian_filter(self, inImage, sigma, order = [0,0]):
        self.image['gaussian'] = {}
        self.image['gaussian']['image'] = gf(inImage,sigma = sigma,order = order)
        self.image['gaussian']['sigma'] = sigma
        self.image['gaussian']['order'] = order
        return(self.image['gaussian']['image'])
        pass
    def bf_d(self):
        self.d = cv2.bilateralFilter(self.d.astype('float32'),9,75,75)

    def reset_d(self):
        self.d = np.copy(self.d_safe)

    def gen_v_disparity(self,bins=30):
        self.v = gen_v_disparity(self.d,bins)

    def gen_u_disparity(self,bins=30):
        self.u = gen_u_disparity(self.d,bins)

    def thresh_v(self, threshold,key = 'image'):
        if self.v:
            ret, thresh = cv2.threshold(self.v[key],threshold,1,cv2.THRESH_BINARY)
            self.v['thresh'] = thresh

    def thresh_u(self, threshold,key = 'image'):
        if self.u:
            ret, thresh = cv2.threshold(self.u[key],threshold,1,cv2.THRESH_BINARY)
            self.u['thresh'] = thresh

    def blur_v(self, size=5 ,n=3, key = 'image',f = 'xx'):
        #kernel = np.ones((size,size))
        xxkernel = np.zeros((size, size))
        xxkernel[int((size-1)/2), :] = np.ones(size)
        xxkernel = xxkernel.T / size
        yykernel = xxkernel.T
        xykernel = np.zeros((size,size))
        for i, row in enumerate(xykernel):
            row[-(i+1)] = 1
        xykernel = xykernel /size
        xg = np.zeros((size,size))
        for row in xg:
            row += gaussian(np.arange(size),size//2,0.5)
        #xg = np.gradient(xg)[1]
        #xg.T[int((size-1)/2), :] = np.ones(size)
        xg = xg/size
        kernels = {'xx':xxkernel,'yy':yykernel,'xy':xykernel,'xg':xg,'yg':xg.T}
        blur = np.copy(self.v[key])
        #print(kernels[f])
        for i in range(n):
            blur = cv2.filter2D(blur, -1, kernel = kernels[f] )
        self.v['blur'] = blur

    def blur_u(self, size=3 ,n=1, key = 'image', f = 'yy'):
        #kernel = np.ones((size,size))
        xxkernel = np.zeros((size, size))
        xxkernel[int((size-1)/2), :] = np.ones(size)
        xxkernel = xxkernel.T / size
        yykernel = xxkernel.T
        xykernel = np.zeros((size,size))
        for i, row in enumerate(xykernel):
            row[-(i+1)] = 1
        xykernel = xykernel /size
        xg = np.zeros((size,size))
        for row in xg:
            row += gaussian(np.arange(size),size//2,0.5)
        #xg = np.gradient(xg)[1]
        #xg.T[int((size-1)/2), :] = np.ones(size)
        xg = xg/size
        kernels = {'xx':xxkernel,'yy':yykernel,'xy':xykernel,'xg':xg,'yg':xg.T}
        blur = np.copy(self.u[key])
        for i in range(n):
            blur = cv2.filter2D(blur, -1, kernel = kernels[f])
        self.u['blur'] = blur

    def lines_v_p(self, rRes =1, thRes = np.pi/180,votes=10, minLineLength = 10, maxLineGap =1):
        if 'thresh' in self.v:
            self.v['plinesPoints'] = cv2.HoughLinesP(self.v['thresh'].astype('uint8'),rRes,thRes,votes,minLineLength,maxLineGap)
            #self.v['linesParams'] = cv2.HoughLines(self.v['thresh'].astype('uint8'),rRes,thRes,votes)
    def lines_u_p(self, rRes =1, thRes = np.pi/180,votes=10, minLineLength = 10, maxLineGap =1):
        if 'thresh' in self.u:
            self.u['plinesPoints'] = cv2.HoughLinesP(self.u['thresh'].astype('uint8'),rRes,thRes,votes,minLineLength,maxLineGap)
            #self.u['linesParams'] = cv2.HoughLines(self.u['thresh'].astype('uint8'),rRes,thRes,votes)

    def lines_u(self,rRes = 1, thRes = np.pi/180,votes = 10):
        self.u['lineParams'] = cv2.HoughLines(self.u['thresh'].astype('uint8'),rRes, thRes, votes)
        self.u['lines'] = {}
        for lineNum,line in enumerate(self.u['lineParams']):
            rho, theta = line[0]
            if 88 < np.degrees(theta) < 92:
                xs = []
                for i, val in enumerate(self.u['thresh'][int(rho)]):
                    if val > 0:
                        xs.append(i)
            self.u['lines'][lineNum]={'x':xs,'d':[rho]*len(xs)}
    def lines_v(self,rRes = 1, thRes = np.pi/180,votes = 10):
        self.v['lineParams'] = cv2.HoughLines(self.v['thresh'].astype('uint8'),rRes, thRes, votes)
        self.v['lines']= {}
        for lineNum,line in enumerate(self.v['lineParams']):
            rho, theta = line[0]
            if np.degrees(theta) == 0:
                ys = []
                for i, val in enumerate(self.v['thresh'].T[int(rho)]):
                    if val > 0:
                        ys.append(i)
            self.v['lines'][lineNum] = {'ys':ys,'d':[rho]*len(ys)}

    def get_plines_v(self, n=100):
        self.v['plines'] = {}
        for i,line in enumerate(self.v['plinesPoints']):
            d1,y1,d2,y2 = line[0]
            self.v['plines'][i]={'y' : np.linspace(y1,y2,num = n).astype('uint16'), 'd' : np.linspace(d1,d2,num = n).astype('uint16')}

    def get_plines_u(self, n=100):
        self.u['plines'] = {}
        for i,line in enumerate(self.u['plinesPoints']):
            x1,d1,x2,d2 = line[0]
            self.u['plines'][i]={'x' : np.linspace(x1,x2,num = n).astype('uint16'), 'd' : np.linspace(d1,d2,num = n).astype('uint16')}

    def match_lines_p(self,delta = 1):
        self.match = {}
        for uline in self.u['plines'].items():
            #print(uline)
            ulineNumber, ulineData = uline
            averageDu = sum(ulineData['d'])/len(ulineData['d'])
            #print(averageDu)
            for vline in self.v['plines'].items():
                vlineNumber, vlineData = vline
                averageDv = sum(vlineData['d'])/len(vlineData['d'])
                if abs(averageDu - averageDv)<=delta:
                    self.match[(ulineNumber,vlineNumber)]={'uline':ulineData,'vline':vlineData}
                    #print(averageDu - averageDv)
    def print_foo(self):
        print('foo')
    def mesh_lines_p(self):
        self.surfs = {}
        self.corners = {}
        for pair in self.match.items():
            matchIndices, lineData = pair
            uline = lineData['uline']
            vline = lineData['vline']
            x,y = np.meshgrid(uline['x'],vline['y'])
            averageD =(sum(uline['d'])+sum(vline['d']))/(uline['d'].size+vline['d'].size)
            d = np.full(x.shape,int(averageD))
            cornerIndices = np.ix_((0,-1),(0,-1))
            self.surfs[matchIndices]={'x':x,'y':y,'d':d}
            self.corners[matchIndices] = {'x':x[cornerIndices],'y':y[cornerIndices],'d':d[cornerIndices]}


    def get_blindspots_p(self):
        self.blindspots_corners = {}
        self.blindspots = {}
        cornerIndices = np.ix_((0,-1),(0,-1))
        for combo in itertools.combinations(self.corners,2):
            first,second = combo
            print('checking',first,second)
            xf1 = min(*self.corners[first]['x'][0])
            xf2 = max(*self.corners[first]['x'][0])
            yf1 = min(*self.corners[first]['y'].T[0])
            yf2 = max(*self.corners[first]['y'].T[0])
            xs1 = min(*self.corners[second]['x'][0])
            xs2 = max(*self.corners[second]['x'][0])
            ys1 = min(*self.corners[second]['y'].T[0])
            ys2 = max(*self.corners[second]['y'].T[0])
            print(xf1,xf2, 'vs',xs1,xs2)
            print(yf1,yf2, 'vs',ys1,ys2)
            xFirstSet = set(range(xf1,xf2+1))
            xSecondRange = range(xs1,xs2+1)
            xIntersect = xFirstSet.intersection(xSecondRange)
            yFirstSet = set(range(yf1,yf2+1))
            ySecondRange = range(ys1,ys2+1)
            yIntersect = yFirstSet.intersection(ySecondRange)
            print(xFirstSet,xSecondRange)
            print('xi',xIntersect)
            print(yFirstSet,ySecondRange)
            print('yi',yIntersect)
            print(combo)
            if xIntersect:
                print('xint true')
            if yIntersect:
                print('yint true')
            if xIntersect:
                if yIntersect:
                    xmesh, ymesh = np.meshgrid(np.asarray(list(xIntersect)),np.asarray(list(yIntersect)))
                    dValues = sorted([np.average(self.corners[first]['d']),np.average(self.corners[second]['d'])])
                    #print(dValues)
                    d1 = np.full(xmesh.shape,dValues[0])
                    d2 = np.full(xmesh.shape,dValues[1])
                    #print(d1,d2)
                    self.blindspots[combo] = {'x':xmesh,'y':ymesh,'d1':d1,'d2':d2}
                    self.blindspots_corners[combo] = {'x':xmesh[cornerIndices],'y':ymesh[cornerIndices],'d1':d1[cornerIndices],'d2':d2[cornerIndices]}
                    print('intersection of',[xf1,xf2] ,'and',[xs1,xs2], 'is', xIntersect)
                    print('xi',np.asarray(list(xIntersect)))
                    print('intersection of',[yf1,yf2] ,'and',[ys1,ys2], 'is', yIntersect)
                    print('yi',np.asarray(list(yIntersect)))
            else:
                if not xIntersect:
                    print('x Intersect Failed')
                    print('no intersection of',[xf1,xf2] ,'and',[xs1,xs2])
                    print('y intersect is',[yf1,yf2], 'and', [ys1,ys2])
                if not yIntersect:
                    print('y Intersect Failed')
                    print('no intersection of',[yf1,yf2],'and',[ys1,ys2])
                    print('x intersect',[xf1,xf2] ,'and',[xs1,xs2])
            print('#######')
    def get_occlusion_zones_p(self):
        self.occlusions = {}
        for i,blindspot in enumerate(self.blindspots_corners.items()):
            blindspotIndex,blindspot = blindspot
            x = blindspot['x'][0]
            y = blindspot['y'].T[0]
            z = [blindspot['d1'].max(),blindspot['d2'].max()]
            xy = np.meshgrid(x,y)
            xz = np.meshgrid(x,z)
            yz = np.meshgrid(y,z)
            #ax.plot_surface(xy[0],xy[1],np.full(xy[0].shape,z[0]))
            xyPlanes = [
                {'x':xy[0],'y':xy[1],'d':np.full(xy[0].shape,z[0])},
                {'x':xy[0],'y':xy[1],'d':np.full(xy[0].shape,z[1])}
                ]
            xzPlanes = [
                {'x':xz[0],'y':np.full(xz[0].shape,y[0]),'d':xz[1]},
                {'x':xz[0],'y':np.full(xz[0].shape,y[1]),'d':xz[1]}
                ]
            yzPlanes = [
                {'x':np.full(yz[0].shape,x[0]),'y':yz[0],'d':yz[1]},
                {'x':np.full(yz[0].shape,x[1]),'y':yz[0],'d':yz[1]}
                ]
            mx = sorted(x)[0]+(sorted(x)[1] - sorted(x)[0])/2
            my = sorted(y)[0]+(sorted(y)[1] - sorted(y)[0])/2
            mz = sorted(z)[0]+(sorted(z)[1] - sorted(z)[0])/2
            self.occlusions[i]={'xy':xyPlanes,'xz':xzPlanes,'yz':yzPlanes,'c':{'x':[mx],'y':[my],'d':[mz]}}
def gen_v_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8')
    vDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] += 1
        vDisparity[i] = hist
    return({'image':vDisparity,'bins':bins})

def gen_u_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8').T
    uDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] += 1
        uDisparity[i] = hist
    return({'image':uDisparity.T,'bins':bins})

def simulated_image():
    image = np.zeros((16,16))
    for i, imageRow in enumerate(image[int(image.shape[0]/2):image.shape[0]+1]):
        imageRow += int(image.shape[0]/2) - i
        #print(int(image.shape[0]/2) - i)
        image[i] += 8
    for i in range(4,12):
        for j in range(4,image.shape[1]-4):
            image[i,j] = 4
    return(image)

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

def makeGaussian(size, fwhm = 3, center=None):
    """ Make a square gaussian kernel.

    size is the length of a side of the square
    fwhm is full-width-half-maximum, which
    can be thought of as an effective radius.
    """

    x = np.arange(0, size, 1, float)
    y = x[:,np.newaxis]

    if center is None:
        x0 = y0 = size // 2
    else:
        x0 = center[0]
        y0 = center[1]

    return np.exp(-4*np.log(2) * ((x-x0)**2 + (y-y0)**2) / fwhm**2)
def test():
    binSize = 30
    #mpl.rcParams['image.cmap'] = 'winter'
    colors = cm.jet_r(np.linspace(0,1,binSize))
    fig,axes = plt.subplots(2,2)
    fileName = sys.argv[1]
    arrays = np.load(fileName)[:,40:,20:110]
    image = imageArrays(arrays)
    image.bf_d()
    image.gen_v_disparity(binSize)
    #plt.imshow(image.v['image'])
    #axes[0,0].imshow(image.v['image'])
    for i in range(1):

        image.blur_v(size = 3,n=3,f = 'xx')
        #image.blur_v(size = 9,n=1,f = 'xy')
        #image.blur_v(size = 9,n=1,f = 'yy')
    for i in range(0):
        image.blur_v(size = 3, f = 'xy')
    image.thresh_v(5,'image')

    #axes[0,1].imshow(image.v['blur'])
    #axes[1,0].imshow(image.v['thresh'])
    #image.thresh_v(8,'blur')
    image.thresh_v(5,'blur')
    #axes[1,1].imshow(image.v['thresh'])
    #plt.show()
    fig2,axes2 = plt.subplots(2)
    image.lines_v(votes = 20)

    '''for line in image.v['lineParams']:
        rho, theta = line[0]
        if np.degrees(theta) == 0:
            ys = []
            for i, val in enumerate(image.v['thresh'].T[int(rho)]):
                if val > 0:
                    ys.append(i)
            axes2[0].plot([rho]*len(ys),ys,color = colors[int(rho)],linewidth = 2)
            print(rho,np.degrees(theta))'''
    image.gen_u_disparity()
    image.blur_u(size = 3,n=3,f = 'yy')
    image.thresh_u(10,'blur')
    image.lines_u(votes = 8)
    '''for line in image.u['lineParams']:
        rho, theta = line[0]
        if 88 < np.degrees(theta) < 92:
            xs = []
            for i, val in enumerate(image.u['thresh'][int(rho)]):
                if val > 0:
                    xs.append(i)
            axes2[1].plot(xs,[rho]*len(xs),color = colors[int(rho)],linewidth = 2)
            print(rho,np.degrees(theta))'''
    #axes2[0].imshow(image.v['thresh'])
    #axes2[1].imshow(image.u['thresh'])
    #plt.show()
    #fig.close()
    im = image.v['image']
    axes[0,0].imshow(im)
    for i in range(3):
        im = image.gaussian_filter(inImage = im,sigma = 1,order = [0,2])
        im = 1-im
    axes[0,1].imshow(im)
    for i in range(3):
        im = image.gaussian_filter(inImage = im,sigma = 1, order = [1,1])
        im = 1-im

    axes[1,0].imshow(im)
    for i in range(3):
        im = image.gaussian_filter(inImage = im,sigma = 1,order = [1,1])
        im = 1-im

    axes[1,1].imshow(im)
    plt.show()
    '''binSize = 30
    for i in range(9):
        pass
        image.bf_d()
    #image.sample_d()
    image.gen_v_disparity(binSize)

    image.blur_v(n=1,key='image')
    image.thresh_v(20,key='blur')
    image.lines_v_p(votes=20,minLineLength = 20, maxLineGap = 1,thRes = np.pi/180)
    image.get_plines_v()


    image.gen_u_disparity(binSize)
    image.blur_u(n=1,key='image')
    image.thresh_u(15,key='blur')
    image.lines_u_p(votes=10,minLineLength = 8, maxLineGap = 10,thRes = np.pi/180)
    image.get_plines_u()

    fig2d, axes = plt.subplots(2,2)

    axes[1,1].imshow(image.u['thresh'])
    image.match_lines_p()

    image.mesh_lines_p()

    axes[0,1].imshow(image.d)
    axes[0,0].imshow(image.v['thresh'])
    axes[1,0].imshow(image.d)
    #print(image.match)
    colors = cm.plasma(np.linspace(0, 1, len(image.surfs.keys())))
    #print('#####', len(image.surfs.keys()))
    for colorNumber,(surfnumber,surf) in enumerate(image.surfs.items()):
        #print(i)
        axes[1,0].plot(surf['x'].flatten(),surf['y'].flatten(),'x',color = colors[colorNumber])
    for colorNumber,(pairIndex,pair) in enumerate(image.match.items()):
        print(colorNumber, pairIndex)
        axes[0,0].plot(pair['vline']['d'],pair['vline']['y'], linewidth = 2,color = colors[colorNumber])
        axes[1,1].plot(pair['uline']['x'],pair['uline']['d'], linewidth = 2,color = colors[colorNumber])
    image.get_blindspots_p()

    fig = plt.figure()
    ax = fig.add_subplot(111,projection = '3d')

    image.get_occlusion_zones_p()
    for i, occlusion in image.occlusions.items():
        #print(occlusion)
        for xyPlane in occlusion['xy']:
            ax.plot_surface(xyPlane['x'],xyPlane['y'],xyPlane['d'],color = 'r',alpha = 0.3,edgecolor = 'xkcd:purple')
        for xzPlane in occlusion['xz']:
            pass
            ax.plot_surface(xzPlane['x'],xzPlane['y'],xzPlane['d'],color = 'r',alpha = 0.3,edgecolor = 'xkcd:purple')
        for xyPlane in occlusion['yz']:
            ax.plot_surface(xyPlane['x'],xyPlane['y'],xyPlane['d'],color = 'r',alpha = 0.3,edgecolor = 'xkcd:purple')
        ax.plot(occlusion['c']['x'],occlusion['c']['y'],occlusion['c']['d'],'o', color = 'xkcd:purple')
    for colorNumber,(meshIndex,mesh) in enumerate(image.corners.items()):
        #print(mesh)
        ax.plot_wireframe(mesh['x'],mesh['y'],mesh['d'],color = colors[colorNumber])
    ax.invert_zaxis()
    ax.invert_xaxis()
    for params in image.v['linesParams']:
        rho = params[0][0]
        theta = params[0][1]
        ys = []
        if np.degrees(theta) == 0:
            print(True)
            for i, val in enumerate(image.v['thresh'].T[int(rho)]):
                if val > 0:
                    ys.append(i)
            print(ys)
            axes[0,0].plot([rho]*len(ys),ys,'o',color='k')
    for params in image.u['linesParams']:

        rho = params[0][0]
        theta = params[0][1]
        if np.degrees(theta) == 90:
            theta = np.radians(90)
            #print(rho)
            #print(image.u['thresh'][int(rho)])
            xs = []
            for i, val in enumerate(image.u['thresh'][int(rho)]):
                if val > 0:
                    xs.append(i)
            #xs = image.u['thresh'][int(rho)]
            #print(np.degrees(theta))
            x0 = rho*np.cos(theta)
            y0 = rho*np.sin(theta)
            x1 = int(x0 + 0*(-np.sin(theta)))
            x2 = int(x0 - 75*(-np.sin(theta)))
            y1 = int(y0 + 0*(np.cos(theta)))
            y2 = int(y0 - 75*(np.cos(theta)))
            axes[1,1].plot(xs,[rho]*len(xs),'o',color = 'k')
            #print(params[0][0])
            #print(np.degrees(params[0][1]))
            #x = params[0][0]*np.cos(params[0][1])
            #y = params[0][0]*np.sin(params[0][1])
            #axes[1,1].plot([x1,x2],[y1,y2])
            #print( x,y)'''
    #plt.show()


if __name__ == '__main__':
    test()
    '''fileName = sys.argv[1]
    arrays = np.load(fileName)[:,:,0:110]
    plt.imshow(arrays[0])
    plt.show()'''
