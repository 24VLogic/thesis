import matplotlib.pyplot as plt
import numpy as np
import time
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D
import sys

#distance amplitude x y z

def main():
    data = np.load(sys.argv[1])
    dist = data[0]
    amp = data[1]
    x = data[2]
    y = data[3]
    z = data[4]
    fig = plt.figure()
    axd = fig.add_subplot(311)
    axd.imshow(dist)
    axa = fig.add_subplot(312)
    axa.imshow(amp)
    axc = fig.add_subplot(313,projection='3d')
    axc.plot_surface(x,z,y,cmap='viridis')
    axc.invert_zaxis()
    plt.show()
if __name__ == '__main__':
    main()
