import numpy as np
import matplotlib.pyplot as plt
from skimage.morphology import skeletonize
import cv2
import sys

class plane():
    def __init__(self,lines):
        pass

def get_paramesh(xVector,yVector):
    uLine = yVector
    vLine = xVector
    xCoords = uLine.T[0]
    yCoords = uLine.T[1]
    for i, xVal in enumerate(vLine[1:]):
        xCoords= np.vstack((uLine.T[0]+vLine.T[0][i+1]-vLine.T[0][0],xCoords))
        yCoords= np.vstack((uLine.T[1]+vLine.T[1][i+1]-vLine.T[1][0],yCoords))
    yCoords = yCoords
    return({'x':xCoords,'y':yCoords})

def get_vDisp(image,bins = 30,thresh = (True,10)):
    scaledImage = np.around((np.copy(image)/image.max())*(bins-1)).astype('uint8')
    #print(scaledImage.max())
    #print(scaledImage.min())
    vDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            #print(pixel)
            hist[pixel] += 1
        #print(hist.size)
        #print(vDisparity.shape)
        vDisparity[i] = hist
    if thresh[0] == True:
        ret, vDisparity = cv2.threshold(vDisparity,thresh[1],1,cv2.THRESH_BINARY)
    return {'image':vDisparity,'scale':bins}

def get_uDisp(image, bins = 30,thresh = (True,15)):
    scaledImage = np.around((np.copy(image)/image.max())*(bins-1)).astype('uint8').T
    uDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            #print(pixel)
            hist[pixel] += 1
        #print(hist.size)
        #print(vDisparity.shape)
        uDisparity[i] = hist
    if thresh[0] == True:
        ret, uDisparity = cv2.threshold(uDisparity,thresh[1],1,cv2.THRESH_BINARY)
    return {'image':uDisparity.T,'scale':bins}
def get_vLines(vDispImage):
    lines = cv2.HoughLinesP(vDispImage.astype('uint8'),1,np.pi/180,20,minLineLength = 20,maxLineGap=1)
    lineDict = {}
    for i,line in enumerate(lines):
        for x1,y1,x2,y2 in line:
            dx = abs(x2-x1)
            dy = abs(y2-y1)
            if dy > dx:
                n = dy+1
            else:
                n = dx+1
            xy = np.array([np.linspace(y1,y2,num=n),np.linspace(x1,x2,num=n)]).T
        lineDict[i] = xy
    return(lineDict)
def get_uLines(uDispImage):
    lines = cv2.HoughLinesP(uDispImage.astype('uint8'),1,np.pi/180,20,minLineLength = 3,maxLineGap=1)
    lineDict = {}
    for i,line in enumerate(lines):
        for x1,y1,x2,y2 in line:
            dx = abs(x2-x1)
            dy = abs(y2-y1)
            if dy > dx:
                n = dy+1
            else:
                n = dx+1
            xy = np.array([np.linspace(y1,y2,num=n),np.linspace(x1,x2,num=n)]).T
        lineDict[i] = xy
    return(lineDict)
def get_lines(imageDict,axis = 'v'):
    image = imageDict['image']
    if axis == 'v':
        lines = cv2.HoughLinesP(image.astype('uint8'),1,np.pi/180,20,minLineLength = 20,maxLineGap=1)
    else:
        lines = cv2.HoughLinesP(image.astype('uint8'),1,np.pi/180,20,minLineLength = 3,maxLineGap=1)
    lineData = []
    for i,line in enumerate(lines):
        for x1,y1,x2,y2 in line:
            dx = abs(x2-x1)
            dy = abs(y2-y1)
            if dy > dx:
                n = dy+1
            else:
                n = dx+1
            xy = np.array([np.linspace(y1,y2,num=n),np.linspace(x1,x2,num=n)]).T
            for point in xy:
                if axis == 'v':
                    image[int(point[0]),int(point[1])] = point[1]
                if axis == 'u':
                    image[int(point[0]),int(point[1])] = point[0]
    return(lines)

def pair_lines(vLines,uLines):
    pairs = []
    for vLine in vLines:
        for uLine in uLines:


            if vLine[0][0] == uLine[0][1]:
                pairs.append([vLine[0],uLine[1]])
    return(pairs)

def match_lines(vLineDict,uLineDict):
    pairs = []
    for vLine in vLineDict:
        for uLine in uLineDict:
            #print(type(vLineDict[vLine]),type(uLineDict[uLine]))
            #print(vLineDict[vLine].shape,uLineDict[uLine].shape)
            if vLineDict[vLine][0][1] == uLineDict[uLine][0][0]:
                #pairs.append([vLineDict[vLine].T[0],uLineDict[uLine].T[1]])
                meshX,meshY = np.meshgrid(vLineDict[vLine].T[0],uLineDict[uLine].T[1])
                meshZ = np.meshgrid(vLineDict[vLine].T[1],uLineDict[uLine].T[0])[0]
                pairs.append([meshX,meshY,meshZ])
    return(pairs)
def mesh_lines(vLineDict,uLineDict):
    pairs = []
    for vLine in vLineDict:
        for uLine in uLineDict:
            if vLineDict[vLine][0][0] == uLineDict[uLine][0][1]:
                pairs.append([vLineDict[vLine].T[0],uLineDict[uLine].T[1]])
    return(pairs)
def show_line(linePtsPair):
    vLinePts = linePtsPair[0]
    uLinePts = linePtsPair[1]
    xs = np.linspace(vLinePts[1],vLinePts[3],num = 10)
    ys = np.linspace(uLinePts[0],uLinePts[2],num = 10)

    #print(np.meshgrid(xs,ys))
    #print(xs,ys)
    return(xs,ys)
def main():
    fileName = sys.argv[1]
    data = np.load(fileName)
    distanceImageFloat = data[4][40:,:111].astype('float')
    #img *= (img.shape[1]-1)/float(img.max())
    amplitudeImageFloat = data[1][40:,:111].astype('float')
    #img2 *= (img2.shape[1]-1)/float(img2.max())

    fig1 = plt.figure()
    distPlot = fig1.add_subplot(2,2,2)
    distPlot.axis('off')
    ampPlot = fig1.add_subplot(2,2,3)
    ampPlot.axis('off')
    vDispPlot = fig1.add_subplot(2,2,1)
    vDispPlot.axis('off')
    uDispPlot = fig1.add_subplot(2,2,4)
    uDispPlot.axis('off')

    vDispImageInt = get_vDisp(distanceImageFloat)['image']
    ret, vDispImageThresh = cv2.threshold(vDispImageInt,4,1,cv2.THRESH_BINARY)
    vDispImageThreshBlur = cv2.GaussianBlur(vDispImageThresh,(3,3),0)
    vDispImageSkel = skeletonize(vDispImageThresh)

    vDispFiltered = np.copy(vDispImageInt)
    #vDispFiltered = cv2.GaussianBlur(vDispImageInt,(3,3),0)
    #vDispFiltered = cv2.GaussianBlur(vDispImageInt,(3,3),0)
    ret, vDispFiltered = cv2.threshold(vDispFiltered,20,1,cv2.THRESH_TOZERO)
    #vDispFiltered = cv2.GaussianBlur(vDispImageInt,(3,3),0)
    #ret, vDispFiltered = cv2.threshold(vDispFiltered,10,1,cv2.THRESH_BINARY)
    uDispImageInt = get_uDisp(distanceImageFloat)['image']
    ret,
    fig2 = plt.figure()
    vdTest1 = fig2.add_subplot(2,2,1)
    vdTest1.axis('off')
    vdTest2 = fig2.add_subplot(2,2,2)
    vdTest2.axis('off')
    vdTest3 = fig2.add_subplot(2,2,3)
    vdTest3.axis('off')
    vdTest4 = fig2.add_subplot(2,2,4)
    vdTest4.axis('off')


    #print(distanceImageFloat.max().astype('uint16'))
    vdTest1.imshow(vDispImageInt)
    vdTest2.imshow(vDispImageSkel)
    vdTest3.imshow(vDispImageThresh)
    vdTest4.imshow(vDispImageThreshBlur)

    distPlot.imshow(distanceImageFloat)
    ampPlot.imshow(amplitudeImageFloat)
    vDispPlot.imshow(vDispFiltered)
    uDispPlot.imshow(uDispImageInt)
    plt.show()

def plot_compression():
    fileName = sys.argv[1]
    data = np.load(fileName)
    distanceImageFloat = data[4][40:,:111].astype('float')
    amplitudeImageFloat = data[1][40:,:111].astype('float')
    binCompressionFig = plt.figure(facecolor = 'xkcd:grey')
    bcax1 = binCompressionFig.add_subplot(111)
    bcax1.imshow(get_vDisp(distanceImageFloat ,bins = int(distanceImageFloat.max()))['image'],cmap = 'jet')
    bcax1.set_xlim(0,250)
    bcax1.title.set_text('V-Disparity Map\n' + str(distanceImageFloat.max().astype('uint16')) + ' bins')
    bcax1.set_aspect('auto')
    plt.savefig('vDispMaxBins',facecolor = "xkcd:cream")
    bcax1.clear()
    compImage = get_vDisp(distanceImageFloat ,bins = int(distanceImageFloat.max()/10))['image']
    ret, compImage = cv2.threshold(compImage,20,1,cv2.THRESH_TOZERO)
    bcax1.imshow(get_vDisp(distanceImageFloat ,bins = int(distanceImageFloat.max()/10))['image'],cmap = 'jet')
    bcax1.title.set_text('V-Disparity Map\n' + str(int(distanceImageFloat.max().astype('uint16')/10)) + ' bins')
    bcax1.set_aspect('auto')
    plt.savefig('vDisp10Bins',facecolor = "xkcd:cream")
    bcax1.clear()

    bcax1.imshow(get_vDisp(distanceImageFloat ,bins = 30)['image'],cmap = 'jet')
    bcax1.set_aspect('auto')
    bcax1.title.set_text('V-Disparity Map\n' + str(30) + ' bins')
    plt.savefig('vDisp30Bins',facecolor = "xkcd:cream")
    compImage = get_vDisp(distanceImageFloat ,bins = 30)['image']
    ret, compImage = cv2.threshold(compImage,20,1,cv2.THRESH_TOZERO)
    plt.show()

def plot_compression_ld():
    bcldFig = plt.figure()
    bcld1Ax = bcldFig.add_subplot(131)
    bcld1Ax.title.set_text("V Disparity")
    bcld2Ax = bcldFig.add_subplot(132)
    bcld2Ax.title.set_text("Thresholding")
    bcld3Ax = bcldFig.add_subplot(133)
    bcld3Ax.title.set_text("Line Detection")
    compImage = get_vDisp(distanceImageFloat ,bins = 30)['image']
    bcld1Ax.imshow(compImage,cmap = 'jet')
    ret, compImage = cv2.threshold(compImage,20,1,cv2.THRESH_BINARY)
    bcld2Ax.imshow(compImage,'jet')

    lines = cv2.HoughLinesP(compImage.astype('uint8'),1,np.pi/180,20,minLineLength = 20,maxLineGap=1)
    lineImage = np.zeros(compImage.shape)
    for i,line in enumerate(lines):
        for x1,y1,x2,y2 in line:
            dx = abs(x2-x1)
            dy = abs(y2-y1)
            if dy > dx:
                n = dy+1
            else:
                n = dx+1
            xy = np.array([np.linspace(y1,y2,num=n),np.linspace(x1,x2,num=n)]).T
            for point in xy:
                 lineImage[int(point[0]),int(point[1])] = 100+(i+1)*30

    bcld3Ax.imshow(lineImage,'jet')
    plt.savefig('vDispLineDetection')
    plt.show()

def uv_compression():
    fig = plt.figure()
    vPlot = fig.add_subplot(221)
    uPlot = fig.add_subplot(224)

    uDisp = get_uDisp(distanceImageFloat)['image']
    vDisp = get_vDisp(distanceImageFloat)['image']

    vPlot.imshow(vDisp)
    uPlot.imshow(uDisp)
    plt.show()

def uv_compression_ld():
    fig = plt.figure(figsize = (4,4))
    vPlot = fig.add_subplot(221,aspect = distanceImageFloat.shape[0]/30)
    uPlot = fig.add_subplot(224,aspect = distanceImageFloat.shape[1]/30)
    distancePlot = fig.add_subplot(222, aspect = 'auto')
    distancePlot.imshow(distanceImageFloat)
    linesPlot = fig.add_subplot(223,aspect = distanceImageFloat.shape[0]/distanceImageFloat.shape[1])
    linesPlot.set_xlim(0,distanceImageFloat.shape[1])
    linesPlot.set_ylim(distanceImageFloat.shape[0],0)
    uDisp = get_uDisp(distanceImageFloat)
    vDisp = get_vDisp(distanceImageFloat)
    vLines =get_lines(vDisp)
    uLines = get_lines(uDisp,axis = 'u')
    pairs = pair_lines(vLines,uLines)
    '''for pair in pairs:
            xs,ys = show_line(pairs[1])
            meshX, meshY = np.meshgrid(xs,ys)
            linesPlot.plot(meshY.flatten(),meshX.flatten(),'o')
    vPlot.imshow(vDisp['image'])'''
    for pair in pairs:
        xs,ys = show_line(pair)
        meshX, meshY = np.meshgrid(xs,ys)
        linesPlot.plot(meshY.flatten(),meshX.flatten(),'o')
        #print(pair)
    '''xs,ys = show_line(pairs[1])
    meshX, meshY = np.meshgrid(xs,ys)
    linesPlot.plot(meshY.flatten(),meshX.flatten(),'o')'''
    uPlot.imshow(uDisp['image'])
    vPlot.imshow(vDisp['image'])
    plt.show()
def surf_test():
    fig = plt.figure(figsize = (4,4))
    vPlot = fig.add_subplot(221,aspect = distanceImageFloat.shape[0]/30)
    vPlot.title.set_text('V Disparity Map')
    uPlot = fig.add_subplot(224,aspect = distanceImageFloat.shape[1]/30)
    uPlot.title.set_text('U Disparity Map')
    distancePlot = fig.add_subplot(222, aspect = 'auto')
    distancePlot.imshow(distanceImageFloat)
    distancePlot.title.set_text('Distance Image')
    linesPlot = fig.add_subplot(223,aspect = distanceImageFloat.shape[0]/distanceImageFloat.shape[1])
    linesPlot.title.set_text('Amplitude Image')
    linesPlot.set_xlim(0,distanceImageFloat.shape[1])
    linesPlot.set_ylim(distanceImageFloat.shape[0],0)
    linesPlot.imshow(amplitudeImageFloat,cmap = 'copper')
    uDisp = get_uDisp(distanceImageFloat, thresh = (False,))
    vDisp = get_vDisp(distanceImageFloat,thresh = (False,))
    uLines = get_uLines(uDisp['image'])
    vLines = get_vLines(vDisp['image'])
    uPlot.imshow(uDisp['image'])
    vPlot.imshow(vDisp['image'])
    plt.tight_layout()
    plt.savefig("uvdisp_preprocessing",facecolor = "xkcd:gray")
    uDisp = get_uDisp(distanceImageFloat)
    vDisp = get_vDisp(distanceImageFloat)
    uLines = get_uLines(uDisp['image'])
    vLines = get_vLines(vDisp['image'])
    uPlot.imshow(uDisp['image'])
    vPlot.imshow(vDisp['image'])
    plt.savefig("uvdisp_threshed",facecolor = "xkcd:gray")
    '''for line in uLines:
        uPlot.plot(uLines[line][:,1],uLines[line][:,0],linewidth = 2)
    for line in vLines:
        vPlot.plot(vLines[line][:,1],vLines[line][:,0],linewidth = 2)'''
    pairs = match_lines(vLines,uLines)
    from mpl_toolkits.mplot3d import Axes3D
    linesPlot.title.set_text('Identified Planes')
    fig2 = plt.figure()
    surfPlot = fig2.add_subplot(111,projection = '3d',facecolor = "xkcd:grey",aspect = 'auto')
    surfPlot.set_zlim(distanceImageFloat.shape[0],0)
    surfPlot.title.set_text('Identified Planes')
    #surfPlot.set_xlim(0,distanceImageFloat.shape[0])
    color=iter(plt.cm.cool(np.linspace(0,1,len(pairs))))
    for pair in pairs:
            xMesh = pair[0]
            yMesh = pair[1]
            zMesh = pair[2]
            c = next(color)
            uPlot.plot(yMesh.T[0],zMesh.T[0], color = c,linewidth = 2)
            vPlot.plot(zMesh[0],xMesh[0],color = c,linewidth = 2)
            linesPlot.plot(yMesh.flatten(),xMesh.flatten(),'o',color = c,markersize = 0.75)
            surfPlot.plot_wireframe(yMesh,zMesh,xMesh,color = c)
    #surfPlot.plot_wireframe(xImage,zImage,yImage)
    #fig.savefig("uvdisp_postprocessing",facecolor = "xkcd:gray")
    #fig2.savefig("identified_planes",facecolor = "xkcd:gray")
    plt.show()

def identify_blindspot():
    fig = plt.figure(figsize = (4,4))
    vPlot = fig.add_subplot(221,aspect = distanceImageFloat.shape[0]/30)
    vPlot.title.set_text('V Disparity Map')
    uPlot = fig.add_subplot(224,aspect = distanceImageFloat.shape[1]/30)
    uPlot.title.set_text('U Disparity Map')
    distancePlot = fig.add_subplot(222, aspect = 'auto')
    distancePlot.imshow(distanceImageFloat)
    distancePlot.title.set_text('Distance Image')
    linesPlot = fig.add_subplot(223,aspect = distanceImageFloat.shape[0]/distanceImageFloat.shape[1])
    linesPlot.title.set_text('Amplitude Image')
    linesPlot.set_xlim(0,distanceImageFloat.shape[1])
    linesPlot.set_ylim(distanceImageFloat.shape[0],0)
    linesPlot.imshow(amplitudeImageFloat,cmap = 'copper')
    uDisp = get_uDisp(distanceImageFloat, thresh = (False,))
    vDisp = get_vDisp(distanceImageFloat,thresh = (False,))
    uLines = get_uLines(uDisp['image'])
    vLines = get_vLines(vDisp['image'])
    uPlot.imshow(uDisp['image'])
    vPlot.imshow(vDisp['image'])
    plt.tight_layout()

    uDisp = get_uDisp(distanceImageFloat)
    vDisp = get_vDisp(distanceImageFloat)
    uLines = get_uLines(uDisp['image'])
    vLines = get_vLines(vDisp['image'])
    uPlot.imshow(uDisp['image'])
    vPlot.imshow(vDisp['image'])
    plt.savefig("uvdisp_threshed",facecolor = "xkcd:gray")
    '''for line in uLines:
        uPlot.plot(uLines[line][:,1],uLines[line][:,0],linewidth = 2)
    for line in vLines:
        vPlot.plot(vLines[line][:,1],vLines[line][:,0],linewidth = 2)'''
    pairs = match_lines(vLines,uLines)
    from mpl_toolkits.mplot3d import Axes3D
    linesPlot.title.set_text('Identified Planes')
    fig2 = plt.figure()
    surfPlot = fig2.add_subplot(111,projection = '3d',facecolor = "xkcd:grey",aspect = 'auto')
    surfPlot.set_zlim(distanceImageFloat.shape[0],0)
    surfPlot.title.set_text('Identified Planes')
    #surfPlot.set_xlim(0,distanceImageFloat.shape[0])
    color=iter(plt.cm.cool(np.linspace(0,1,len(pairs))))

    total_occlusions = np.zeros((distanceImageFloat.shape[0],distanceImageFloat.shape[1]+1))

    totalX,totalY = np.meshgrid(np.arange(distanceImageFloat.shape[0]),np.arange(distanceImageFloat.shape[1]+1))
    totalZ = np.copy(total_occlusions)

    blank_occlusions = np.copy(total_occlusions)
    figOcc = plt.figure()
    occPlot = figOcc.add_subplot(111)
    occPlot.title.set_text('XY-Plane Surface Overlap')
    for pair in pairs:
            xMesh = pair[0].astype('uint16')
            yMesh = pair[1].astype('uint16')
            zMesh = 30 - pair[2].astype('uint16')
            c = next(color)
            uPlot.plot(yMesh.T[0],zMesh.T[0], color = c,linewidth = 2)
            vPlot.plot(zMesh[0],xMesh[0],color = c,linewidth = 2)
            linesPlot.plot(yMesh.flatten(),xMesh.flatten(),'o',color = c,markersize = 0.75)
            surfPlot.plot_wireframe(yMesh,zMesh,xMesh,color = c)
            absolute_mesh = np.copy(blank_occlusions)
            #print(absolute_mesh.shape)
            absolute_mesh[xMesh[0][-1]:xMesh[0][0]+1,yMesh.T[0][0]:yMesh.T[0][-1]+1]+=1
            if zMesh.max() > totalZ[xMesh[0][-1]:xMesh[0][0]+1,yMesh.T[0][0]:yMesh.T[0][-1]+1].max():
                totalZ[xMesh[0][-1]:xMesh[0][0]+1,yMesh.T[0][0]:yMesh.T[0][-1]+1] = 30-zMesh.T
                print('catch')
            #print(absolute_mesh[yMesh.T[0][0]:yMesh.T[0][-1]])
            total_occlusions += absolute_mesh
    ret, filteredOcclusions = cv2.threshold(total_occlusions,1,1,cv2.THRESH_BINARY)
    oyMesh,oxMesh = np.meshgrid(np.nonzero(filteredOcclusions)[0],np.nonzero(filteredOcclusions)[1])

    print(oyMesh)
    oyCorners = np.array([[oyMesh[0][0],oyMesh[0][-1]],[oyMesh[-1][0],oyMesh[-1][-1]]])
    oxCorners = np.array([[oxMesh[0][0],oxMesh[0][-1]],[oxMesh[-1][0],oxMesh[-1][-1]]])
    print(oxCorners)
    occPlot.imshow(total_occlusions)
    plt.savefig('filtered_occlusions',facecolor="xkcd:grey")
    plt.show()
if __name__ == "__main__":
    fileName = sys.argv[1]
    data = np.load(fileName)
    distanceImageFloat = data[0][40:,:111].astype('float')
    #distanceImageFloat = cv2.GaussianBlur(distanceImageFloat,(5,5),0)
    #distanceImageFloat = cv2.GaussianBlur(distanceImageFloat,(5,5),0)
    #distanceImageFloat = cv2.bilateralFilter(distanceImageFloat.astype('float32'),9,75,75)
    amplitudeImageFloat = data[1][40:,:111].astype('float')
    xImage =  data[2].astype('int16')[40:,:111]
    yImage =  data[3].astype('int16')[40:,:111]
    zImage =  data[4].astype('int16')[40:,:111]
    #main()
    #plot_compression()
    #plot_compression_ld()
    #uv_compression()
    #uv_compression_ld()
    surf_test()
    #identify_blindspot()
