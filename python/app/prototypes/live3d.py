import sys
import o3d3xx
import matplotlib.pyplot as plt
import numpy as np
import time
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D

imageWidth = 176
imageHeight = 132

class GrabO3D300():
    def __init__(self,data):
        self.data = data
        self.Amplitude = np.zeros((imageHeight,imageWidth))
        self.Distance = np.zeros((imageHeight,imageWidth))

    def readNextFrame(self):
        result = self.data.readNextFrame()
        self.Amplitude = np.frombuffer(result['amplitude'],dtype='uint16')
        self.Amplitude = self.Amplitude.reshape(imageHeight,imageWidth)
        self.Distance = np.frombuffer(result['distance'],dtype='uint16')
        self.Distance = self.Distance.reshape(imageHeight,imageWidth)
        self.x = np.frombuffer(result['x'],dtype='int16')
        self.x =self.x.reshape(imageHeight,imageWidth)
        self.y = np.frombuffer(result['y'],dtype='int16')
        self.y =self.y.reshape(imageHeight,imageWidth)
        self.z = np.frombuffer(result['z'],dtype='int16')
        self.z =self.z.reshape(imageHeight,imageWidth)
        self.illuTemp = 20.0

def updatefig(*args):
    g = args[1]
    g.readNextFrame();

    points3d = args[2]

    points3d._offsets3d = (g.x.flatten(),g.y.flatten(),g.z.flatten())
    print(points3d._offsets3d)
    return(points3d)
def update_wf(*args):
    g = args[1]
    g.readNextFrame();
    x = g.x
    y = g.y
    z = g.z
    ax1 = args[2]
    ax1.clear()
    ax1.invert_zaxis()
    wf = ax1.plot_surface(x,z,y)

def main():
    address = sys.argv[1]
    camData = o3d3xx.ImageClient(address, 50010)

    fig = plt.figure()
    grabber = GrabO3D300(camData)
    grabber.readNextFrame()
    x = grabber.x
    y = grabber.y
    z = grabber.z
    ax1 = fig.add_subplot(1, 1, 1, projection = '3d')
    wf = ax1.plot_surface(x,z,y)
    ani = animation.FuncAnimation(fig, update_wf, interval=50, blit=False, fargs = [grabber,ax1])
    plt.show()

if __name__ == '__main__':
    main()
