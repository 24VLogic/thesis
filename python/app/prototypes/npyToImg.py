import numpy as np
import matplotlib.pyplot as plt
import cv2

# load image data
img = np.load('boxdist.npy')
img = img[:,30:140].astype('float')
img *= (img.shape[1]-1)/float(img.max())

img2 = np.load('boxamp.npy')
img2 = img2[:,30:140].astype('float')
img2 *= (img2.shape[1]-1)/float(img2.max())

# compute v disparity
vDisparity = np.zeros((img.shape[0],img.shape[1]))

for i,row in enumerate(img):
    hist = np.zeros(img.shape[1])
    #print hist.shape
    for pixel in row:
        #print int(pixel)
        hist[int(pixel)] += 1
    vDisparity[i] = hist
#print vDisparity.shape
#print img.shape

# compute u disparity
uDisparity = np.zeros((img.T.shape[0],img.T.shape[1]))

for i,row in enumerate(img.T):
    hist = np.zeros(img.T.shape[1])
    #print hist.shape
    for pixel in row:
        #print int(pixel)
        hist[int(pixel)] += 1
    uDisparity[i] = hist
uDisparity = uDisparity.T
print uDisparity.shape
print vDisparity.shape
print img.shape
fig = plt.figure()
plt.suptitle("U-V Disparity Map of O3D Image")
ax1 = fig.add_subplot(2, 2, 1, aspect = 'equal')
ax1.imshow(vDisparity, cmap = 'plasma')
plt.title('V-Disparity Map')
plt.axis('off')
ax2 = fig.add_subplot(2,2,2, aspect = 'equal')
ax2.imshow(img,cmap = 'plasma_r')
plt.title('O3D Depth Image')
plt.axis('off')
ax4 = fig.add_subplot(2,2,4,aspect = 'equal')
ax4.imshow(uDisparity,cmap = 'plasma')
plt.title('U-Disparity Map')
plt.axis('off')
ax3 = fig.add_subplot(2,2,3)
ax3.imshow(img2,cmap = 'gray')
plt.title('O3D Amplitude Image')
plt.axis('off')

fig.savefig('uvDisparityO3D.png')
