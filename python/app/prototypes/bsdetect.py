import itertools
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy.ndimage.filters import gaussian_filter as gf

from mpl_toolkits.mplot3d import Axes3D
from skimage.morphology import skeletonize
import cv2
import sys

#Filters

def gaussian_filter(inImage, sigma = 1, order = [0,0]):
    imageOut = {}
    imageOut['image'] = 1-gf(inImage,sigma = sigma,order = order)
    imageOut['sigma'] = sigma
    imageOut['order'] = order
    return(imageOut)
    pass
def vertical_average_filter(inImage, size = 3):
    kernel = np.zeros((size, size))
    kernel[int((size-1)/2), :] = np.ones(size)
    kernel[0,1] = 0
    kernel = kernel.T/size
    imageOut = cv2.filter2D(inImage, -1, kernel)
    return(imageOut)
def average_filter(inImage, kSize = 3):
    kernel = np.ones((kSize,kSize))/kSize
    imageOut = cv2.filter2D(inImage, -1, kernel)
    return(imageOut)
def bilateral_filter(inImage,a1,a2,a3):
    imageOut = cv2.bilateralFilter(inImage.astype('float32'),a1,a2,a3)
    return(imageOut)
def thresh(inImage,threshold,kw = cv2.THRESH_BINARY):
    ret,imageOut = cv2.threshold(inImage,threshold,1,kw)
    return(imageOut)

#Line Detection

def get_vertical_lines(inImage, votes = 10, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    vLines = []
    if len(lines>0):
        for line in lines:
            ys = []

            r, theta = line[0]
            if theta == 0:
                #print(inImage.T[int(r)])
                ys = [i for i, p in enumerate(inImage.T[int(r)]) if p >0]
            rs = [r]*len(ys)
            if ys:
                vLines.append({'d':rs,'y':ys})
    return(vLines)

def get_horizontal_lines(inImage, votes = 10, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    hLines = []
    if len(lines)>0:
        for line in lines:
            xs = []
            r, theta = line[0]
            #print(r,int(np.degrees(theta)))
            if int(np.degrees(theta)) == 90:
                #print('catch')
                xs = [i for i, p in enumerate(inImage[int(r)]) if p >0]
            rs = [r]*len(xs)
            if xs:
                hLines.append({'d':rs,'x':xs})
        #print(hLines)
    return(hLines)

#Disparity Maps
def gen_v_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8')
    vDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] += 1
        vDisparity[i] = hist
    return({'image':vDisparity,'bins':bins})

def gen_sparse_v_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8')
    vDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] = 1
        vDisparity[i] = hist
    return({'image':vDisparity,'bins':bins})

def gen_u_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8').T
    uDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] += 1
        uDisparity[i] = hist
    return({'image':uDisparity.T,'bins':bins})
def filter_response(image,sigma = 0.75):
    imxx = np.copy(image)
    imxy = np.copy(image)
    imyy = np.copy(image)
    for i in range(3):
        imxx = gaussian_filter(imxx,sigma,[0,2])['image']
        imxy = gaussian_filter(imxy,sigma,[1,1])['image']
        imyy = gaussian_filter(imyy,sigma,[2,0])['image']
    imDiff = np.zeros(image.shape)
    for y, row in enumerate(image):
        for x, value in enumerate(row):
            inspect = [imxx[y,x],imxy[y,x],imyy[y,x]]
            highestFilterResponse = max(inspect)
            lowestFilterResponse = min(inspect)
            imDiff[y,x] = highestFilterResponse - lowestFilterResponse
    return(imDiff)
def check_lines(uLines,vLines):
    pairIndex = 0
    matchedLines = {}
    for uLine in uLines:
        #print('check')
        #pass
        for vLine in vLines:
            #print(uLine['d'][0],vLine['d'][0])
            if uLine['d'][0] == vLine['d'][0]:
                #print('catch')
                matchedLines[pairIndex] = {'uLine' : uLine,'vLine' : vLine}
                pairIndex += 1
    return(matchedLines)
class imageArrays():
    def __init__(self, arr):

        self.d = arr[0].astype('uint16')
        #self.d_safe = arr[0].astype('uint16')
        #self.d_max = self.d_safe.max()
        self.a = arr[1].astype('uint16')
        self.x = arr[2].astype('uint16')
        self.y = arr[3].astype('uint16')
        self.z = arr[4].astype('uint16')
        self.v = None
        self.u = None
        self.image = {}
        self.image['normalized'] = np.copy(self.d)/self.d.max()
        self.image['original'] = np.copy(self.d)
        self.image['255'] = np.copy(self.image['normalized'])*255
        self.image['255'] = self.image['255'].astype('uint8')

def sparseTest():
    im = image.image['255']
    vBins = 30
    imV = gen_v_disparity(im,vBins)['image']
    imV = thresh(imV,15)
    imVxx = np.copy(imV)
    imVxy = np.copy(imV)
    imVyy = np.copy(imV)
    sigma = 0.75
    for i in range(3):
        imVxx = gaussian_filter(imVxx,sigma,[0,2])['image']
        imVxy = gaussian_filter(imVxy,sigma,[1,1])['image']
        imVyy = gaussian_filter(imVyy,sigma,[2,0])['image']
    imDiff = np.zeros(imV.shape)
    for y, row in enumerate(imV):
        for x, value in enumerate(row):
            inspect = [imVxx[y,x],imVxy[y,x],imVyy[y,x]]
            highestFilterResponse = max(inspect)
            lowestFilterResponse = min(inspect)
            imDiff[y,x] = highestFilterResponse - lowestFilterResponse
    plt.imshow(imDiff)
    plt.show()
def gaussVdisp():
    im = image.image['255']
    vBins = 30
    imV = gen_v_disparity(im,bins =vBins)['image']
    s = [0.5,1,2]
    imVgf1 = gaussian_filter(imV,s[0],order = [0,2])['image']
    imVgf2 = gaussian_filter(imV,s[1],order = [0,2])['image']
    imVgf3 = gaussian_filter(imV,s[2],order=[0,2])['image']
    imVgfs = [imVgf1,imVgf2,imVgf3]
    fig, axes = plt.subplots(1,4,sharey=True)
    #fig.suptitle('Gaussian Filter of V Disparity\n\n')
    axes[0].imshow(imV)
    axes[0].title.set_text('Original')
    for i in range(3):
        axes[i+1].imshow(imVgfs[i])
        axes[i+1].title.set_text(r'$\sigma$'+' = ' + str(s[i]))
    plt.tight_layout()
    #plt.show()
    plt.savefig('gaussVdisp',facecolor = 'xkcd:grey')

def gaussUdispThresh():
    im = image.image['255']
    vBins = 30
    threshold = 6
    imU = gen_u_disparity(im,bins =vBins)['image']
    s = [0.5,1,2]
    imUgf1 = thresh(gaussian_filter(imU,s[0],order = [2,0])['image'],threshold)
    imUgf2 = thresh(gaussian_filter(imU,s[1],order = [2,0])['image'],threshold)
    imUgf3 = thresh(gaussian_filter(imU,s[2],order=[2,0])['image'],threshold)
    imUgfs = [imUgf1,imUgf2,imUgf3]
    fig, axes = plt.subplots(4,1)
    #fig.suptitle('Gaussian Filter of V Disparity\n\n')
    axes[0].imshow(thresh(imU,threshold))
    axes[0].title.set_text('Original')
    for ax in axes:
        ax.set_ylim(vBins,0)
    for i in range(3):
        axes[i+1].imshow(imUgfs[i])
        axes[i+1].title.set_text(r'$\sigma$'+' = ' + str(s[i]))
    plt.tight_layout()
    #plt.show()
    plt.savefig('gaussUdispThresh',facecolor = "xkcd:grey")
def gaussUdisp():
    im = image.image['255']
    vBins = 30
    imU = gen_u_disparity(im,bins =vBins)['image']
    s = [0.5,1,2]
    imUgf1 = gaussian_filter(imU,s[0],order = [2,0])['image']
    imUgf2 = gaussian_filter(imU,s[1],order = [2,0])['image']
    imUgf3 = gaussian_filter(imU,s[2],order=[2,0])['image']
    imUgfs = [imUgf1,imUgf2,imUgf3]
    fig, axes = plt.subplots(4,1)
    #fig.suptitle('Gaussian Filter of V Disparity\n\n')
    axes[0].imshow(imU)
    axes[0].title.set_text('Original')
    for ax in axes:
        ax.set_ylim(vBins,0)
    for i in range(3):
        axes[i+1].imshow(imUgfs[i])
        axes[i+1].title.set_text(r'$\sigma$'+' = ' + str(s[i]))
    plt.tight_layout()
    plt.show()
    #plt.savefig('gaussUdisp',facecolor = 'xkcd:grey')
def func1():
    #mpl.rcParams['image.cmap'] = 'tab20'
    im = image.image['normalized']
    fig, axes = plt.subplots(4,2)
    fig.suptitle("Comparison of Smoothing Filters")
    imV = gen_v_disparity(im,bins=30)['image']
    imgf = gaussian_filter(im,2)['image']
    imgfV = gen_v_disparity(imgf,30)['image']
    axes[0,1].imshow(im)
    axes[0,0].imshow(imV)
    axes[0,1].title.set_text("Normalized Distance Image")
    axes[1,1].imshow(imgf)
    axes[1,0].imshow(imgfV)
    axes[2,1].imshow(bilateral_filter(im,9,150,150))
    #axes[2].imshow(gaussian_filter(im,sigma = 1)['image'],cmap='tab20')
    axes[3,1].imshow(average_filter(im,kSize = 11))
    plt.tight_layout()
    #lt.show()
def gaussVdispThresh():
    im = image.image['255']
    vBins = 30
    threshold = 6
    imV = gen_v_disparity(im,bins =vBins)['image']
    s = [0.5,1,2]
    imVgf1 = thresh(gaussian_filter(imV,s[0],order = [0,2])['image'],threshold)
    imVgf2 = thresh(gaussian_filter(imV,s[1],order = [0,2])['image'],threshold)
    imVgf3 = thresh(gaussian_filter(imV,s[2],order=[0,2])['image'],2)
    imVgfs = [imVgf1,imVgf2,imVgf3]
    fig, axes = plt.subplots(1,4,sharey=True)
    #fig.suptitle('Gaussian Filter of V Disparity\n\n')
    axes[0].imshow(thresh(imV,threshold))
    axes[0].title.set_text('Original')
    for i in range(3):
        axes[i+1].imshow(imVgfs[i])
        axes[i+1].title.set_text(r'$\sigma$'+' = ' + str(s[i]))
    plt.tight_layout()
    #plt.show()
    #plt.savefig('gaussVdispThresh',facecolor = 'xkcd:grey')

def vdisp_line_detect():
    im = image.image['255']
    vBins = 30
    threshold = 6
    imV = gen_v_disparity(im,bins =vBins)['image']
    s = [0.5,1,2]
    imVgf = thresh(gaussian_filter(imV,1,order = [0,2])['image'],threshold).astype('uint8')
    plt.imshow(imVgf)
    lines = get_vertical_lines(imVgf,votes = 40)
    if len(lines)>0:
        for line in lines:
            plt.plot(line['d'],line['y'],linewidth = 2)
    #plt.tight_layout()
    plt.show()
    #plt.savefig('vdsip_ld',facecolor = 'xkcd:grey')

def udisp_line_detect():
    im = image.image['255']
    bins = 30
    threshold = 6
    imU = gen_u_disparity(im,bins =bins)['image']
    s = [0.5,1,2]
    imUgf = thresh(gaussian_filter(imU,1.25,order = [2,0])['image'],threshold).astype('uint8')
    plt.imshow(imUgf)
    lines = get_horizontal_lines(imUgf,votes = 20)
    if len(lines)>0:
        for line in lines:
            plt.plot(line['x'],line['d'],linewidth = 2)
    #plt.tight_layout()
    plt.show()
    #plt.savefig('vdsip_ld',facecolor = 'xkcd:grey')
def window_segmentation():
    fig = plt.figure()
    axes = fig.subplots(2,2).flatten()
    im = image.image['255']
    axes[1].imshow(im)
    bins = 29
    threshold = 6
    imU = gen_u_disparity(im,bins =bins)['image']
    s = [0.5,1,2]
    imUgf = thresh(filter_response(imU),50).astype('uint8')
    axes[3].imshow(imUgf)
    imV = gen_v_disparity(im,bins =bins)['image']
    imVgf = thresh(filter_response(imV),20).astype('uint8')
    axes[0].imshow(imVgf)
    uLines = get_horizontal_lines(imUgf,votes = 10)
    vLines = get_vertical_lines(imVgf,votes = 10)

    for uLine in uLines:
        #print('check')

        for vLine in vLines:
            #print(uLine['d'][0],vLine['d'][0])
            if uLine['d'][0] == vLine['d'][0]:
                print('catch')
                axes[0].plot(vLine['d'],vLine['y'])
                axes[3].plot(uLine['x'],uLine['d'])
                xx,yy = np.meshgrid(uLine['x'],vLine['y'])
                axes[2].plot(xx.flatten(),yy.flatten(),'o')
    axes[2].imshow(im)
    #plt.imshow(im)
    plt.show()
def test():
    im = image.image['255']
    vBins = 30
    threshold = 6
    imV = gen_v_disparity(im,bins =vBins)['image']
    #response = 1- gaussian_filter(imV,1,order = [0,2])['image']
    va = vertical_average_filter(imV,3)
    for i in range(2):
        va = vertical_average_filter(va,3)
    for i in range(3):
        pass
        #imV = gaussian_filter(imV,1, order=[0,2])['image']
        #imV = gaussian_filter(imV,5, order=[1,1])['image']
        #imV = gaussian_filter(imV,0.25, order=[2,0])['image']
    for i in range(3):
        pass
        #imV = gaussian_filter(imV,1, order=[0,2])['image']
        #imV = gaussian_filter(imV,2, order=[1,1])['image']
        #imV = gaussian_filter(imV,0.25, order=[2,0])['image']
    #imV = thresh(imV,3)
    print(va)
    plt.imshow(va)
    plt.show()
def tri_filter_diff_result():
    im = image.image['255']
    bins = 30
    fig, axes = plt.subplots(nrows=1,ncols=3,sharey=True,)
    for ax in axes.flatten():
        ax.set_xlim(0,bins)
        ax.set_aspect('equal')
        #ax.set_ylim(im.shape[0]-1,0)
        #ax.axis('off')
    axes[0].title.set_text('V Disparity Map')
    #axes[1].title.set_text(r'$G_{xx}$ Response')
    #axes[2].title.set_text(r'$G_{xy}$ Response')
    #axes[3].title.set_text(r'$G_{yy}$ Response')
    #axes[4].title.set_text('Response Difference')
    imV = gen_v_disparity(im, bins)['image']
    #imV = vertical_average_filter(imV)
    imVxx = np.copy(imV)
    imVxy = np.copy(imV)
    imVyy = np.copy(imV)
    axIndex = 0
    sigma = 0.5
    for i in range(3):
        imVxx = gaussian_filter(imVxx,sigma,[0,2])['image']
        imVxy = gaussian_filter(imVxy,sigma,[1,1])['image']
        imVyy = gaussian_filter(imVyy,sigma,[2,0])['image']
    axes[0].imshow(imV)
    #axes[1].imshow(imVxx)
    #axes[2].imshow(imVxy)
    #axes[3].imshow(imVyy)
    imDiff = np.zeros(imV.shape)
    for y, row in enumerate(imV):
        for x, value in enumerate(row):
            inspect = [imVxx[y,x],imVxy[y,x],imVyy[y,x]]
            highestFilterResponse = max(inspect)
            lowestFilterResponse = min(inspect)
            imDiff[y,x] = highestFilterResponse - lowestFilterResponse
    axes[1].imshow(imDiff)
    axes[1].title.set_text('Response Difference')
    imThresh = thresh(imDiff,threshold = 1500,kw=cv2.THRESH_BINARY)
    imThresh = skeletonize(imThresh)
    axes[2].imshow(imThresh)
    axes[2].title.set_text('Threshold')
    #plt.suptitle(r'$\sigma$ = ' + str(sigma))
    plt.tight_layout()
    plt.savefig('tri_filter_diff_result',facecolor = 'xkcd:grey')
    #plt.show()

def tri_filter_diff():
    im = image.image['255']
    bins = 30
    fig, axes = plt.subplots(nrows=1,ncols=4,sharey=True,)
    for ax in axes.flatten():
        ax.set_xlim(0,bins)
        ax.set_aspect('equal')
        #ax.set_ylim(im.shape[0]-1,0)
        #ax.axis('off')
    axes[0].title.set_text('V Disparity Map')
    axes[1].title.set_text(r'$G_{xx}$ Response')
    axes[2].title.set_text(r'$G_{xy}$ Response')
    axes[3].title.set_text(r'$G_{yy}$ Response')
    #axes[4].title.set_text('Response Difference')
    imV = gen_v_disparity(im, bins)['image']
    #imV = vertical_average_filter(imV)
    imVxx = np.copy(imV)
    imVxy = np.copy(imV)
    imVyy = np.copy(imV)
    axIndex = 0
    sigma = 0.5
    for i in range(3):
        imVxx = gaussian_filter(imVxx,sigma,[0,2])['image']
        imVxy = gaussian_filter(imVxy,sigma,[1,1])['image']
        imVyy = gaussian_filter(imVyy,sigma,[2,0])['image']
    axes[0].imshow(imV)
    axes[1].imshow(imVxx)
    axes[2].imshow(imVxy)
    axes[3].imshow(imVyy)
    imDiff = np.zeros(imV.shape)
    for y, row in enumerate(imV):
        for x, value in enumerate(row):
            inspect = [imVxx[y,x],imVxy[y,x],imVyy[y,x]]
            highestFilterResponse = max(inspect)
            lowestFilterResponse = min(inspect)
            imDiff[y,x] = highestFilterResponse - lowestFilterResponse
    #axes[4].imshow(imDiff)
    #imThresh = thresh(imDiff,threshold = 1500,kw=cv2.THRESH_BINARY)
    #axes[4].imshow(imThresh)
    #plt.suptitle(r'$\sigma$ = ' + str(sigma))
    plt.tight_layout()
    plt.savefig('tri_filter_diff',facecolor = 'xkcd:grey')
    plt.show()

if __name__ == '__main__':
    #colors = cm.jet_r(np.linspace(0,1,binSize))
    fileName = sys.argv[1]
    arrays = np.load(fileName)[:,40:,20:110]
    image = imageArrays(arrays)
    #gaussVdispThresh()
    #test()
    #gaussUdisp()
    #gaussUdispThresh()
    #vdisp_line_detect()
    #udisp_line_detect()
    #tri_filter_diff_result()
    #sparseTest()
    window_segmentation()
