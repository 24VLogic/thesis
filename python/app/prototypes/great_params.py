import itertools
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from skimage.morphology import skeletonize
import cv2
import sys

class imageArrays():
    def __init__(self, arr):
        self.d = arr[0].astype('uint16')
        self.d_safe = arr[0].astype('uint16')
        self.d_max = self.d_safe.max()
        self.a = arr[1].astype('uint16')
        self.x = arr[2].astype('uint16')
        self.y = arr[3].astype('uint16')
        self.z = arr[4].astype('uint16')
        self.v = None
        self.u = None

    def blur_d(self,kernelSize = 3):
        kernel = np.ones((kernelSize,kernelSize))/kernelSize
        self.d = cv2.filter2D(self.d, -1, kernel)

    def bf_d(self):
        self.d = cv2.bilateralFilter(distanceImageFloat.astype('float32'),9,75,75)

    def reset_d(self):
        self.d = np.copy(self.d_safe)

    def gen_v_disparity(self,bins=30):
        self.v = gen_v_disparity(self.d,bins)

    def gen_u_disparity(self,bins=30):
        self.u = gen_u_disparity(self.d,bins)

    def thresh_v(self, threshold,key = 'image'):
        if self.v:
            ret, thresh = cv2.threshold(self.v[key],threshold,1,cv2.THRESH_BINARY)
            self.v['thresh'] = thresh

    def thresh_u(self, threshold,key = 'image'):
        if self.u:
            ret, thresh = cv2.threshold(self.u[key],threshold,1,cv2.THRESH_BINARY)
            self.u['thresh'] = thresh

    def blur_v(self, size=3 ,n=1, key = 'image'):
        #kernel = np.ones((size,size))
        kernel = np.zeros((size, size))
        kernel[int((size-1)/2), :] = np.ones(size)
        kernel = kernel.T / size
        blur = np.copy(self.v[key])
        for i in range(n):
            blur = cv2.filter2D(blur, -1, kernel)
        self.v['blur'] = blur

    def blur_u(self, size=3 ,n=1, key = 'image'):
        kernel = np.zeros((size, size))
        kernel[int((size-1)/2), :] = np.ones(size)
        kernel = kernel / size
        blur = np.copy(self.u[key])
        for i in range(n):
            blur = cv2.filter2D(blur, -1, kernel)
        self.u['blur'] = blur

    def lines_v(self, rRes =1, thRes = np.pi/180,votes=10, minLineLength = 10, maxLineGap =1):
        if 'thresh' in self.v:
            self.v['plinesPoints'] = cv2.HoughLinesP(self.v['thresh'].astype('uint8'),rRes,thRes,votes,minLineLength,maxLineGap)
            self.v['linesParams'] = cv2.HoughLines(self.v['thresh'].astype('uint8'),rRes,thRes,votes)
    def lines_u(self, rRes =1, thRes = np.pi/180,votes=10, minLineLength = 10, maxLineGap =1):
        if 'thresh' in self.u:
            self.u['plinesPoints'] = cv2.HoughLinesP(self.u['thresh'].astype('uint8'),rRes,thRes,votes,minLineLength,maxLineGap)
            self.u['linesParams'] = cv2.HoughLines(self.u['thresh'].astype('uint8'),rRes,thRes,votes)

    def get_plines_v(self, n=10):
        self.v['plines'] = {}
        for i,line in enumerate(self.v['plinesPoints']):
            d1,y1,d2,y2 = line[0]
            self.v['plines'][i]={'y' : np.linspace(y1,y2,num = n).astype('uint16'), 'd' : np.linspace(d1,d2,num = n).astype('uint16')}

    def get_plines_u(self, n=10):
        self.u['plines'] = {}
        for i,line in enumerate(self.u['plinesPoints']):
            x1,d1,x2,d2 = line[0]
            self.u['plines'][i]={'x' : np.linspace(x1,x2,num = n).astype('uint16'), 'd' : np.linspace(d1,d2,num = n).astype('uint16')}

    def match_lines(self,delta = 1):
        self.match = {}
        for uline in self.u['plines'].items():
            #print(uline)
            ulineNumber, ulineData = uline
            averageDu = sum(ulineData['d'])/len(ulineData['d'])
            #print(averageDu)
            for vline in self.v['plines'].items():
                vlineNumber, vlineData = vline
                averageDv = sum(vlineData['d'])/len(vlineData['d'])
                if abs(averageDu - averageDv)<=delta:
                    self.match[(ulineNumber,vlineNumber)]={'uline':ulineData,'vline':vlineData}
                    #print(averageDu - averageDv)
    def print_foo(self):
        print('foo')
    def mesh_lines(self):
        self.surfs = {}
        self.corners = {}
        for pair in self.match.items():
            matchIndices, lineData = pair
            uline = lineData['uline']
            vline = lineData['vline']
            x,y = np.meshgrid(uline['x'],vline['y'])
            averageD =(sum(uline['d'])+sum(vline['d']))/(uline['d'].size+vline['d'].size)
            d = np.full(x.shape,int(averageD))
            cornerIndices = np.ix_((0,-1),(0,-1))
            self.surfs[matchIndices]={'x':x,'y':y,'d':d}
            self.corners[matchIndices] = {'x':x[cornerIndices],'y':y[cornerIndices],'d':d[cornerIndices]}


    def get_blindspots(self):
        self.blindspots_corners = {}
        self.blindspots = {}
        cornerIndices = np.ix_((0,-1),(0,-1))
        for combo in itertools.combinations(self.corners,2):
            first,second = combo

            xf1 = min(*self.corners[first]['x'][0])
            xf2 = max(*self.corners[first]['x'][0])
            yf1 = min(*self.corners[first]['y'].T[0])
            yf2 = max(*self.corners[first]['y'].T[0])
            xs1 = min(*self.corners[second]['x'][0])
            xs2 = max(*self.corners[second]['x'][0])
            ys1 = min(*self.corners[second]['y'].T[0])
            ys2 = max(*self.corners[second]['y'].T[0])


            xFirstSet = set(range(xf1,xf2))
            xSecondRange = range(xs1,xs2)
            xIntersect = xFirstSet.intersection(xSecondRange)
            yFirstSet = set(range(yf1,yf2))
            ySecondRange = range(ys1,ys2)
            yIntersect = yFirstSet.intersection(ySecondRange)

            if xIntersect & yIntersect:
                xmesh, ymesh = np.meshgrid(np.asarray(list(xIntersect)),np.asarray(list(yIntersect)))
                dValues = sorted([np.average(self.corners[first]['d']),np.average(self.corners[second]['d'])])
                #print(dValues)
                d1 = np.full(xmesh.shape,dValues[0])
                d2 = np.full(xmesh.shape,dValues[1])
                #print(d1,d2)
                self.blindspots[combo] = {'x':xmesh,'y':ymesh,'d1':d1,'d2':d2}
                self.blindspots_corners[combo] = {'x':xmesh[cornerIndices],'y':ymesh[cornerIndices],'d1':d1[cornerIndices],'d2':d2[cornerIndices]}
    def get_occlusion_zones(self):
        self.occlusions = {}
        for blindspot in self.blindspots_corners.items():
            i,blindspot = blindspot
            x = blindspot['x'][0]
            y = blindspot['y'].T[0]
            z = [blindspot['d1'].max(),blindspot['d2'].max()]
            xy = np.meshgrid(x,y)
            xz = np.meshgrid(x,z)
            yz = np.meshgrid(y,z)
            #ax.plot_surface(xy[0],xy[1],np.full(xy[0].shape,z[0]))
            xyPlanes = [
                {'x':xy[0],'y':xy[1],'d':np.full(xy[0].shape,z[0])},
                {'x':xy[0],'y':xy[1],'d':np.full(xy[0].shape,z[1])}
                ]
            xzPlanes = [
                {'x':xz[0],'y':np.full(xz[0].shape,y[0]),'d':xz[1]},
                {'x':xz[0],'y':np.full(xz[0].shape,y[1]),'d':xz[1]}
                ]
            yzPlanes = [
                {'x':np.full(yz[0].shape,x[0]),'y':yz[0],'d':yz[1]},
                {'x':np.full(yz[0].shape,x[1]),'y':yz[0],'d':yz[1]}
                ]
            mx = sorted(x)[0]+(sorted(x)[1] - sorted(x)[0])/2
            my = sorted(y)[0]+(sorted(y)[1] - sorted(y)[0])/2
            mz = sorted(z)[0]+(sorted(z)[1] - sorted(z)[0])/2
            self.occlusions[i]={'xy':xyPlanes,'xz':xzPlanes,'yz':yzPlanes,'c':{'x':[mx],'y':[my],'d':[mz]}}
def gen_v_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8')
    vDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] += 1
        vDisparity[i] = hist
    return({'image':vDisparity,'bins':bins})

def gen_u_disparity(distanceImage,bins):
    scaledImage = np.around((np.copy(distanceImage)/distanceImage.max())*(bins-1)).astype('uint8').T
    uDisparity = np.zeros((scaledImage.shape[0],bins))
    for i,row in enumerate(scaledImage):
        hist = np.zeros(bins)
        for pixel in row:
            pass
            hist[pixel] += 1
        uDisparity[i] = hist
    return({'image':uDisparity.T,'bins':bins})

def test():
    fileName = sys.argv[1]
    arrays = np.load(fileName)[:,40:,20:110]
    image = imageArrays(arrays)
    #image.blur_d(3)
    image.gen_v_disparity(30)
    #image.thresh_v(8,key='image')
    #image.blur_v(n=1,key='thresh')
    image.blur_v(n=1,key='image')
    #image.thresh_v(0.5,key='blur')
    image.thresh_v(8,key='blur')
    #skel = skeletonize(image.v['thresh'])
    image.lines_v(votes=20,minLineLength = 20, maxLineGap = 1,thRes = np.pi/720)

    image.get_plines_v()

    '''plt.imshow(image.v['thresh'])
    plt.imshow(image.d)
    for line in image.v['plines'].items():
        lineNumber, lineDict = line
        plt.plot(lineDict['d'],lineDict['y'])'''
    #plt.close()

    image.gen_u_disparity(30)
    image.blur_u(n=1,key='image')
    image.thresh_u(15,key='blur')
    image.lines_u(votes=20,minLineLength = 3, maxLineGap = 1,thRes = np.pi/180)
    #print(image.u['plinesPoints'])
    image.get_plines_u()
    #print(image.u['plines'])
    '''for line in image.u['plines'].items():
        lineNumber, lineDict = line
        plt.plot(lineDict['x'],lineDict['d'])'''
    plt.imshow(image.u['thresh'])
    image.match_lines()
    #print(image.match)

    '''for match in image.match:
        uline = image.u['plines'][match['u']]
        vline = image.v['plines'][match['v']]
        #plt.plot(uline['x'],vline['y'])
        #print(image.u['plines'][match['u']])
        #print(image.v['plines'][match['v']])'''
    image.mesh_lines()
    #print(image.corners)
    #for mesh in image.surfs:
        #plt.plot(mesh['x'].flatten(),mesh['y'].flatten(),'o')
        #pass
    #for corners in image.surfCorners:
        #print(corners)
        #plt.plot(corners['x'].flatten(),corners['y'].flatten(),'x')
        #pass
    #plt.imshow(image.d)

    #fig = plt.figure()
    #ax = fig.add_subplot(111,projection = '3d')
    #ax.plot_wireframe(image.x,image.y,image.z)
    '''for mesh in image.surfs:
        ax.plot_wireframe(mesh['x'],mesh['y'],mesh['d'])
        #plt.plot(mesh['x'].flatten(),mesh['y'].flatten(),'o')
        pass'''
    '''for combo in itertools.combinations(image.corners,2):
        first,second = combo
        xf1 = min(*image.corners[first]['x'][0])
        xf2 = max(*image.corners[first]['x'][0])
        yf1 = min(*image.corners[first]['y'].T[0])
        yf2 = max(*image.corners[first]['y'].T[0])
        xs1 = min(*image.corners[second]['x'][0])
        xs2 = max(*image.corners[second]['x'][0])
        ys1 = min(*image.corners[second]['y'].T[0])
        ys2 = max(*image.corners[second]['y'].T[0])
        xFirstSet = set(range(xf1,xf2))
        xSecondRange = range(xs1,xs2)
        xIntersect = xFirstSet.intersection(xSecondRange)
        yFirstSet = set(range(yf1,yf2))
        ySecondRange = range(ys1,ys2)
        yIntersect = yFirstSet.intersection(ySecondRange)
        xmesh, ymesh = np.meshgrid(np.asarray(list(xIntersect)),np.asarray(list(yIntersect)))
        plt.plot(xmesh.flatten(),ymesh.flatten(),'x',color = 'r')'''

    image.get_blindspots()
    #print(image.blindspots_corners.items())
        #ax.plot_wireframe(image.x,image.y,image.z)
    fig = plt.figure(facecolor = 'xkcd:grey')
    ax = fig.add_subplot(111,projection = '3d',facecolor='xkcd:grey')
    '''for surf in image.surfs.items():
        i, surf = surf
        ax.plot_wireframe(surf['x'],surf['y'],surf['d'])'''

    '''for blindspot in image.blindspots_corners.items():
        i,blindspot = blindspot
        x = blindspot['x'][0]
        y = blindspot['y'].T[0]
        z = [blindspot['d1'].max(),blindspot['d2'].max()]
        xy = np.meshgrid(x,y)
        xz = np.meshgrid(x,z)
        yz = np.meshgrid(y,z)
        #ax.plot_surface(xy[0],xy[1],np.full(xy[0].shape,z[0]))
        for zVal in z:
            ax.plot_surface(xy[0],xy[1],np.full(xy[0].shape,zVal),color = 'r',alpha = 0.5,edgecolor = 'xkcd:purple')
        for yVal in y:
            ax.plot_surface(xz[0],np.full(xz[0].shape,yVal),xz[1],color = 'r',alpha = 0.5,edgecolor = 'xkcd:purple')
        for xVal in x:
            ax.plot_surface(np.full(yz[0].shape,xVal),yz[0],yz[1],color = 'r',alpha = 0.5,edgecolor = 'xkcd:purple')
        mx = sorted(x)[0]+(sorted(x)[1] - sorted(x)[0])/2
        my = sorted(y)[0]+(sorted(y)[1] - sorted(y)[0])/2
        mz = sorted(z)[0]+(sorted(z)[1] - sorted(z)[0])/2
        ax.plot([mx],[my],[mz],'o',color = 'k')'''
        #ax.plot_surface()
    image.get_occlusion_zones()
    for i, occlusion in image.occlusions.items():
        print(occlusion)
        for xyPlane in occlusion['xy']:
            ax.plot_surface(xyPlane['x'],xyPlane['d'],xyPlane['y'],color = 'r',alpha = 0.3,edgecolor = 'xkcd:purple')
        for xzPlane in occlusion['xz']:
            pass
            ax.plot_surface(xzPlane['x'],xzPlane['d'],xzPlane['y'],color = 'r',alpha = 0.3,edgecolor = 'xkcd:purple')
        for xyPlane in occlusion['yz']:
            ax.plot_surface(xyPlane['x'],xyPlane['d'],xyPlane['y'],color = 'r',alpha = 0.3,edgecolor = 'xkcd:purple')
        ax.plot(occlusion['c']['x'],occlusion['c']['d'],occlusion['c']['y'],'o', color = 'xkcd:purple')
    for i,mesh in image.surfs.items():
        #print(mesh)
        ax.plot_wireframe(mesh['x'],mesh['d'],mesh['y'])
    ax.title.set_text('Occlusion Zone')
    ax.invert_zaxis()
    ax.facecolor = 'xkcd:grey'
    #ax.axis('off')
    #ax.invert_xaxis()
    #cube = [(x < 3) & (y < 3) & (z < 3)]
    #ax.voxels(voxels, facecolors=colors, edgecolor='k')

    #print('\nsurfs\n',image.surfs)
    #print('\ncorners\n',image.corners)
    #print('\nmatch\n',image.match)
    plt.show()
    #plt.savefig('occlusionZone',facecolor="xkcd:grey")

if __name__ == '__main__':
    test()
    '''fileName = sys.argv[1]
    arrays = np.load(fileName)[:,:,0:110]
    plt.imshow(arrays[0])
    plt.show()'''
