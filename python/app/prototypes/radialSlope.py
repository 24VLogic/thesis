import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D as l2d

img = np.load('boxdist.npy')
img = img[:,30:150].astype('float')
#slopeImg = np.zeros((131,120),dtype="float")

def get_radial_slope(column):
    radial_slope = [x2-x1 for x1, x2 in zip(column[:-1], column[1:])]
    return(radial_slope)
rs_img = np.zeros((132,3000))

selectedColumn = np.copy(img.T[25])
img.T[25] = 255

yx = zip(*[yx for yx in enumerate(selectedColumn)])

y = list(yx[0])

x = list(yx[1])
y = y[::-1]

rs = get_radial_slope(selectedColumn)
print rs


columnLine = l2d(
    x,
    y,
    color='b'
)

for i, point in enumerate(rs):
    rsy = int(y[i])
    rsx = int(x[i])
    val = point
    print rsy, rsx, val

    rs_img[rsy,rsx] = abs(val)*100
    print rs_img[rsy,rsx]

fig1 = plt.figure()
ax1 = fig1.add_subplot(221)
ax1.imshow(img)
ax2 = fig1.add_subplot(222)
ax2.add_line(columnLine)
ax2.set_ylim(0,132)
ax2.set_xlim(0,3000)
#ax2.imshow(slopeImg,cmap='Dark2')
ax3 =fig1.add_subplot(223)
ax3.imshow(rs_img[:,1500:3000],cmap='Dark2')
plt.show()
