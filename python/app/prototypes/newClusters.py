import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import DBSCAN
import matplotlib.image as mpimg

def get_groundMask(yImage, bins = 15):
    yImageScaled = yImage
    counts,bounds = np.histogram(yImageScaled.flatten(),bins)
    nGroundPoints = np.max(counts)
    #yBounds = (bounds[np.argmax(counts)],bounds[np.argmax(counts)+1])
    yBounds = (bounds[-2],bounds[-1])
    #yBounds = (bounds[np.argmax(counts)],bounds[-1])
    groundMask= np.zeros((yImage.shape),dtype=bool)
    print(yBounds)
    for m,row in enumerate(yImageScaled):
        for n,pixel in enumerate(row):
            if yBounds[0] <= pixel <= yBounds[1]:
                groundMask[m,n] = True
    return(groundMask,yBounds)

data = np.load('data/testData1.npy')

distanceImage = data[0][:,30:]
amplitudeImage = data[1][:,30:]
xImage = data[2][:,30:]
yImage = data[3][:,30:]
zImage = data[4][:,30:]

groundMask,yBounds = get_groundMask(yImage)

pointsArray = np.vstack((xImage.flatten(),yImage.flatten(),zImage.flatten())).T
indicesNonGround = []
for i,point in enumerate(pointsArray):
    if yBounds[0] <= point[1] <= yBounds[1]:
        pass
    else:
        indicesNonGround.append(i)
pointsArrayNonGround = np.zeros((len(indicesNonGround),3))
for i, index in enumerate(indicesNonGround):
    pointsArrayNonGround[i] = pointsArray[index]

epsilon = 100
dbNonGround = DBSCAN(eps=epsilon, min_samples=10).fit(pointsArrayNonGround)
core_samples_mask = np.zeros_like(dbNonGround.labels_, dtype=bool)
core_samples_mask[dbNonGround.core_sample_indices_] = True
labelsNonGround = dbNonGround.labels_
epsilon = 100

db = DBSCAN(eps=epsilon, min_samples=10).fit(pointsArray)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_
unique_labels = set(labels)

unique_labelsNonGround = set(labelsNonGround)
nanLabels = np.full(labels.shape,np.nan)
print(len(labelsNonGround), len(indicesNonGround))
for i, index in enumerate(indicesNonGround):
    nanLabels[index] = labelsNonGround[i]
nanLabelsMesh = nanLabels.reshape(zImage.shape)
labels = labels.reshape(zImage.shape)
colors = [plt.cm.jet(each)
          for each in np.linspace(0, 1, len(unique_labelsNonGround))]
fig = plt.figure(facecolor='xkcd:grey',figsize = (12,6))
ax1 = fig.add_subplot(121,projection = '3d')
ax2 = fig.add_subplot(122,projection = '3d')
zzGround = np.full(zImage.shape, np.nan)
for m, row in enumerate(groundMask):
    for n, pixel in enumerate(row):
        if pixel:
            zzGround[m,n] = zImage[m,n]
ax2.plot_wireframe(xImage, zzGround, yImage,rstride = 10,cstride = 10,color = 'k')
for k, color in zip(unique_labelsNonGround,colors):
    if k != -1:
        zzCopy = np.full(zImage.shape, np.nan)
        for m, row in enumerate(nanLabelsMesh):
            for n, label in enumerate(row):
                if label == k:
                    zzCopy[m,n] = zImage[m,n]
        ax2.plot_surface(xImage,zzCopy,yImage,cstride = 2, rstride = 2,color = color)

for k, color in zip(unique_labels,colors):
    if k != -1:
        zzCopy = np.full(zImage.shape, np.nan)
        for m, row in enumerate(labels):
            for n, label in enumerate(row):
                if label == k:
                    zzCopy[m,n] = zImage[m,n]

        ax1.plot_surface(xImage,zzCopy,yImage,cstride = 2, rstride = 2,color = color)
ax1.invert_zaxis()
ax1.set_facecolor('xkcd:grey')
ax2.invert_zaxis()
ax2.set_facecolor('xkcd:grey')
ax1.set_xticklabels([])
ax2.set_xticklabels([])
ax1.set_yticklabels([])
ax2.set_yticklabels([])
ax1.set_zticklabels([])
ax2.set_zticklabels([])
ax1.title.set_text('DBSCAN Results')
ax2.title.set_text('Ground Subtraction')
plt.savefig('groundSubtractionClustering.png',facecolor = 'xkcd:grey')
'''
plt.close()
fig, axes = plt.subplots(2,2,sharex=True,sharey=True,figsize=(6,6))
axes = axes.flatten()
for ax in axes:
    ax.set_facecolor('xkcd:violet')
    ax.set_xticklabels([])
    ax.set_yticklabels([])
axes[0].imshow(distanceImage)
axes[1].imshow(labels,cmap = 'jet')
axes[3].imshow(nanLabelsMesh,cmap = 'jet')
dgs = np.copy(distanceImage).astype(float)
for m, row in enumerate(groundMask):
    for n, bool in enumerate(row):
        if bool:
            dgs[m,n] = np.nan
axes[2].imshow(dgs)
axes[0].title.set_text('Distance Image')
axes[1].title.set_text('DBSCAN Results')
axes[2].title.set_text('Ground Subtraction')
axes[3].title.set_text('Filtered DBSCAN Results')
plt.savefig('groundSubtractionClustering2d.png',facecolor = 'xkcd:grey')'''

#plt.show()
