import sys
import o3d3xx
import matplotlib.pyplot as plt
import numpy as np
import time
import matplotlib.animation as animation
import cv2
from scipy.ndimage.filters import gaussian_filter as gf

#from bsdetect import gen_v_disparity,gen_u_disparity,get_horizontal_lines,get_vertical_lines,filter_response,thresh,check_lines

imageWidth = 176
imageHeight = 132

class GrabO3D300():
    def __init__(self,data):
        self.data = data
        self.Amplitude = np.zeros((imageHeight,imageWidth))
        self.Distance = np.zeros((imageHeight,imageWidth))

    def readNextFrame(self):
        result = self.data.readNextFrame()
        self.Amplitude = np.frombuffer(result['amplitude'],dtype='uint16')
        self.Amplitude = self.Amplitude.reshape(imageHeight,imageWidth)
        self.Distance = np.frombuffer(result['distance'],dtype='uint16')
        self.Distance = self.Distance.reshape(imageHeight,imageWidth)
        self.y = np.frombuffer(result['y'],dtype='int16')
        self.y = self.y.reshape(imageHeight,imageWidth)
        self.illuTemp = 20.0

def updatefig(*args):
    g = args[1]
    g.readNextFrame();
    imV = args[2]
    imU = args[3]

    imDist = args[4]
    imDetect = args[5]

    dist_max = float(max(np.max(g.Distance),1));
    imDist.set_array(g.Distance/ dist_max)
    vDisp = gen_v_disparity(g.Distance,bins=30)['image']
    uDisp = gen_u_disparity(g.Distance,bins=30)['image']
    #vResponse = thresh(filter_response(vDisp),threshold = 20)
    #uResponse = thresh(filter_response(uDisp),threshold = 30)
    vDisp_f = filter_response(vDisp)
    uDisp_f = filter_response(uDisp)
    #imV.set_array(vResponse)
    #imU.set_array(uResponse)
    vDisp_f = vDisp_f/float(max(np.max(vDisp),1))
    uDisp_f = uDisp_f/float(max(np.max(uDisp),1))
    vDisp_t = thresh(vDisp_f*100,threshold = 10)
    uDisp_t = thresh(uDisp_f*100,threshold = 10)
    vLines = get_vertical_lines(vDisp_t.astype('uint8'),votes = 20)
    uLines = get_horizontal_lines(uDisp_t.astype('uint8'),votes = 20)
    vLinesImage = np.zeros(vDisp.shape)
    uLinesImage = np.zeros(uDisp.shape)
    '''
    for line in vLines:
        for i,yVal in enumerate(line['y']):
            vLinesImage[int(yVal),int(line['d'][i])] = 1
    for line in uLines:
        for i,xVal in enumerate(line['x']):
            uLinesImage[int(line['d'][i]),int(xVal)] = 1'''
    pairs = check_lines(uLines,vLines)
    detectArray = np.zeros(g.Distance.shape)
    if pairs:
        for key, pair in pairs.items():
            for i,yVal in enumerate(pair['vLine']['y']):
                vLinesImage[int(yVal),int(pair['vLine']['d'][i])] = 1
            for i,xVal in enumerate(pair['uLine']['x']):
                uLinesImage[int(pair['uLine']['d'][i]),int(xVal)] = 1
            ys = pair['vLine']['y']
            xs = pair['uLine']['x']
            detectArray[int(ys[0]):int(ys[-1]),int(xs[0]):int(xs[-1])] = pair['uLine']['d'][0]
    detectArray = detectArray/float(max(np.max(detectArray),1))
    #imV.set_array(vLinesImage)
    #imU.set_array(uLinesImage)
    imV.set_array(np.hstack((vDisp_f,vLinesImage)))
    imU.set_array(np.vstack((uLinesImage,uDisp_f)))
    imDetect.set_array(detectArray)
    #imV.set_array(vDisp)
    #imU.set_array(uDisp)
    #axDist = imDist.get_axes()
    #axDist.set_title('Distance')
    return imV,imU,imDist,imDetect
def get_vDisparity(distanceImage, bins = 30, mask = np.array([None])):
    distanceImageScaled = bins*distanceImage/np.max(distanceImage)
    distanceImageScaled = distanceImageScaled.astype(int)
    dMap = np.zeros((distanceImageScaled.shape[0],bins+1))
    for m,row in enumerate(distanceImageScaled):
        for n, pixel in enumerate(row):
            if mask.any() != None:
                dMap[m,pixel] +=1 & np.invert(mask[m,n])
            else:
                dMap[m,pixel] +=1
    return(dMap/np.max(dMap))
def get_uDisparity(distanceImage, bins = 30, mask = np.array([None])):
    distanceImageScaled = bins*distanceImage/np.max(distanceImage)
    distanceImageScaled = distanceImageScaled.astype(int)
    dMap = np.zeros((bins+1,distanceImageScaled.shape[1]))
    for n,column in enumerate(distanceImageScaled.T):
        for m, pixel in enumerate(column):
            if mask.any() != None:
                dMap[pixel,n] +=1 & np.invert(mask[m,n])
            else:
                dMap[pixel,n] +=1
    return(dMap/np.max(dMap))
def get_groundMask(yImage, bins = 15):
    yImageScaled = yImage
    counts,bounds = np.histogram(yImageScaled.flatten(),bins)
    nGroundPoints = np.max(counts)
    #yBounds = (bounds[np.argmax(counts)],bounds[np.argmax(counts)+1])
    yBounds = (bounds[-2],bounds[-1])
    #yBounds = (bounds[np.argmax(counts)],bounds[-1])
    groundMask= np.zeros((yImage.shape),dtype=bool)
    print(yBounds)
    for m,row in enumerate(yImageScaled):
        for n,pixel in enumerate(row):
            if yBounds[0] <= pixel <= yBounds[1]:
                groundMask[m,n] = True
    return(groundMask)
def gaussian_filter(inImage, sigma = 1, order = [0,0]):
    imageOut = {}
    imageOut['image'] = 1-gf(inImage,sigma = sigma,order = order)
    imageOut['sigma'] = sigma
    imageOut['order'] = order
    return(imageOut)

def filter_response(image,sigma = 0.5):
    imxx = np.copy(image)
    imxy = np.copy(image)
    imyy = np.copy(image)
    for i in range(3):
        imxx = gaussian_filter(imxx,sigma,[0,2])['image']
        imxy = gaussian_filter(imxy,sigma,[1,1])['image']
        imyy = gaussian_filter(imyy,sigma,[2,0])['image']
    imDiff = np.zeros(image.shape)
    for y, row in enumerate(image):
        for x, value in enumerate(row):
            inspect = [imxx[y,x],imxy[y,x],imyy[y,x]]
            highestFilterResponse = max(inspect)
            lowestFilterResponse = min(inspect)
            imDiff[y,x] = highestFilterResponse - lowestFilterResponse
    return(imDiff/np.max(imDiff))
def thresh(inImage,threshold,kw = cv2.THRESH_BINARY):
    ret,imageOut = cv2.threshold(inImage,threshold,1,kw)
    return(imageOut)
def get_vertical_lines(inImage, votes = 10, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    vLines = []
    if len(lines>0):
        for line in lines:
            ys = []

            r, theta = line[0]
            if theta == 0:
                #print(inImage.T[int(r)])
                ys = [i for i, p in enumerate(inImage.T[int(r)]) if p >0]
            rs = [r]*len(ys)
            if ys:
                vLines.append({'d':rs,'y':ys})
    return(vLines)
def get_horizontal_lines(inImage, votes = 10, rRes = 1, thRes = np.pi/180):
    lines =  cv2.HoughLines(inImage, rRes, thRes, votes)
    #print(len(lines),'lines detected')
    hLines = []
    if len(lines)>0:
        for line in lines:
            xs = []
            r, theta = line[0]
            #print(r,int(np.degrees(theta)))
            if int(np.degrees(theta)) == 90:
                #print('catch')
                xs = [i for i, p in enumerate(inImage[int(r)]) if p >0]
            rs = [r]*len(xs)
            if xs:
                hLines.append({'d':rs,'x':xs})
        #print(hLines)
    return(hLines)
def check_lines(uLines,vLines):
    pairIndex = 0
    matchedLines = {}
    for uLine in uLines:
        #print('check')
        #pass
        for vLine in vLines:
            #print(uLine['d'][0],vLine['d'][0])
            if uLine['d'][0] == vLine['d'][0]:
                #print('catch')
                matchedLines[pairIndex] = {'uLine' : uLine,'vLine' : vLine}
                pairIndex += 1
    return(matchedLines)
def get_planes(pairs):
    planes = {}
    for key, pair in pairs.items():
        #print(key)
        xVector = pair['uLine']['x']
        yVector = pair['vLine']['y']
        averageDistance = pair['uLine']['d']
        xx,yy = np.meshgrid(xVector,yVector)
        planes[key]={
            'xVector':xVector,
            'yVector':yVector,
            'distance':averageDistance,
            'xMesh':xx,
            'yMesh':yy,
            'zMesh':np.full(xx.shape,averageDistance[0])
            }
        return(planes)

def ground_subtraction(*args):
    g = args[1]
    g.readNextFrame();
    imV = args[2]
    imU = args[3]
    imDist = args[4]
    imDetect = args[5]
    dist_max = float(max(np.max(g.Distance),1));
    groundMask = get_groundMask(g.y/np.max(g.y))
    imDist.set_array(g.Distance/ dist_max)
    vDisp = get_vDisparity(g.Distance,mask = groundMask)
    uDisp = get_uDisparity(g.Distance,mask = groundMask)
    vDisp_f = filter_response(vDisp)
    uDisp_f = filter_response(uDisp)
    imV.set_array(np.hstack((vDisp,vDisp_f)))
    imU.set_array(np.vstack((uDisp_f,uDisp)))

    ampScale = g.Amplitude/np.max(g.Amplitude)
    imDetect.set_array((np.ma.masked_array(g.y/np.max(g.y), mask=groundMask)))
    return imV,imU,imDist,imDetect
def main():
    bins = 30
    address = sys.argv[1]
    camData = o3d3xx.ImageClient(address, 50010)

    fig = plt.figure()

    grabber = GrabO3D300(camData)
    ax1 = fig.add_subplot(2, 2, 1)
    ax2 = fig.add_subplot(2, 2, 2)
    ax3 = fig.add_subplot(2, 2, 3)
    ax4 = fig.add_subplot(2, 2, 4)
    imV = ax1.imshow(np.random.rand(imageHeight,bins*2))
    imU = ax4.imshow(np.random.rand(bins*2,imageWidth))
    imDetect = ax3.imshow(np.random.rand(imageHeight,imageWidth))
    imDistance = ax2.imshow(np.random.rand(imageHeight,imageWidth))
    ani = animation.FuncAnimation(fig, ground_subtraction, interval=50, blit=True, fargs = [grabber,imV,imU,imDistance,imDetect])
    plt.show()

if __name__ == '__main__':
    main()
