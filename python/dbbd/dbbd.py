'''
Decision-Based Blind-Spot Detection Package

Contents:

Functions:

get_ground_mask(y_image, bins): this function applies the y-histogram analysis to find the boundaries of the ground plane mask
    inputs:
        y_image: the y-axis O3D image
        bins: the number of histogram bins for the masking
    outputs:
        ground_mask: the boolean ground mask image
        y_bounds: the boundaries of the y-values used to create the ground mask image
        counts: the histogram analysis counts
        bins: the histogram analysis bins

get_threshold(image, threshold): this function returns a binary threshold of the input image
    inputs:
        image: the image to be thresholded
        threshold: the threshold value
    outputs:
        thresh_image: the thresholded binary image
'''

def get_ground_mask(y_image, bins):
    y_data = y_image.flatten()
    counts,bounds = np.histogram(y_data[~np.isnan(y_data)],bins)
    num_ground_points = np.max(counts)
    #y_bounds = (bounds[np.argmax(counts)],bounds[np.argmax(counts)+1])
    y_bounds = [np.average(y_image[-3:-1])-100,np.average(y_image[-3:-1])+100]
    #y_bounds = (bounds[-2],bounds[-1])
    #y_bounds = (bounds[np.argmax(counts)],bounds[-1])
    ground_mask= np.zeros((y_image.shape),dtype=bool)
    mif,nif = np.where(np.logical_and(y_image>=y_bounds[0], y_image<=y_bounds[1]))
    for i, m in enumerate(mif):
        n = nif[i]
        ground_mask[m,n] = True
    return(ground_mask,y_bounds,counts,bounds)

def get_threshold(image,threshold):
    ret, thresh_image = cv2.threshold(image,threshold,1,cv2.THRESH_BINARY)
    return(thresh_image.astype('uint8'))

def get_u_disparity_map(distance_image, bins,ground_mask = np.array(None)):
    if ground_mask.any() == None:
        ground_mask = np.zeros((distance_image.shape),dtype = bool)
    distance_image_scaled = bins*distance_image/np.max(distance_image[~np.isnan(distance_image)])
    distance_image_scaled= distance_image_scaled.astype(int)
    ud_map = np.zeros((bins+1,distance_image_scaled.shape[1]))
    for n, column in enumerate(distance_image_scaled.T):
        for m,pixel in enumerate(column):
            if not ground_mask[m,n]:
                #print(pixel)
                if pixel >= 0 :
                    ud_map[int(pixel),n] +=1
    return(ud_map)

def get_v_disparity_map(distance_image, bins,ground_mask = np.array(None)):
    if ground_mask.any() == None:
        ground_mask = np.zeros((distance_image.shape),dtype = bool)
    distance_image_scaled = bins*distance_image/np.max(distance_image[~np.isnan(distance_image)])
    distance_image_scaled= distance_image_scaled.astype(int)
    vd_map = np.zeros((distance_image_scaled.shape[0],bins+1))
    for m, row in enumerate(distance_image_scaled):
        for n,pixel in enumerate(row):
            if not ground_mask[m,n]:
                #print(pixel)
                if pixel >= 0 :
                    vd_map[m,pixel] +=1
    return(vd_map)

def get_xz_histogram(x_image,z_image,x_bins,z_bins,ground_mask = np.array(None)):
    if ground_mask.any() == None:
        ground_mask = np.zeros((x_image.shape),dtype = bool)
    z_image_scaled = z_bins*z_image/np.max(z_image[~np.isnan(z_image)])
    z_image_scaled = z_image_scaled.astype(int)

    x_offset = np.min(x_image[~np.isnan(x_image)])
    x_span = np.max(x_image[~np.isnan(x_image)])-x_offset
    x_image_scaled = np.copy(x_image).astype(float)
    x_image_scaled -= x_offset
    x_image_scaled = x_bins*x_image_scaled/x_span
    x_image_scaled = x_image_scaled.astype(int)

    xz = np.zeros((z_bins+1,x_bins))
    for i in range(len(x_image_scaled.flatten())):
        x_value = x_image_scaled.flatten()[i]-1
        z_value = z_image_scaled.flatten()[i]
        if not ground_mask.flatten()[i]:
            if z_value >=0:
                xz[z_value,x_value]+=1
    return(xz, x_span/x_bins,x_offset)

def get_yz_histogram(y_image,z_image,y_bins,z_bins,ground_mask = np.array(None)):
    if ground_mask.any() == None:
        ground_mask = np.zeros((y_image.shape),dtype = bool)
    z_image_scaled = z_bins*z_image/np.max(z_image[~np.isnan(z_image)])
    z_image_scaled = z_image_scaled.astype(int)

    y_offset = np.min(y_image[~np.isnan(y_image)])
    y_span = np.max(y_image[~np.isnan(y_image)])-y_offset
    y_image_scaled = np.copy(y_image).astype(float)
    y_image_scaled -= y_offset
    y_image_scaled = y_bins*y_image_scaled/y_span
    y_image_scaled = y_image_scaled.astype(int)

    yz = np.zeros((y_bins,z_bins+1))
    for i in range(len(y_image_scaled.flatten())):
        y_value = y_image_scaled.flatten()[i]-1
        z_value = z_image_scaled.flatten()[i]
        if not ground_mask.flatten()[i]:
            if z_value >= 0:
                yz[y_value,z_value]+=1
    return(yz, y_span/y_bins,y_offset)

def gaussian_filter(input_image, sigma = 1, order = [0,0]):
    output_image = {}
    output_image['image'] = 1-gf(input_image, sigma = sigma, order = order)
    output_image['sigma'] = sigma
    output_image['order'] = order
    return(output_image)

def get_filter_response(image, sigma,n=3):
    imxx = np.copy(image)/np.max(image)
    imxy = np.copy(image)/np.max(image)
    imyy = np.copy(image)/np.max(image)
    difference_image = np.copy(image)
    for i in range(n):
        imxx = gaussian_filter(difference_image,sigma,[0,2])['image']
        imxy = gaussian_filter(difference_image,sigma,[1,1])['image']
        imyy = gaussian_filter(difference_image,sigma,[2,0])['image']
        for y, row in enumerate(image):
            for x, value in enumerate(row):
                inspect = [imxx[y,x],imxy[y,x],imyy[y,x]]
                #inspect = [imxx[y,x],imyy[y,x]]
                hi_filter_response = max(inspect)
                lo_filter_response = min(inspect)
                difference_image[y,x] = hi_filter_response - lo_filter_response
        return(difference_image)

def get_vertical_lines(input_image, votes, r_resolution=0.25, theta_resolution = np.pi/180,verbose = False):
    input_image = np.vstack((input_image,np.zeros(input_image.shape[1],dtype='uint8')))
    #input_image = np.hstack((input_image,np.zeros((input_image.shape[0],1),dtype='uint8')))
    lines = cv2.HoughLines(input_image, r_resolution, theta_resolution, votes)
    vertical_lines = []
    if verbose:
        print('detecting vertical lines. . .')
    if np.array(lines).any() != None:

        for line in lines:
            r, theta = line[0]
            if verbose:
                print('line detected')
                print('r',r)
                print('theta',np.degrees(theta))
            if int(np.degrees(theta) == 0):
                row = input_image.T[np.around(int(r))+1]
                if verbose:
                    print('theta within range')
                    print('row',int(np.around(r)))
                    print(row)
                y_arrays = []
                y = []
                for i, pixel in enumerate(row):
                    if pixel > 0:
                        y.append(i)
                    else:
                        if y:
                            y_arrays.append(y)
                            y = []
                for array in y_arrays:
                    vertical_lines.append({'d':[int(np.around(r))]*len(array),'y':array})
    return(vertical_lines)

def get_horizontal_lines(input_image, votes, r_resolution=0.25, theta_resolution = np.pi/180,verbose = False):
    input_image = np.vstack((input_image,np.zeros(input_image.shape[1],dtype='uint8')))
    input_image = np.hstack((input_image,np.zeros((input_image.shape[0],1),dtype='uint8')))
    lines = cv2.HoughLines(input_image, r_resolution, theta_resolution, votes)
    horizontal_lines = []
    if verbose:
        print('detecting lines horizontal lines. . .')
    if np.array(lines).any() != None:
        for line in lines:
            r, theta = line[0]
            if verbose:
                print('horizontal line detected')
                print('r',r)
                print('theta',np.degrees(theta))
            if int(np.degrees(theta) == 90):
                row = input_image[int(np.around(r))]
                if verbose:
                    print('theta within range')
                    print('row',int(np.around(r)))
                    print(row)
                x_arrays = []
                x = []
                for i, pixel in enumerate(row):

                    if pixel > 0:
                        x.append(i)
                    else:
                        #print('line break detected')
                        if x:
                            x_arrays.append(x)
                            x = []
                for array in x_arrays:
                    horizontal_lines.append({'d':[int(np.around(r))]*len(array),'x':array})
    return(horizontal_lines)

class bsdata():
    #the bsdata object

    def __init__(self,orientation = {'angle':0,'x_offset':0,'y_offset':0,'z_offset':0}):
        #set_frame
        #self.set_frame(camera_array)
        #set default parameters
        self.orientation = orientation
        self.g_bins = 15
        self.ud_bins = 31
        self.vd_bins = self.ud_bins
        self.x_bins = self.x_image.shape[1]
        self.z_bins = 20
        self.y_bins = self.y_image.shape[0]
        self.u_filter_sigma = 0.5
        self.v_filter_sigma = 0.5
        self.x_filter_sigma = 1
        self.y_filter_sigma = 1
        self.u_threshold = 9
        self.v_threshold = 100
        self.y_threshold = 20
        self.x_threshold = 20
        self.u_votes = 5
        self.v_votes = 5
        self.x_votes = 4
        self.y_votes = 4
        self.db_eps = 85
        self.db_min = 10
        #update frame()
        #self.update_frame()


    def set_frame(self,camera_array):
        self.distance_image = camera_array[0]
        self.amplitude_image = camera_array[1]
        self.x_image = np.array(camera_array[2],dtype = float)
        self.y_image = np.array(camera_array[3],dtype = float)
        self.z_image = np.array(camera_array[4],dtype = float)

    def get_uv_analysis(self):
        self.get_ud_map()
        self.get_vd_map()
        self.get_vd_fr()
        self.get_ud_fr()
        self.get_ud_fr_threshold()
        self.get_vd_fr_threshold()
        self.get_ud_lines()
        self.get_vd_lines()
        self.get_uv_planes()

    def get_xyz_histogram_analysis(self):
        self.get_xz_hist()
        self.get_yz_hist()
        self.get_xz_hist_fr()
        self.get_yz_hist_fr()
        self.get_xz_hist_fr_threshold()
        self.get_yz_hist_fr_threshold()
        self.get_x_lines()
        self.get_y_lines()
        self.get_xy_planes()
        self.get_xy_plane_masks()

    def get_spatial_clustering_analysis(self):
        self.cluster_ng()

    def update_frame(self):
        self.reorient_data(self.orientation)
        self.blur_distance_image()
        #update blindspot detector frame

        ## GROUND MASK ##
        ###ground mask is currently required for all subsequent functions
        self.get_ground_mask()
        self.apply_ground_mask()

        ## SPATIAL CLUSTERING ##
        self.cluster_ng()

        ## UV DISPARITY MAP ##
        self.get_ud_map()
        self.get_vd_map()
        self.get_vd_fr()
        self.get_ud_fr()
        self.get_ud_fr_threshold()
        self.get_vd_fr_threshold()
        self.get_ud_lines()
        self.get_vd_lines()
        self.get_uv_planes()

        ## XY HISTOGRAM MAP $$
        '''self.get_xz_hist()
        self.get_yz_hist()
        self.get_xz_hist_fr()
        self.get_yz_hist_fr()
        self.get_xz_hist_fr_threshold()
        self.get_yz_hist_fr_threshold()
        self.get_x_lines()
        self.get_y_lines()
        self.get_xy_planes()
        self.get_xy_plane_masks()'''
        #self.get_xyz_bounds()
        #self.get_xyz_blindspots()

    def blur_distance_image(self):
        self.distance_image = np.array(self.distance_image,dtype='float32')
        self.distance_image = cv2.bilateralFilter(self.distance_image,9,75,75)
        self.distance_image = cv2.bilateralFilter(self.distance_image,9,75,75)
        self.distance_image = cv2.bilateralFilter(self.distance_image,9,75,75)
        self.distance_image = cv2.bilateralFilter(self.distance_image,9,75,75)
        self.distance_image = cv2.bilateralFilter(self.distance_image,9,75,75)
        self.distance_image = cv2.bilateralFilter(self.distance_image,9,75,75)
        self.distance_image = cv2.bilateralFilter(self.distance_image,9,75,75)

    def reorient_data(self, orientation):
        angle = orientation['angle']
        x_offset = orientation['x_offset']
        y_offset = orientation['y_offset']
        z_offset = orientation['z_offset']
        point_cloud = np.vstack((self.x_image.flatten(),self.y_image.flatten(),self.z_image.flatten())).T
        new_point_cloud = RY(point_cloud, angle)
        new_x_image = new_point_cloud.T[0].reshape(self.x_image.shape) + x_offset
        new_y_image = new_point_cloud.T[1].reshape(self.y_image.shape) + y_offset
        new_z_image = new_point_cloud.T[2].reshape(self.z_image.shape) + z_offset

        self.x_image = new_x_image
        self.y_image = new_y_image
        self.z_image = new_z_image

    def get_ground_mask(self):
        self.ground_mask,self.g_y_bounds,self.g_counts,self.g_bins = get_ground_mask(self.y_image,self.g_bins)

    def apply_ground_mask(self):
        m_if,n_if = np.where(self.ground_mask == True)
        self.x_ng = np.copy(self.x_image).astype(float)
        self.y_ng = np.copy(self.y_image).astype(float)
        self.z_ng = np.copy(self.z_image).astype(float)
        self.d_ng = np.copy(self.distance_image).astype(float)
        self.a_ng = np.copy(self.amplitude_image).astype(float)
        for i, m in enumerate(m_if):
            n = n_if[i]
            self.x_ng[m,n] = np.nan
            self.y_ng[m,n] = np.nan
            self.z_ng[m,n] = np.nan
            self.d_ng[m,n] = np.nan
            self.a_ng[m,n] = np.nan

    def get_ud_map(self):
        self.ud_map = get_u_disparity_map(self.distance_image,self.ud_bins)
        self.ud_map_ng = get_u_disparity_map(self.distance_image,self.ud_bins,self.ground_mask)

        pass

    def get_vd_map(self):
        self.vd_map = get_v_disparity_map(self.distance_image,self.vd_bins)
        self.vd_map_ng = get_v_disparity_map(self.distance_image,self.vd_bins,self.ground_mask)

    def get_xz_hist(self):
        self.xz,self.x_scale,self.x_offset = get_xz_histogram(self.x_image,self.z_image,self.x_bins,self.z_bins)
        self.xz_ng,self.x_scale_ng,self.x_offset_ng = get_xz_histogram(self.x_image,self.z_image,self.x_bins,self.z_bins,ground_mask = self.ground_mask)

    def get_yz_hist(self):
        self.yz, self.y_scale, self.y_offset = get_yz_histogram(self.y_image,self.z_image,self.y_bins,self.z_bins)
        self.yz_ng, self.y_scale_ng, self.y_offset_ng = get_yz_histogram(self.y_image,self.z_image,self.y_bins,self.z_bins,self.ground_mask)

    def get_ud_fr(self):
        self.ud_fr = get_filter_response(self.ud_map,sigma = self.u_filter_sigma)
        self.ud_ng_fr = get_filter_response(self.ud_map_ng, sigma = self.u_filter_sigma)

    def get_vd_fr(self):
        self.vd_fr = get_filter_response(self.vd_map,sigma = self.v_filter_sigma)
        self.vd_ng_fr = get_filter_response(self.vd_map_ng, sigma = self.v_filter_sigma)

    def get_xz_hist_fr(self):
        self.xz_fr = get_filter_response(self.xz,self.x_filter_sigma)
        self.xz_ng_fr = get_filter_response(self.xz_ng,self.x_filter_sigma)

    def get_yz_hist_fr(self):
        self.yz_fr = get_filter_response(self.yz,self.y_filter_sigma)
        self.yz_ng_fr = get_filter_response(self.yz_ng,self.y_filter_sigma)

    def get_ud_fr_threshold(self):
        self.ud_fr_thresh = get_threshold(self.ud_fr, self.u_threshold)
        self.ud_ng_fr_thresh = get_threshold(self.ud_ng_fr,self.u_threshold)

    def get_vd_fr_threshold(self):
        self.vd_fr_thresh = get_threshold(self.vd_fr, self.v_threshold)
        self.vd_ng_fr_thresh = get_threshold(self.vd_ng_fr, self.v_threshold)

    def get_xz_hist_fr_threshold(self):
        self.xz_fr_thresh = get_threshold(self.xz_fr, self.x_threshold)
        self.xz_ng_fr_thresh = get_threshold(self.xz_ng_fr,self.x_threshold)

    def get_yz_hist_fr_threshold(self):
        self.yz_fr_thresh = get_threshold(self.yz_fr, self.y_threshold)
        self.yz_ng_fr_thresh = get_threshold(self.yz_ng_fr, self.y_threshold)

    def get_ud_lines(self):
        self.ud_lines = get_horizontal_lines(self.ud_fr_thresh, self.u_votes)
        self.ud_ng_lines = get_horizontal_lines(self.ud_ng_fr_thresh, self.u_votes)

    def get_vd_lines(self):
        self.vd_lines = get_vertical_lines(self.vd_fr_thresh,self.v_votes)
        self.vd_ng_lines = get_vertical_lines(self.vd_ng_fr_thresh,self.v_votes)

    def get_x_lines(self):
        self.xz_lines = get_horizontal_lines(self.xz_fr_thresh,self.x_votes)
        self.xz_ng_lines = get_horizontal_lines(self.xz_ng_fr_thresh, self.x_votes)

    def get_y_lines(self):
        self.yz_lines = get_vertical_lines(self.yz_fr_thresh,self.y_votes)
        self.yz_ng_lines = get_vertical_lines(self.yz_ng_fr_thresh, self.y_votes)

    def get_uv_planes(self):
        self.uv_planes = []
        for u_line in self.ud_ng_lines:
            for v_line in self.vd_ng_lines:
                if np.average(u_line['d']) == np.average(v_line['d']):
                    uu, vv = np.meshgrid(u_line['x'],v_line['y'])
                    dd = np.full(uu.shape,np.average(u_line['d']*self.ud_bins))
                    self.uv_planes.append({'uu':uu,'vv':vv,'dd':dd})

    def get_xy_planes(self):
        self.xy_planes = []
        for y_line in self.yz_ng_lines:
            for x_line in self.xz_ng_lines:
                if np.average(y_line['d']) == np.average(x_line['d']):
                    xx, yy = np.meshgrid(x_line['x'],y_line['y'])
                    zz = np.full(xx.shape,np.average(x_line['d']))
                    self.xy_planes.append({'xx':xx,'yy':yy,'zz':zz})

    def get_xy_plane_masks(self):
        self.xy_plane_masks = []
        for xy_plane in self.xy_planes:
                xy_plane_mask = np.full(self.distance_image.shape,False)
                x_vector = np.sort([xy_plane['xx'][0][0]*self.x_scale + self.x_offset,xy_plane['xx'][0][-1]*self.x_scale + self.x_offset])
                y_vector = np.sort([xy_plane['yy'].T[0][0]*self.y_scale + self.y_offset,xy_plane['yy'].T[0][-1]*self.y_scale + self.y_offset])
                z_min = np.average(xy_plane['zz'])*np.max(self.z_image)/self.z_bins
                for m, row in enumerate(self.distance_image):
                    for n, pixel in enumerate(row):
                        if x_vector[-1] >= self.x_image[m,n] >= x_vector[0]:
                            if y_vector[-1] >= self.y_image[m,n] >= y_vector[0]:
                                if self.z_image[m,n] >= z_min:
                                    xy_plane_mask[m,n] = True
                self.xy_plane_masks.append(xy_plane_mask)

    def get_xyz_blindspots(self):
        self.xyz_blindspots = []
        for xyz_bound in self.xyz_bounds:
            occlusion_mask = np.zeros(self.distance_image.shape,dtype = bool)
            for m in range(self.distance_image.shape[0]):
                for n in range(self.distance_image.shape[1]):
                    occlusion_mask[m,n] = (self.x_image[m,n] >= xyz_bound['x'][0]) & (self.y_image[m,n] >= xyz_bound['y'][0]) & (self.z_image[m,n] >= xyz_bound['z']) & 1-self.ground_mask[m,n]
            self.xyz_blindspots.append(occlusion_mask)

    def get_xyz_bounds(self):
        self.xyz_bounds = []
        for plane in self.xy_planes:
            x_bounds = [np.min(plane['xx'])*self.x_scale_ng + self.x_offset_ng,np.max(plane['xx'])*self.x_scale_ng + self.x_offset_ng]
            y_bounds = [np.min(plane['yy'])*self.y_scale_ng + self.y_offset_ng,np.max(plane['yy'])*self.y_scale_ng + self.y_offset_ng]
            z_bound = [np.min(plane['zz'])*np.max(self.z_image)/self.z_bins]
            self.xyz_bounds.append({'x':x_bounds,'y':y_bounds,'z':z_bound})

    def return_occluded_data(self, xyz_bounds):
        x_bounds = xyz_bounds['x']
        y_bounds = xyz_bounds['y']
        z_bound = xyz_bounds['z']
        x_mask = np.zeros(self.distance_image.shape,dtype = bool)
        y_mask = np.copy(x_mask)
        z_mask = np.copy(x_mask)
        for m, row in enumerate(self.distance_image):
            for n, value in enumerate(row):
                x_mask[m,n] = (x_bounds[1] > self.x_image[m,n] > x_bounds[0])
                y_mask[m,n] = (y_bounds[1] > self.y_image[m,n] > y_bounds[0])
                z_mask[m,n] = (self.z_image[m,n] > z_bound)
        y_mask = y_mask & 1-self.ground_mask
        return({'x':x_mask,'y':y_mask,'z':z_mask})

    def cluster_ng(self):
        ng_indices_m, ng_indices_n = np.where(self.ground_mask == False)
        points = np.zeros((ng_indices_m.size,3))

        for i, m in enumerate(ng_indices_m):
                n = ng_indices_n[i]
                points[i][0] = self.x_image[m,n]
                points[i][1] = self.y_image[m,n]
                points[i][2] = self.z_image[m,n]

        db = DBSCAN(eps= self.db_eps, min_samples = self.db_min).fit(points)
        core_samples_mask = np.zeros_like(db.labels_,dtype=bool)
        core_samples_mask[db.core_sample_indices_] = True
        labels = db.labels_
        unique_labels = set(labels)
        nan_labels = np.full(self.x_image.shape,np.nan)
        bool_labels = np.zeros(self.x_image.shape,dtype=bool)
        self.clusters = []
        self.clusters_master = np.copy(nan_labels)
        for unique_label in unique_labels:
            label_mask = np.copy(bool_labels)
            label_tag = unique_label
            cluster = {'mask':label_mask,'tag':label_tag}
            for i,label in enumerate(labels):
                if label != -1:
                    self.clusters_master[ng_indices_m[i],ng_indices_n[i]] = label
                if unique_label == label:
                    label_mask[ng_indices_m[i],ng_indices_n[i]] = True
            self.clusters.append(cluster)
