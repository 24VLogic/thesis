\documentclass[11pt]{report}
\usepackage{graphicx}
\usepackage{float}
\graphicspath{ {images/} }
\author{Joseph Prine}
\title{\textbf{Decision-Based Blind-Spot Detection with 3D Imaging}\\Thesis Proposal}

\begin{document}

\maketitle

\section*{Abstract}

\paragraph{Active 3d cameras can be deployed in a space to remotely sense objects and create spatial models of the environment. A large object entering this environment creates a region of occlusion, or a blind-spot. This blind-spot presents a problem for the active camera, because it has the potential to conceal information from the spatial model, which could be critical if the system is employed for safety of persons and property. Blind-spots can be mitigated by the deployment of multiple networked cameras. A system will be developed that creates a spatial model from a 3d camera and identifies blind-spots. This system will then publish information regarding the blind-spot to the network. Two of these systems acting on the same network will then pass missing information between them in order to mitigate the information loss in the blind-spot. The developed system will be capable of classifying blind-spots in order of priority, based upon certain criteria.}

\newpage

\section*{Literature Survey}
\paragraph{The remote sensing of objects is a topic that has generated lots of interest in many fields for some time. The problem can be divided into two categories: sensing and interpretation.}
\paragraph{Many methods have been developed to remotely detect objects. Cameras, sonar, and lidar are some examples of remote detection technologies. These methods can be active, or passive. Active methods are those that act on a space and objects in it, by emitting a sound wave or some form of electromagnetic energy, and collecting the physical response with appropriate sensors. An example of an active sensing method is a laser range-finder, which emits a coherent beam of light that is scattered upon striking an object. The sensor of the laser range-finder then measures the magnitude, wavelength, and other properties of the scattered, or re-emitted light. Passive methods instead use sensors to detect physical phenomenon in the environment. A passive method could be, for example, a camera that captures a the ambient light reflected by an object. Both active and passive methods comprise the sensing part of the technique of remote object detection.}
\paragraph{After data has been collected, some processing must occur for it to be useful. The sensing methods previously described inherently require at least the minimal amount of processing to be useful, for instance, the conversion of the phase-shift of the wavelength of light emitted by the laser range-finder into the time-of-flight value, and ultimately, the distance from the sensor. The paradigm of interpretation presented by this research, however, is based upon processing the collected data at a higher level. For instance, not only collecting and calculating the spatial coordinates of a cloud of one-dimensional points from an active camera exposure, but instead estimating a three-dimensional surface mesh from a cloud of one-dimensional points.}

\paragraph{Several methods have been presented for faithfully estimating a three-dimensional model from analysis of three-dimensional point-clouds collected of a scanned object\cite{bernard}\cite{nagai}. Density clustering and other techniques can be used to extract information about object differentiation from the raw data of the point cloud\cite{ghosh}. Point-cloud interpretation has a wide range of applications, including medical, and archaeological research\cite{verhoeven}.}

\paragraph{Two-dimensional images captured by optical cameras can be used for object detection and tracking. Images from multiple perspectives can be used to estimate three-dimensional object locations in a scene. Active cameras can provide range images as well. These images can be captured by multiple cameras\cite{hane}, or one moving active camera\cite{nakao}. Active cameras, like those used in this research, provide opportunities to employ a variety of image processing techniques and geometric analyses for the purpose of object detection\cite{derhawen}\cite{gao2}\cite{gao}\cite{sobottka}}

\paragraph{The concept of multiple cameras, or sensors has been explored to enable more complete surface estimations, as well as additional interpretations of detected objects and their movements through an environment. In the case of active sensors, the concept of a multi-functional array has been proposed. This array is suggested to be capable of not only remotely detecting objects, but also using the active component of the sensor to transmit or broadcast information between the nodes in the array.\cite{krill}. Multiple sensors in an environment enable the tracking of moving objects through crowded or occluded environments\cite{wenzl}\cite{karamiani}. Reconstructing a complete scene from multiple perspectives enables a more complete view of an event\cite{sun}\cite{henrick}.}

\section*{Proposed Algorithm}
\paragraph{The proposed algorithm will execute as follows:}
\begin{enumerate}

\item Background subtraction
\begin{itemize}
\item{Differentiating free-standing objects from the surroundings is a critical first step. Averaging and median filters will be evaluated, as well as Gaussian blur techniques.}
\end{itemize}
\newpage
\item Clustering and segmentation
\begin{itemize}
\item{Spatial clustering and segmentation will be important for the decison-making steps of the algorithm. Segmentation allows for the potential to parallelize the next steps of the algorithm.}
\end{itemize}

\item Contour identification
\begin{itemize}
\item{Identifying the contour of a detected object is the first step in estimating the object's shadow.}
\end{itemize}

\item Blind-spot estimation
\begin{itemize}
\item{The estimated shadow will be used to model the blind-spot, or occlusion zone. Criteria will then be used to determine if the blind-spot is critical. Example criteria are the maximum height or width of a hideable object.}
\end{itemize}

\item Network communication
\begin{itemize}
\item{If a blindspot is determined to be critical, the algorithm will make a network request for the missing pieces of the image. The algorithm will then publish information to serve requests from other instances of the algorithm. Ideally this communication will be performed by symmetric actors, but a server-client topology is also possible.}
\end{itemize}

\item Mosaic Construction
\begin{itemize}
\item{The algorithm will then assemble an image with more complete information, potentially mitigating the blind-spot.}
\end{itemize}
\end{enumerate}
\medskip
\section*{Example Images}
\begin{figure}[H]
\centering
\caption{Example of O3D Image Capture}
\includegraphics[height=3in]{o3dfig1}
\end{figure}

\begin{figure}[H]
\centering
\caption{Example of Averaging Filter Background Subtraction}
\includegraphics[height=3in]{background_subtraction}
\end{figure}
\begin{figure}[H]
\centering
\caption{Example of a DBSCAN Clustering Algorithm}
\includegraphics[height=3in]{DBSCAN_3d}
\end{figure}
\begin{figure}[H]
\centering
\caption{DBSCAN Clustering of an O3D Image}
\includegraphics[height=3in]{fridge}
\end{figure}
\section*{Deliverables}
\begin{itemize}
\item Networked 3D-Sensor Testbed
\begin{itemize}
\item Testbed comprized of two free-standing or mountable IFM O3D Sensors, a networking component (e.g. a router or switch), and a python-based application for the acquisition of radial images, distance images, and cartesian coordinate point-clouds.
\item To be delivered May 2018 
\end{itemize}
\item Blind-spot Detection Algorithm 
\begin{itemize}
\item Python-based application for the estimation of blind-spots created by large occluding objects in the scene.
\item To be delivered June 2018 
\end{itemize}
\item Decision-based Blind-spot Mitigation Algorithm
\begin{itemize}
\item Python-based application that will analyze the blind-spot estimation and request data from a corresponding perspective to create a more complete image.
\item To be delivered July 2018 
\end{itemize}
\item Thesis Document and Defense Presentation
\begin{itemize}
\item A thesis document fulfilling the requirements in the Florida Polytechnic Graduate Thesis manual and the accompanying defense, which will be presented to the thesis committee
\item To be delivered August 2018 
\end{itemize}
\end{itemize}

\bibliographystyle{unsrt}
\bibliography{bib}
\nocite{*}
\end{document}